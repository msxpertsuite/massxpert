[Setup]
AppName=massXpert
appname=massxpert

; Set version number below
#define public version "6.0.1"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "C:\msys64\home\mydar\devel\minexpert2\development"

; Set version number below
AppVerName={#AppName} version {#version}
DefaultDirName={commonpf}\{#AppName}
DefaultGroupName={#AppName}
OutputDir="C:\msys64\home\mydar\devel\{#appname}\development\winInstaller"

; Set version number below
OutputBaseFilename={#AppName}-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile={#AppName}-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\LICENSE"
AppCopyright="Copyright (C) 2016-2020 Filippo Rusconi"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments={#AppName}, by Filippo Rusconi"
AppContact="Filippo Rusconi, PhD, Research scientist at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
WizardImageFile="{#sourceDir}\images\splashscreen-{#appname}-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}\data"
Name: "{app}\doc"

[Files]
Source: "C:\{#AppName}-libDeps\*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\doc\history.html"; DestDir: {app}\doc;

Source: "{#sourceDir}\..\build-area\mingw64\src\{#AppName}.exe"; DestDir: {app};

Source: "{#sourceDir}\massxpert\data\*"; DestDir: {app}\data; Flags: recursesubdirs;

Source: "{#sourceDir}\doc\user-manual\{#appname}-doc.pdf"; DestDir: {app}\doc;
Source: "{#sourceDir}\doc\user-manual\build\{#appname}-user-manual\html\{#appname}-user-manual\*"; Flags: recursesubdirs; DestDir: {app}\doc\html;

[Icons]
Name: "{group}\{#AppName}"; Filename: "{app}\{#AppName}.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall {#AppName}"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\{#AppName}.exe"; Description: "Launch {#AppName}"; Flags: postinstall nowait unchecked

