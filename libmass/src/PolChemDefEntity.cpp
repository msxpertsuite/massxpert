/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "PolChemDefEntity.hpp"


namespace msxps
{

namespace libmass
{


  //! Constructs a polymer chemistry definition entity.
  /*!

    \param polChemDef reference polymer chemistry definition. This pointer
    cannot be 0.

    \param name name of the entity.
  */
  PolChemDefEntity::PolChemDefEntity(PolChemDefCstSPtr polChemDefCstSPtr,
                                     const QString &name)
  {
    Q_ASSERT(polChemDefCstSPtr);
    Q_ASSERT(polChemDefCstSPtr.get());

    mcsp_polChemDef = polChemDefCstSPtr;

    Q_ASSERT(!name.isEmpty());
    m_name = name;
  }


  //! Constructs a copy of \p other.
  /*!  \param other entity to be used as a mold.
   */
  PolChemDefEntity::PolChemDefEntity(const PolChemDefEntity &other)
  {
    Q_ASSERT(other.mcsp_polChemDef);
    mcsp_polChemDef = other.mcsp_polChemDef;

    Q_ASSERT(!other.m_name.isEmpty());
    m_name = other.m_name;
  }


  PolChemDefEntity::~PolChemDefEntity()
  {
  }


  //! Creates a new polymer chemistry definition entity.
  /*! The newly created polymer chemistry definition entity is
    initialized with the data in \c this.

    \return the newly allocated polymer chemistry definition entity which
    will have to be deleted when no longer in use.
  */
  PolChemDefEntity *
  PolChemDefEntity::clone() const
  {
    PolChemDefEntity *other = new PolChemDefEntity(*this);

    return other;
  }


  //! Modifies \p other to be identical to \p this.
  /*!  \param other entity to modify.
   */
  void
  PolChemDefEntity::clone(PolChemDefEntity *other) const
  {
    Q_ASSERT(other);

    Q_ASSERT(mcsp_polChemDef);
    Q_ASSERT(mcsp_polChemDef.get());

    other->mcsp_polChemDef = mcsp_polChemDef;

    Q_ASSERT(!m_name.isEmpty());
    other->m_name = m_name;
  }


  //! Modifies \p this  to be identical to \p other.
  /*!  \param other entity to be used as a mold.
   */
  void
  PolChemDefEntity::mold(const PolChemDefEntity &other)
  {
    if(&other == this)
      return;

    Q_ASSERT(other.mcsp_polChemDef);
    mcsp_polChemDef = other.mcsp_polChemDef;

    Q_ASSERT(!other.m_name.isEmpty());
    m_name = other.m_name;
  }


  //! Assigns other to \p this entity.
  /*! \param other entity used as the mold to set values to \p this
    instance.

    \return a reference to \p this entity.
  */
  PolChemDefEntity &
  PolChemDefEntity::operator=(const PolChemDefEntity &other)
  {
    if(&other != this)
      mold(other);

    return *this;
  }


  //! Sets the name.
  /*!  \param text new name.
   */
  void
  PolChemDefEntity::setName(const QString &text)
  {
    m_name = text;
  }


  //! Returns the name.
  /*!  \return the name.
   */
  QString
  PolChemDefEntity::name() const
  {
    Q_ASSERT(!m_name.isEmpty());
    return m_name;
  }


  void
  PolChemDefEntity::setPolChemDefCstSPtr(PolChemDefCstSPtr polChemDefCstSPtr)
  {
    mcsp_polChemDef = polChemDefCstSPtr;
  }


  //! Returns the polymer chemistry definition.
  /*!  \return the polymer chemistry definition. Assertion that it is
    non-0 is performed before the return. Non-0 return is thus
    guaranteed.
  */
  PolChemDefCstSPtr
  PolChemDefEntity::polChemDefCstSPtr() const
  {
    Q_ASSERT(mcsp_polChemDef);
    Q_ASSERT(mcsp_polChemDef.get());
    return mcsp_polChemDef;
  }


  //! Tests equality.
  /*! The test is performed on:

    - The polymer chemistry definition pointer, not the contents of that
    polymer chemistry definition;

    - The name.

    \param other entity to be compared with \p this.

    \return true if the entities are identical, false otherwise.
  */
  bool
  PolChemDefEntity::operator==(const PolChemDefEntity &other) const
  {
    int tests = 0;

    Q_ASSERT(mcsp_polChemDef && other.mcsp_polChemDef);
    Q_ASSERT(mcsp_polChemDef.get() && other.mcsp_polChemDef.get());
    tests += (mcsp_polChemDef == other.mcsp_polChemDef);

    Q_ASSERT(!m_name.isEmpty() && !other.m_name.isEmpty());
    tests += (m_name == other.m_name);

    if(tests < 2)
      return false;

    return true;
  }


  //! Tests inequality.
  /*! The test is performed on:

    - The polymer chemistry definition pointer, not the contents of that
    polymer chemistry definition;

    - The name.

    \param other entity to be compared with \p this.

    \return true if the entities differ, false otherwise.
  */
  bool
  PolChemDefEntity::operator!=(const PolChemDefEntity &other) const
  {
    int tests = 0;

    Q_ASSERT(mcsp_polChemDef && other.mcsp_polChemDef);
    Q_ASSERT(mcsp_polChemDef.get() && other.mcsp_polChemDef.get());
    tests += (mcsp_polChemDef != other.mcsp_polChemDef);

    Q_ASSERT(!m_name.isEmpty() && !other.m_name.isEmpty());
    tests += (m_name != other.m_name);

    if(tests > 0)
      return true;

    return false;
  }


  bool
  PolChemDefEntity::validate() const
  {
    int tests = 0;

    tests += (m_name.isNull() || m_name.isEmpty());
    tests += (!mcsp_polChemDef);
    tests += (!mcsp_polChemDef.get());

    if(tests)
      return false;

    return true;
  }

} // namespace libmass

} // namespace msxps
