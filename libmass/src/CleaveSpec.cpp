/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "CleaveSpec.hpp"


namespace msxps
{

namespace libmass
{


  //! Constructs a cleavage specification.
  /*!

    \param polChemDef Polymer chemistry definition. Cannot be 0.

    \param name Name. Cannot be empty.

    \param pattern Pattern. Defaults to the null string.
    */
  CleaveSpec::CleaveSpec(PolChemDefCstSPtr polChemDefCstSPtr,
                         QString name,
                         QString pattern)
    : PolChemDefEntity(polChemDefCstSPtr, name), m_pattern(pattern)
  {
  }


  //! Constructs a copy of \p other.
  /*!  \param other cleavage specification to be used as a mold.
   */
  CleaveSpec::CleaveSpec(const CleaveSpec &other)
    : PolChemDefEntity(other), m_pattern(other.m_pattern)
  {
    for(int iter = 0; iter < other.m_motifList.size(); ++iter)
      {
        CleaveMotif *cleaveMotif = new CleaveMotif(*other.m_motifList.at(iter));

        m_motifList.append(cleaveMotif);
      }

    for(int iter = 0; iter < other.m_ruleList.size(); ++iter)
      {
        CleaveRule *cleaveRule = new CleaveRule(*other.m_ruleList.at(iter));

        m_ruleList.append(cleaveRule);
      }
  }


  //! Destroys the cleavage specification.
  CleaveSpec::~CleaveSpec()
  {
    while(!m_motifList.isEmpty())
      delete m_motifList.takeFirst();

    while(!m_ruleList.isEmpty())
      delete m_ruleList.takeFirst();
  }


  //! Modifies \p other to be identical to \p this.
  /*!  \param other cleavage specification.
   */
  void
  CleaveSpec::clone(CleaveSpec *other) const
  {
    CleaveRule *cleaveRule   = 0;
    CleaveMotif *cleaveMotif = 0;

    Q_ASSERT(other);

    PolChemDefEntity::clone(other);

    other->m_pattern = m_pattern;

    while(!other->m_ruleList.isEmpty())
      delete other->m_ruleList.takeFirst();

    for(int iter = 0; iter < m_ruleList.size(); ++iter)
      {
        cleaveRule = new CleaveRule(mcsp_polChemDef, "NOT_SET");

        m_ruleList.at(iter)->clone(cleaveRule);

        other->m_ruleList.append(cleaveRule);
      }

    while(!other->m_motifList.isEmpty())
      delete other->m_motifList.takeFirst();

    for(int iter = 0; iter < m_motifList.size(); ++iter)
      {
        cleaveMotif = new CleaveMotif(mcsp_polChemDef, "NOT_SET");

        m_motifList.at(iter)->clone(cleaveMotif);

        other->m_motifList.append(cleaveMotif);
      }
  }


  //! Modifies \p this  to be identical to \p other.
  /*!  \param other cleavage specification to be used as a mold.
   */
  void
  CleaveSpec::mold(const CleaveSpec &other)
  {
    if(&other == this)
      return;

    CleaveRule *cleaveRule   = 0;
    CleaveMotif *cleaveMotif = 0;

    PolChemDefEntity::mold(other);

    m_pattern = other.m_pattern;

    while(!m_ruleList.isEmpty())
      delete m_ruleList.takeFirst();

    for(int iter = 0; iter < other.m_ruleList.size(); ++iter)
      {
        cleaveRule = new CleaveRule(*other.m_ruleList.at(iter));

        m_ruleList.append(cleaveRule);
      }

    while(!m_motifList.isEmpty())
      delete m_motifList.takeFirst();

    for(int iter = 0; iter < other.m_motifList.size(); ++iter)
      {
        cleaveMotif = new CleaveMotif(*other.m_motifList.at(iter));

        m_motifList.append(cleaveMotif);
      }
  }


  //! Assigns other to \p this cleavage specification.
  /*! \param other cleavage specification used as the mold to set values
    to \p this instance.

    \return a reference to \p this specification.
    */
  CleaveSpec &
  CleaveSpec::operator=(const CleaveSpec &other)
  {
    if(&other != this)
      mold(other);

    return *this;
  }


  //! Sets the pattern.
  /*!  \param str new pattern.
   */
  void
  CleaveSpec::setPattern(const QString &str)
  {
    m_pattern = str;
  }


  //! Returns the pattern.
  /*!  \return the pattern.
   */
  const QString &
  CleaveSpec::pattern()
  {
    return m_pattern;
  }


  //! Returns the list of motifs.
  /*!  \return the list of motifs.
   */
  QList<CleaveMotif *> *
  CleaveSpec::motifList()
  {
    return &m_motifList;
  }


  //! Returns the list of cleavage rules.
  /*!  \return the list of cleavage rules.
   */
  QList<CleaveRule *> *
  CleaveSpec::ruleList()
  {
    return &m_ruleList;
  }


  //! Searches a cleavage specification in a list.
  /*! Searches for a cleavage specification having a name identical to
    argument \p str in \p refList. If such cleavage specification is
    found, and if \p other is non-0, the found cleavage specification's
    data are copied into \p other.

    \param str name.

    \param refList list of cleavage specifications.

    \param other cleavage specification to be updated with data in
    the found cleavage specification.  Defaults to 0, in which case
    no update occurs.

    \return the int index of the found cleavage specification or -1
    if none is found or if \p str is empty.
    */
  int
  CleaveSpec::isNameInList(const QString &str,
                           const QList<CleaveSpec *> &refList,
                           CleaveSpec *other)
  {
    CleaveSpec *cleaveSpec = 0;

    if(str.isEmpty())
      return -1;

    for(int iter = 0; iter < refList.size(); ++iter)
      {
        cleaveSpec = refList.at(iter);
        Q_ASSERT(cleaveSpec);

        if(cleaveSpec->m_name == str)
          {
            if(other)
              cleaveSpec->clone(other);

            return iter;
          }
      }

    return -1;
  }


  //! Parses the cleavage specification.
  /*! The parsing of the cleavage specification is performed to ensure
    that the pattern is correct.

    Starting from a pattern "Lys/;Arg/;-Lys/Pro", the parsing would
    first split it into three site strings:

    \li "Lys/";
    \li "Arg/";
    \li "-Lys/Pro"

    Each of these site strings will be deconstructed into motifs, stored
    in CleaveMotif objects:

    \li First motif has a code list "[0] Lys", an offset of 1 and is for
    cleave;

    \li Second motif has a code list "[0] Arg", an offset of 1 and is for
    cleave;

    \li Third motif has a code list "[0] Lys, [1] Pro", an offset of 1
    and is not for cleave(see the '-' ?)


    \return true if parsing is successful, false otherwise.
    */
  bool
  CleaveSpec::parse()
  {
    if(m_pattern.isEmpty())
      return false;

    // The 'm_pattern' is a ';'-delimited string, in which each
    // sub-string is a 'site'. Each site is in turn constituted by a
    // motif and a '/' that indicates where the motif is actually
    // cleaved.
    //
    // For example the "-Lys/Pro" site is actually a motif of sequence
    // "LysPro" and the site holds two more informations with respect to
    // the mere motif: it says that the motif should not be cleaved
    //('-') and that if the '-' were not there, the cleavage would
    // occur between the Lys and the Pro('/' symbolizes the cleavage).

    // For example, if the cleavespec had a "Lys/;Arg/;-Lys/Pro" string,
    // it would be split into 3 strings: "Lys/" and "Arg/" and
    // "-Lys/Pro"(these are 'site' strings). These three site string
    // would further be deconstructed into motif string(removal of '-'
    // and '/' characters). Where would these 3 motif strings be stored?
    // They would be set into one cleavemotif instance for each
    // motif. Thus, for example, "-Lys/Pro" would yield a cleavemotif of
    // 'motif' LysPro, with a FALSE cleave member and a 1 offset member.

    // Will return the number of cleavemotif instances that were created.
    // Upon error -1 is returned.


    // "Lys/;Arg/;-Lys/Pro" --> [0] Lys/, [1] Arg/, [2] -Lys/Pro
    QStringList sites = m_pattern.split(";", Qt::SkipEmptyParts);

    //   for (int iter = 0; iter < sites.size() ; ++iter)
    //     qDebug() << __FILE__ << __LINE__
    // 	      << sites.at(iter);

    bool isForCleave = true;

    for(int iter = 0; iter < sites.size(); ++iter)
      {
        isForCleave = true;

        QString currSite = sites.at(iter);

        if(currSite.length() < 2)
          {
            qDebug() << __FILE__ << __LINE__ << currSite
                     << "is an invalid cleavage site";
            return false;
          }

        int count = currSite.count(QChar('/'));
        if(count != 1)
          {
            qDebug() << __FILE__ << __LINE__
                     << "The must be one and only one '/' "
                        "in the cleavage site";
            return false;
          }

        // Remove spaces.
        currSite.remove(QRegularExpression("\\s+"));

        // Is there a '-' in the site string indicating that this site
        // is NOT for cleavage? If so, there should be only one such
        // sign and in position 0.

        count = currSite.count(QChar('-'));
        if(count > 1)
          return false;
        else if(count == 1)
          {
            if(currSite.indexOf(QChar('-'), 0, Qt::CaseInsensitive) != 0)
              return false;

            isForCleave = false;

            // We can remove the '-' character, as we now know
            // that the site is NOT for cleavage.
            currSite.remove(0, 1);
          }
        // else: site is for cleavage: no '-' found.


        // We can create a new cleavage motif.
        CleaveMotif *motif = new CleaveMotif(mcsp_polChemDef, "NOT_SET");

        motif->setForCleave(isForCleave);

        if(motif->parse(currSite) == -1)
          {
            qDebug() << __FILE__ << __LINE__ << "Failed to parse site"
                     << currSite;

            delete motif;

            return false;
          }

        //       qDebug() << __FILE__ << __LINE__
        // 		<< "Appending motif" << motif->motif();

        // Append the newly created motif to the list.
        m_motifList.append(motif);
      }

    return true;
  }


  //! Validates the cleavage specification.
  /*!  \return true if validation is successful, false otherwise.
   */
  bool
  CleaveSpec::validate()
  {
    if(m_name.isEmpty())
      return false;

    if(m_pattern.isEmpty())
      return false;


    if(!parse())
      return false;

    // If there are rules, we have to check them all.

    for(int iter = 0; iter < m_ruleList.size(); ++iter)
      {
        if(!m_ruleList.at(iter)->validate())
          return false;
      }

    return true;
  }


  //! Parses a cleavage specification XML element.
  /*! Parses the cleavage specification XML element passed as argument
    and sets its data to \p this cleavage specification(this is called
    XML rendering).

    \param element XML element to be parsed and rendered.

    \return true if parsing and formula validation were successful,
    false otherwise.

    \sa formatXmlClsElement(int offset, const QString &indent).
    */
  bool
  CleaveSpec::renderXmlClsElement(const QDomElement &element, int version)
  {
    // For the time being the version is not necessary here. As of
    // version up to 2, the current function works ok.

    if(version == 1)
      {
        // NoOp
        version = 1;
      }

    QDomElement child;
    QDomElement childRule;

    CleaveRule *cleaveRule = 0;

    /* The xml node we are in is structured this way:
     *
     * <cls>
     *   <name>CyanogenBromide</name>
     *   <pattern>M/</pattern>
     *   <clr>
     *     <le-mnm-code>M</le-mnm-code>
     *     <le-formula>-C1H2S1+O1</le-formula>
     *     <re-mnm-code>M</re-mnm-code>
     *     <re-formula>-C1H2S1+O1</re-formula>
     *   </clr>
     * </cls>
     *
     * And the element parameter points to the
     *
     * <cls> element tag:
     *  ^
     *  |
     *  +----- here we are right now.
     *
     * Which means that element.tagName() == "cls" and that
     * we'll have to go one step down to the first child of the
     * current node in order to get to the <name> element.
     */

    if(element.tagName() != "cls")
      return false;

    child = element.firstChildElement("name");

    if(child.isNull())
      return false;

    m_name = child.text();
    //   qDebug() << __FILE__ << __LINE__
    // 	    << "name is" << m_name;

    child = child.nextSiblingElement("pattern");

    if(child.isNull())
      return false;

    m_pattern = child.text();

    // At this point there might be 0, 1 or more cleavage rules.
    child = child.nextSiblingElement("clr");

    while(!child.isNull())
      {
        cleaveRule = new CleaveRule(mcsp_polChemDef, "NOT_SET");

        if(!cleaveRule->renderXmlClrElement(child, version))
          {
            delete cleaveRule;
            return false;
          }

        if(!cleaveRule->validate())
          {
            delete cleaveRule;
            return false;
          }

        m_ruleList.append(cleaveRule);

        child = child.nextSiblingElement("clr");
      }

    if(!validate())
      return false;

    return true;
  }


  //! Formats a string suitable to use as an XML element.
  /*! Formats a string suitable to be used as an XML element in a
    polymer chemistry definition file. The typical cleavage
    specification element that is generated in this function looks like
  this:

  \verbatim
  <cls>
  <name>CyanogenBromide</name>
  <pattern>M/</pattern>
  <clr>
  <re-mnm-code>M</re-mnm-code>
  <re-formula>-CH2S+O</re-formula>
  </clr>
  </cls>
  \endverbatim

  \param offset times the \p indent string must be used as a lead in the
  formatting of elements.

  \param indent string used to create the leading space that is placed
  at the beginning of indented XML elements inside the XML
  element. Defaults to two spaces(QString(" ")).

  \return a dynamically allocated string that needs to be freed after
  use.

  \sa renderXmlClsElement(const QDomElement &element).
  */
  QString *
  CleaveSpec::formatXmlClsElement(int offset, const QString &indent)
  {

    int newOffset;
    int iter = 0;

    QString lead("");
    QString *string = new QString();


    // Prepare the lead.
    newOffset = offset;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }


    *string += QString("%1<cls>\n").arg(lead);

    // Prepare the lead.
    ++newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }

    // Continue with indented elements.

    *string += QString("%1<name>%2</name>\n").arg(lead).arg(m_name);

    *string += QString("%1<pattern>%2</pattern>\n").arg(lead).arg(m_pattern);

    for(int iter = 0; iter < m_ruleList.size(); ++iter)
      {
        QString *ruleString =
          m_ruleList.at(iter)->formatXmlClrElement(newOffset);

        *string += *ruleString;

        delete ruleString;
      }

    // Prepare the lead for the closing element.
    --newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }

    *string += QString("%1</cls>\n").arg(lead);

    return string;
  }

} // namespace libmass

} // namespace msxps
