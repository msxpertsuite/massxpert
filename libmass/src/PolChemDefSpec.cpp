/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#include "PolChemDefSpec.hpp"


/////////////////////// Qt includes
#include <QDir>


namespace msxps
{

namespace libmass
{


  PolChemDefSpec::PolChemDefSpec()
  {
    return;
  }


  PolChemDefSpec::~PolChemDefSpec()
  {
    return;
  }


  void
  PolChemDefSpec::setName(const QString &name)
  {
    m_name = name;
  }


  const QString &
  PolChemDefSpec::name() const
  {
    return m_name;
  }


  void
  PolChemDefSpec::setFilePath(const QString &filePath)
  {
    m_filePath = filePath;
  }


  const QString &
  PolChemDefSpec::filePath() const
  {
    return m_filePath;
  }


  QString
  PolChemDefSpec::dirPath()
  {
    // if m_filePath is "/usr/share/massxpert/polChemDefs/protein.xml",
    // returns "/usr/share/massxpert/polChemDefs"
    QFileInfo fileInfo(m_filePath);
    QDir dir(fileInfo.dir());
    return dir.absolutePath();
  }


} // namespace libmass

} // namespace msxps
