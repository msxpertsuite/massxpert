/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "IonizeRule.hpp"


namespace msxps
{

namespace libmass
{


  //! Constructs an ionization rule.
  /*! The \c m_charge is set to 0 and \c m_level member is set to
    0. The \c m_isValid member is set to false. The fact that the
    ionization rule is initialized with level 0 and charge 0 is
    critical to some parts of the code that rely on these values to
    determine if ionization using this ionization rule is to be
    performed or not(that is if the IonizeRule is valid or not).
  */
  IonizeRule::IonizeRule() : m_charge(0), m_level(0)
  {
    m_isValid = false;
  }


  //! Constructs a copy of \p other.
  /*! The IonizeRule that is constructed in this manner should not be
    considered valid, as the template might be invalid(\c m_charge
    = 0, or \c m_isValid = false). To ascertain validity either
    check the validity of the template or call validate after copy
    construction.

    \param other ionization rule to be used as a mold.
  */
  IonizeRule::IonizeRule(const IonizeRule &other)
    : Formula(other),
      m_charge(other.m_charge),
      m_level(other.m_level),
      m_isValid(other.m_isValid)
  {
  }


  //! Creates a new ionization rule.
  /*! The newly created ionization rule is initialized using \c this.

    \return The newly created ionization rule, which should be deleted
    when no longer in use.
  */
  IonizeRule *
  IonizeRule::clone() const
  {
    IonizeRule *other = new IonizeRule(*this);

    return other;
  }


  //! Modifies \p other to be identical to \p this.
  /*!

    \param other ionization rule.
  */
  void
  IonizeRule::clone(IonizeRule *other) const
  {
    Q_ASSERT(other);

    if(other == this)
      return;

    Formula::clone(other);

    other->m_charge = m_charge;
    other->m_level  = m_level;

    other->m_isValid = m_isValid;
  }


  //! Modifies \p this  to be identical to \p other.
  /*!  \param other ionization rule to be used as a mold.
   */
  void
  IonizeRule::mold(const IonizeRule &other)
  {
    if(&other == this)
      return;

    Formula::mold(other);

    m_charge = other.m_charge;
    m_level  = other.m_level;

    m_isValid = other.m_isValid;
  }


  //! Assigns \p other to \c this ionization rule.
  /*!

    \param other ionization rule used as the mold to set values to \p
    this.

    \return a reference to \c this ionization rule.
  */
  IonizeRule &
  IonizeRule::operator=(const IonizeRule &other)
  {
    if(&other != this)
      mold(other);

    return *this;
  }


  //! Sets the charge.
  /*! An IonizeRule with a(charge <= 0) is invalid by definition. If
    so, the m_isValid member is set to false.

    \param value New charge.
  */
  void
  IonizeRule::setCharge(int value)
  {
    m_charge = value;

    if(m_charge <= 0)
      m_isValid = false;
  }


  //! Returns the charge.
  /*!
    \return the charge.
  */
  int
  IonizeRule::charge() const
  {
    return m_charge;
  }


  //! Set the level.
  /*! An IonizeRule might have an ionization(level == 0 but not level
    < 0), as this means that the analyte is not ionized at all. If
    (level < 0), then \c m_isValid is set to false.

    \param value New level.
  */
  void
  IonizeRule::setLevel(int value)
  {
    m_level = value;

    if(m_level < 0)
      m_isValid = false;
  }


  //! Returns the ionization level.
  /*!
    \return the level.
  */
  int
  IonizeRule::level() const
  {
    return m_level;
  }


  QString
  IonizeRule::formula() const
  {
    return Formula::text();
  }


  bool
  IonizeRule::operator==(const IonizeRule &other) const
  {
    int tests = 0;

    tests += Formula::operator==(other);
    tests += (m_charge == other.m_charge);
    tests += (m_level == other.m_level);
    tests += (m_isValid == other.m_isValid);

    if(tests < 4)
      return false;

    return true;
  }


  bool
  IonizeRule::operator!=(const IonizeRule &other) const
  {
    int tests = 0;

    tests += Formula::operator!=(other);
    tests += (m_charge != other.m_charge);
    tests += (m_level != other.m_level);
    tests += (m_isValid != other.m_isValid);

    if(tests > 0)
      return true;

    return false;
  }


  //! Validates the IonizeRule.
  /*! An IonizeRule is valid if it generates a m/z ratio after
      ionization(or deionization if (\c m_level == 0) of the analyte
      that is different than the previous and if its Formula
      validates.  This means that the following should be true:

      \li The Formula should be valid(that is should contain at least
      one atom(use a 0-weighing atom if necessary, like Nul from the
      polymer chemistry definitions shipped with the software);

      \li The charge should > 0;

      \li The level should >= 0;

      If these three tests do not fail, the IonizeRule is considered
      valid and the m_isValid boolean value is set to true; false
      otherwise.

      \param refList List of Atom objects to be used for the formula
      validation.

      \return true if validation succeeds, false otherwise.

      \sa Formula::validate()
  */
  bool
  IonizeRule::validate(const QList<Atom *> &refList)
  {
    int tests = 0;

    tests += Formula::validate(refList);
    tests += (m_charge > 0);
    tests += (m_level >= 0);

    if(tests < 3)
      {
        m_isValid = false;

        return false;
      }

    m_isValid = true;

    return true;
  }


  //! Returns the validity.
  /*! The validity that is returned is according to a member boolean
     (m_isValid) that is set after a call to validate(const
      QList<Atom *> &refList). If the formula or the charge/level
      values are changed after that call, the formula might be
      invalid.

      \return the validity as a boolean true/false value.

      \sa validate(const QList<Atom *> &refList)
  */
  bool
  IonizeRule::isValid() const
  {
    return m_isValid;
  }


  //! Renders an ionization rule XML element.
  /*! The XML element is parsed and the data extracted from the XML data
    are set to \p this instance.

    The DTD says this: <!ELEMENT ionizerule(formula,charge,level)>

    A typical ionization rule element looks like this:

    \verbatim
    <ionizerule>
    <formula>+H</formula>
    <charge>1</charge>
    <level>1</level>
    </ionizerule>
    \endverbatim

    \param element XML element to be parsed and rendered.

    Note that \c this IonizeRule is not valid, as it has not been
    validated by calling validate(). The caller is reponsible for
    checking the validity of the IonizeRule prior use.

    \return true if the parsing is successful, false otherwise.

    \sa formatXmlIonizeRuleElement(int offset, const QString &indent).
  */
  bool
  IonizeRule::renderXmlIonizeRuleElement(const QDomElement &element)
  {
    QDomElement child;

    //   <ionizerule>
    //     <formula>+H</formula>
    //     <charge>1</charge>
    //     <level>1</level>
    //   </ionizerule>

    if(element.tagName() != "ionizerule")
      return false;

    // <formula>
    child = element.firstChildElement();
    if(child.tagName() != "formula")
      return false;
    if(!renderXmlFormulaElement(child))
      return false;

    // <charge>
    child = child.nextSiblingElement();
    if(child.tagName() != "charge")
      return false;
    bool ok  = false;
    m_charge = child.text().toInt(&ok);
    if(!m_charge && !ok)
      return false;

    // <level>
    child = child.nextSiblingElement();
    if(child.tagName() != "level")
      return false;
    ok      = false;
    m_level = child.text().toInt(&ok);
    if(!m_level && !ok)
      return false;

    // We have not validated this IonizeRule, as we should have the
    // reference list of atoms to do that. The caller is responsible
    // for the validate() call.
    m_isValid = false;

    return true;
  }


  //! Formats a string suitable to use as an XML element.
  /*! Formats a string suitable to be used as an XML element in a
    polymer chemistry definition file. The typical ionization rule
    element that is generated in this function looks like this:

    The DTD says this: <!ELEMENT ionizerule(formula,charge,level)>

    A typical ionization rule element looks like this:

    \verbatim

    <ionizerule>
    ~~<formula>+H</formula>
    ~~<charge>1</charge>
    ~~<level>1</level>
    </ionizerule>

    \endverbatim

    \param offset times the \p indent string must be used as a lead in the
    formatting of elements.

    \param indent string used to create the leading space that is placed
    at the beginning of indented XML elements inside the XML
    element. Defaults to two spaces(QString(" ")).

    \return a dynamically allocated string that needs to be freed after
    use.

    \sa renderXmlIonizeRuleElement(const QDomElement &element).
  */
  QString *
  IonizeRule::formatXmlIonizeRuleElement(int offset, const QString &indent)
  {
    int newOffset;
    int iter = 0;

    QString lead("");
    QString *string = new QString();


    // Prepare the lead.
    newOffset = offset;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }

    /* We are willing to create an <ionizerule> node that should look like this:
     *
     *<ionizerule>
     *  <formula>+H</formula>
     *  <charge>1</charge>
     *  <level>1</level>
     *</ionizerule>
     *
     */

    *string += QString("%1<ionizerule>\n").arg(lead);

    // Prepare the lead.
    ++newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }

    // Continue with indented elements.

    *string += QString("%1<formula>%2</formula>\n").arg(lead).arg(formula());

    *string += QString("%1<charge>%2</charge>\n").arg(lead).arg(m_charge);

    *string += QString("%1<level>%2</level>\n").arg(lead).arg(m_level);

    // Prepare the lead for the closing element.
    --newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }

    *string += QString("%1</ionizerule>\n").arg(lead);

    return string;
  }


  void
  IonizeRule::debugPutStdErr()
  {
    qDebug() << __FILE__ << __LINE__
             << QString("Ionizerule: charge=%1; level=%2; formula=%3")
                  .arg(m_charge)
                  .arg(m_level)
                  .arg(m_formula);
  }

} // namespace libmass

} // namespace msxps
