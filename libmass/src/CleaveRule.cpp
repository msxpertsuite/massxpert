/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "CleaveRule.hpp"
#include "PolChemDef.hpp"


namespace msxps
{

namespace libmass
{


  //! Construct a cleavage rule.
  /*!
    \param polChemDef Polymer chemistry definition. Cannot be 0.

    \param name Name. Cannot be empty.

    \param leftCode Left code. Defaults to the null string.

    \param leftFormula Left formula. Defaults to the null string.

    \param rightCode Right code. Defaults to the null string.

    \param rightFormula Right formula. Defaults to the null string.
  */
  CleaveRule::CleaveRule(PolChemDefCstSPtr polChemDefCstSPtr,
                         QString name,
                         QString leftCode,
                         QString leftFormula,
                         QString rightCode,
                         QString rightFormula)
    : PolChemDefEntity(polChemDefCstSPtr, name),
      m_leftCode(leftCode),
      m_leftFormula(leftFormula),
      m_rightCode(rightCode),
      m_rightFormula(rightFormula)
  {
  }


  //! Constructs a copy of \p other.
  /*!  \param other cleavage rule to be used as a mold.
   */
  CleaveRule::CleaveRule(const CleaveRule &other)
    : PolChemDefEntity(other),
      m_leftCode(other.m_leftCode),
      m_leftFormula(other.m_leftFormula),
      m_rightCode(other.m_rightCode),
      m_rightFormula(other.m_rightFormula)
  {
  }


  //! Destroys the cleavage rule.
  CleaveRule::~CleaveRule()
  {
  }


  //! Modifies \p other to be identical to \p this.
  /*!  \param other cleavage rule.
   */
  void
  CleaveRule::clone(CleaveRule *other)
  {
    Q_ASSERT(other);

    PolChemDefEntity::clone(other);

    other->m_leftCode = m_leftCode;
    m_leftFormula.clone(&other->m_leftFormula);

    other->m_rightCode = m_rightCode;
    m_rightFormula.clone(&other->m_rightFormula);
  }


  //! Modifies \p this  to be identical to \p other.
  /*!  \param other cleavage rule to be used as a mold.
   */
  void
  CleaveRule::mold(const CleaveRule &other)
  {
    if(&other == this)
      return;

    m_leftCode = other.m_leftCode;
    m_leftFormula.mold(other.m_leftFormula);

    m_rightCode = other.m_rightCode;
    m_rightFormula.mold(other.m_rightFormula);
  }


  //! Assigns other to \p this cleavage rule.
  /*! \param other cleavage rule used as the mold to set values to \p this
    instance.

    \return a reference to \p this cleavage rule.
  */
  CleaveRule &
  CleaveRule::operator=(const CleaveRule &other)
  {
    if(&other != this)
      mold(other);

    return *this;
  }


  //! Sets the left code.
  /*!  \param str new code.
   */
  void
  CleaveRule::setLeftCode(const QString &str)
  {
    m_leftCode = str;
  }


  //! Returns the left code.
  /*!  \return the left code.
   */
  const QString &
  CleaveRule::leftCode()
  {
    return m_leftCode;
  }


  //! Sets the right code.
  /*!  \param str new code.
   */
  void
  CleaveRule::setRightCode(const QString &str)
  {
    m_rightCode = str;
  }


  //! Returns the right code.
  /*!  \return the right code.
   */
  const QString &
  CleaveRule::rightCode()
  {
    return m_rightCode;
  }


  //! Sets the left formula.
  /*!  \param str new formula.
   */
  void
  CleaveRule::setLeftFormula(const Formula &str)
  {
    m_leftFormula = str;
  }


  //! Returns the left formula.
  /*!  \return the left formula.
   */
  const Formula &
  CleaveRule::leftFormula()
  {
    return m_leftFormula;
  }


  //! Sets the right formula.
  /*!  \param str new formula.
   */
  void
  CleaveRule::setRightFormula(const Formula &str)
  {
    m_rightFormula = str;
  }


  //! Returns the right formula.
  /*!  \return the right formula.
   */
  const Formula &
  CleaveRule::rightFormula()
  {
    return m_rightFormula;
  }


  int
  CleaveRule::isNameInList(const QString &str,
                           const QList<CleaveRule *> &refList,
                           CleaveRule *other)
  {
    CleaveRule *cleaveRule = 0;

    if(str.isEmpty())
      return -1;

    for(int iter = 0; iter < refList.size(); ++iter)
      {
        cleaveRule = refList.at(iter);
        Q_ASSERT(cleaveRule);

        if(cleaveRule->m_name == str)
          {
            if(other)
              cleaveRule->clone(other);

            return iter;
          }
      }

    return -1;
  }


  //! Validates the cleavage rule.
  /*!  \return true if validation is successful, false otherwise.
   */
  bool
  CleaveRule::validate()
  {
    const QList<Monomer *> &monomerRefList = mcsp_polChemDef->monomerList();
    const QList<Atom *> &atomRefList       = mcsp_polChemDef->atomList();

    if(!m_leftCode.isEmpty())
      {
        if(Monomer::isCodeInList(m_leftCode, monomerRefList) == -1)
          return false;

        if(m_leftFormula.text().isEmpty())
          return false;

        if(!m_leftFormula.validate(atomRefList))
          return false;
      }

    if(!m_rightCode.isEmpty())
      {
        if(Monomer::isCodeInList(m_rightCode, monomerRefList) == -1)
          return false;

        if(m_rightFormula.text().isEmpty())
          return false;

        if(!m_rightFormula.validate(atomRefList))
          return false;
      }

    return true;
  }


  //! Parses a cleavage rule XML element.
  /*! Parses the cleavage rule XML element passed as argument and sets
    its data to \p this cleavage rule(this is called XML
    rendering).

    \param element XML element to be parsed and rendered.

    \return true if parsing and validation were successful, false
    otherwise.

    \sa formatXmlClrElement(int offset, const QString &indent).
  */
  bool
  CleaveRule::renderXmlClrElement(const QDomElement &element, int version)
  {
    QDomElement child;

    bool leftCodeSet     = false;
    bool leftFormulaSet  = false;
    bool rightCodeSet    = false;
    bool rightFormulaSet = false;

    /* The xml node we are in is structured this way:
     *
     *  <clr>
     *     <name>Homeseryl</name>
     *     <le-mnm-code>M</le-mnm-code>
     *     <le-formula>-C1H2S1+O1</le-formula>
     *     <re-mnm-code>M</re-mnm-code>
     *     <re-formula>-C1H2S1+O1</re-formula>
     *  </clr>
     *
     * And the element parameter points to the
     *
     * <clr> element tag:
     *  ^
     *  |
     *  +----- here we are right now.
     *
     * Which means that xml_node->name == "clr" and that
     * we'll have to go one step down to the first child of the
     * current node in order to get to the <code> element.
     *
     * Note that the DTD stipulates that there can be no or one at most
     * of each left end and/or right end set of data. So be careful
     * with the assertions !
     * This is the DTD material:
     * <!ELEMENT clr((le-mnm-code,le-formula)?,
     *(re-mnm-code,re-formula)?)>
     */

    if(element.tagName() != "clr")
      return false;

    child = element.firstChildElement();

    // If version is at least 3, then the next element has to be "name".
    if(version >= 3)
      {
        if(child.tagName() != "name")
          return false;

        m_name = child.text();

        child = child.nextSiblingElement();
      }

    // It cannot be that there is nothing in this element!
    while(!child.isNull())
      {
        // OK, apparently there is a child element, so let's try to see
        // what's going on. It can either be "le-mnm-code" or "re-mnm-code".

        if(child.tagName() == "le-mnm-code")
          {
            m_leftCode  = child.text();
            leftCodeSet = true;
          }
        else if(child.tagName() == "le-formula")
          {
            m_leftFormula.setFormula(child.text());
            leftFormulaSet = true;
          }
        else if(child.tagName() == "re-mnm-code")
          {
            m_rightCode  = child.text();
            rightCodeSet = true;
          }
        else if(child.tagName() == "re-formula")
          {
            m_rightFormula.setFormula(child.text());
            rightFormulaSet = true;
          }

        child = child.nextSiblingElement();
      }

    // OK, we just finished parsing this <clr> element. Check what we
    // got.

    if(leftCodeSet)
      {
        if(!leftFormulaSet)
          return false;
      }

    if(rightCodeSet)
      {
        if(!rightFormulaSet)
          return false;
      }

    // It cannot be that no single code could be set.
    if(!leftCodeSet && !rightCodeSet)
      return false;

    if(!validate())
      return false;

    return true;
  }


  //! Formats a string suitable to use as an XML element.
  /*! Formats a string suitable to be used as an XML element in a
    polymer chemistry definition file. The typical cleavage rule element
    that is generated in this function looks like this:

    \verbatim
    <clr>
    <re-mnm-code>M</re-mnm-code>
    <re-formula>-CH2S+O</re-formula>
    </clr>
    \endverbatim

    \param offset times the \p indent string must be used as a lead in the
    formatting of elements.

    \param indent string used to create the leading space that is placed
    at the beginning of indented XML elements inside the XML
    element. Defaults to two spaces(QString(" ")).

    \return a dynamically allocated string that needs to be freed after
    use.

    \sa renderXmlClrElement(const QDomElement &element).
  */
  QString *
  CleaveRule::formatXmlClrElement(int offset, const QString &indent)
  {

    int newOffset;
    int iter = 0;

    QString lead("");
    QString *string = new QString();


    // Prepare the lead.
    newOffset = offset;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }

    /*
      <clr>
      <re-mnm-code>M</re-mnm-code>
      <re-formula>-CH2S+O</re-formula>
      </clr>
    */

    *string += QString("%1<clr>\n").arg(lead);

    // Prepare the lead.
    ++newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }

    // Continue with indented elements.

    *string += QString("%1<name>%2</name>\n").arg(lead).arg(m_name);

    if(!m_leftCode.isEmpty())
      {
        Q_ASSERT(!m_leftFormula.text().isEmpty());

        *string += QString("%1<le-mnm-code>%2</le-mnm-code>\n")
                     .arg(lead)
                     .arg(m_leftCode);

        *string += QString("%1<le-formula>%2</le-formula>\n")
                     .arg(lead)
                     .arg(m_leftFormula.text());
      }

    if(!m_rightCode.isEmpty())
      {
        Q_ASSERT(!m_rightFormula.text().isEmpty());

        *string += QString("%1<re-mnm-code>%2</re-mnm-code>\n")
                     .arg(lead)
                     .arg(m_rightCode);

        *string += QString("%1<re-formula>%2</re-formula>\n")
                     .arg(lead)
                     .arg(m_rightFormula.text());
      }

    // Prepare the lead for the closing element.
    --newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
        lead += indent;
        ++iter;
      }

    *string += QString("%1</clr>\n").arg(lead);

    return string;
  }

} // namespace libmass

} // namespace msxps
