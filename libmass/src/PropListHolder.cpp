/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes


/////////////////////// Local includes
#include "PropListHolder.hpp"


namespace msxps
{

namespace libmass
{


  PropListHolder::PropListHolder()
  {
  }


  PropListHolder::PropListHolder(const PropListHolder &other)
  {
    for(int iter = 0; iter < other.m_propList.size(); ++iter)
      {
        Prop *prop = other.m_propList.at(iter);
        Q_ASSERT(prop);

        void *newProp = static_cast<void *>(prop->clone());
        Q_ASSERT(newProp);

        appendProp(static_cast<Prop *>(newProp));
      }
  }


  PropListHolder::~PropListHolder()
  {
    while(!m_propList.isEmpty())
      delete(m_propList.takeFirst());
  }


  PropListHolder *
  PropListHolder::clone() const
  {
    PropListHolder *other = new PropListHolder(*this);

    return other;
  }


  void
  PropListHolder::clone(PropListHolder *other) const
  {
    Q_ASSERT(other);

    while(!other->m_propList.isEmpty())
      delete(other->m_propList.takeFirst());

    for(int iter = 0; iter < m_propList.size(); ++iter)
      {
        Prop *prop = m_propList.at(iter);
        Q_ASSERT(prop);

        void *newProp = static_cast<void *>(prop->clone());
        Q_ASSERT(newProp);

        other->appendProp(static_cast<Prop *>(newProp));
      }
  }


  void
  PropListHolder::mold(const PropListHolder &other)
  {


    while(!m_propList.isEmpty())
      delete(m_propList.takeFirst());

    for(int iter = 0; iter < other.m_propList.size(); ++iter)
      {
        Prop *prop = other.m_propList.at(iter);
        Q_ASSERT(prop);

        void *newProp = static_cast<void *>(prop->clone());
        Q_ASSERT(newProp);

        appendProp(static_cast<Prop *>(newProp));
      }
  }


  PropListHolder &
  PropListHolder::operator=(const PropListHolder &other)
  {
    if(&other != this)
      mold(other);

    return *this;
  }


  const QList<Prop *> &
  PropListHolder::propList() const
  {
    return m_propList;
  }


  QList<Prop *> &
  PropListHolder::propList()
  {
    return m_propList;
  }


  Prop *
  PropListHolder::prop(const QString &name, int *index)
  {
    for(int iter = 0; iter < m_propList.size(); ++iter)
      {
        Prop *localProp = m_propList.at(iter);
        Q_ASSERT(localProp);

        if(localProp->name() == name)
          {
            if(index)
              *index = iter;

            return localProp;
          }
      }

    return 0;
  }


  int
  PropListHolder::propIndex(const QString &name, Prop *prop)
  {
    for(int iter = 0; iter < m_propList.size(); ++iter)
      {
        Prop *localProp = m_propList.at(iter);
        Q_ASSERT(localProp);

        if(localProp->name() == name)
          {
            if(prop)
              prop = localProp;

            return iter;
          }
      }

    return -1;
  }


  bool
  PropListHolder::appendProp(Prop *prop)
  {
    m_propList.append(prop);

    return true;
  }


  bool
  PropListHolder::removeProp(Prop *prop)
  {
    if(m_propList.removeAll(prop))
      return true;

    return false;
  }


  bool
  PropListHolder::removeProp(const QString &name)
  {
    for(int iter = 0; iter < m_propList.size(); ++iter)
      {
        if(m_propList.at(iter)->name() == name)
          {
            m_propList.removeAt(iter);

            return true;
          }
      }

    return false;
  }

} // namespace libmass

} // namespace msxps
