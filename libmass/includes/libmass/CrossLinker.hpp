/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CROSS_LINKER_HPP
#define CROSS_LINKER_HPP
//#warning "Entering CROSS_LINKER_HPP"


/////////////////////// Local includes
#include "PolChemDefEntity.hpp"
#include "Formula.hpp"
#include "Modif.hpp"


namespace msxps
{


namespace libmass
{


  class CrossLinker : public PolChemDefEntity, public Formula, public Ponderable
  {
    protected:
    // We do not own the modifications below, the pointers refer to
    // modifications in the polymer chemistry definition.
    QList<Modif *> m_modifList;

    public:
    CrossLinker(PolChemDefCstSPtr, const QString &, const QString &);

    CrossLinker(const CrossLinker &);

    ~CrossLinker();

    CrossLinker *clone() const;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    void clone(CrossLinker *) const;
#pragma clang diagnostic pop

    bool setModifAt(Modif *, int);
    bool appendModif(Modif *);
    const Modif *modifAt(int) const;
    bool removeModifAt(int);

    QString formula() const;

    QList<Modif *> &modifList();

    int hasModif(const QString &);

    virtual bool operator==(const CrossLinker &) const;

    int isNameKnown();
    static int isNameInList(const QString &,
                            const QList<CrossLinker *> &,
                            CrossLinker * = 0);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    bool validate();
    virtual bool accountMasses(double * = 0, double * = 0, int = 1);
    virtual bool accountMasses(Ponderable *, int = 1);
#pragma clang diagnostic pop

    bool calculateMasses();

    bool renderXmlClkElement(const QDomElement &, int);
    bool renderXmlClkElement(const QDomElement &);
    bool renderXmlClkElementV3(const QDomElement &);

    QString *formatXmlClkElement(int, const QString & = QString("  "));
  };

} // namespace libmass

} // namespace msxps


#endif // CROSS_LINKER_HPP
