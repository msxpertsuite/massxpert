/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CLEAVE_MOTIF_HPP
#define CLEAVE_MOTIF_HPP


/////////////////////// Qt includes
#include <QString>
#include <QStringList>
#include <QList>


/////////////////////// Local includes
#include "PolChemDefEntity.hpp"
#include "Monomer.hpp"


namespace msxps
{

namespace libmass
{


  //! The CleaveMotif class provides cleavage motifs.
  /*! When a polymer sequence cleavage occurs, using for example the
    specification "Lys/;Arg/;-Lys/Pro", a number of actions need be
    performed prior to listing the oligomers obtained following
    cleavage.

    The "Lys/;Arg/;-Lys/Pro" specification gets crunched in a number of
    steps and motifs are generated for it. In this specific case we'll
    have three motifs with the following data:

    - First motif:
    - "Lys/"
    - code list [0] = "Lys"
    - offset = 1('/' indicates that cut is right of monomer)
    - is for cleavage ? = true

    - Second motif:
    - "Arg/"
    - code list [0] = "Arg"
    - offset = 1('/' indicates that cut is right of monomer)
    - is for cleavage ? = true

    - Third motif:
    - "-Lys/Pro"
    - code list [0] = "Lys", [1] = "Pro"
    - offset = 1('/' indicates that cut is right of monomer)
    - is for cleavage ? = false

    Thanks to this deconstruction(from "Lys/;Arg/;-Lys/Pro" to the 3
    motifs above) is the polymer sequence cleaved according to the
    specification.
  */
  class CleaveMotif : public PolChemDefEntity
  {
    private:
    //! Motif.
    QString m_motif;

    //! List of codes in motif.
    QStringList m_codeList;

    //! Offset of the cleavage.
    int m_offset;

    //! Tells if motif is for cleavage.
    bool m_isForCleave;

    public:
    CleaveMotif(PolChemDefCstSPtr,
                QString,
                const QString & = QString(),
                int             = 0,
                bool            = false);

    CleaveMotif(const CleaveMotif &);


    ~CleaveMotif();

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    void clone(CleaveMotif *) const;
    void mold(const CleaveMotif &);
#pragma clang diagnostic pop

    CleaveMotif &operator=(const CleaveMotif &);

    void setMotif(const QString &);
    const QString &motif();

    const QStringList &codeList() const;

    void setOffset(int);
    int offset();

    void setForCleave(bool);
    bool isForCleave();

    int parse(const QString &);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    bool validate();
#pragma clang diagnostic pop
  };

} // namespace libmass

} // namespace msxps


#endif // CLEAVE_MOTIF_HPP
