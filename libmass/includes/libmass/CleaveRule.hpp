/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CLEAVE_RULE_HPP
#define CLEAVE_RULE_HPP


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "PolChemDefEntity.hpp"
#include "Formula.hpp"
#include "Monomer.hpp"


namespace msxps
{

namespace libmass
{


  //! The CleaveRule class provides a cleavage rule.
  /*! Cleavage rules help refine the description of the chemical
    reaction that is the basis of a cleavage(either enzymatic or
    chemical).

    While a number of cleavage agent(like a number of enzymes) do not
    make unexpected reactions upon cleavage(enzyme usually hydrolyze
    their substrates), chemical agents sometimes make a cleavage, and in
    the process of the cleavage reaction modify chemically the ends of
    the generaed oligomers. One notorious example is the case of cyanogen
    bromide, that cleaves proteins right of methionyl residues. Upon such
    cleavage, the monomer at the right side of the generated oligomer
   (methionyl residue) get modified according to this formula:
    "-CH2S+O".

    Cleavage rules are designed to be able to model such complex reactions.
  */
  class CleaveRule : public PolChemDefEntity
  {
    private:
    //! Left code.
    QString m_leftCode;

    //! Left formula.
    Formula m_leftFormula;

    //! Right code.
    QString m_rightCode;

    //! Right formula.
    Formula m_rightFormula;

    public:
    CleaveRule(PolChemDefCstSPtr,
               QString,
               QString = QString(),
               QString = QString(),
               QString = QString(),
               QString = QString());

    CleaveRule(const CleaveRule &);

    ~CleaveRule();

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    void clone(CleaveRule *);
    void mold(const CleaveRule &);
#pragma clang diagnostic pop

    CleaveRule &operator=(const CleaveRule &);

    void setLeftCode(const QString &);
    const QString &leftCode();

    void setRightCode(const QString &);
    const QString &rightCode();

    void setLeftFormula(const Formula &);
    const Formula &leftFormula();

    void setRightFormula(const Formula &);
    const Formula &rightFormula();

    void setAtomRefList(const QList<Atom *> *);
    void setMonomerRefList(const QList<Monomer *> *);

    static int isNameInList(const QString &,
                            const QList<CleaveRule *> &,
                            CleaveRule *other = 0);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    bool validate();
#pragma clang diagnostic pop

    bool renderXmlClrElement(const QDomElement &, int);

    QString *formatXmlClrElement(int, const QString & = QString("  "));
  };

} // namespace libmass

} // namespace msxps


#endif // CLEAVE_RULE_HPP
