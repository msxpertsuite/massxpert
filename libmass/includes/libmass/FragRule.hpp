/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef FRAG_RULE_HPP
#define FRAG_RULE_HPP


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "PolChemDefEntity.hpp"
#include "Formula.hpp"
#include "Monomer.hpp"


namespace msxps
{


namespace libmass
{


  //! The FragRule class provides a fragmentation rule.
  /*! Fragmentation rules characterize in more detail the chemical
    reaction that governs the fragmentation of the polymer in the
    gas-phase. The rule is a conditional rule. Its logic is based on the
    presence of given monomers at the place of the fragmentation and
    before or after that precise location.

    In saccharide chemistry, fragmentations are a very complex
    topic. This is because a given monomer will fragment according to a
    given chemistry if it is preceded in the sequence by a monomer of a
    given identity and according to another chemistry if its direct
    environment is different.

    This paradigm is implemented using a sequence environment logic
    based on conditions that can be formulated thanks to three monomer codes:

    \li the monomer at which the fragmentation takes place(current code);
    \li the monomer preceeding the current code(previous code);
    \li the monomer following the current code(following code);

    The use of these codes is typically according to this logic:

    "If current monomer is Glu and that previous monomer is Gly and
    following monomer is Arg, then fragmentation should occur according
    to this formula : "-H2O".
  */
  class FragRule : public PolChemDefEntity, public Formula
  {
    private:
    //! Previous code.
    QString m_prevCode;

    //! Current code.
    QString m_currCode;

    //! Next code.
    QString m_nextCode;

    //! Comment.
    QString m_comment;

    public:
    FragRule(PolChemDefCstSPtr,
             QString,
             QString         = QString(),
             QString         = QString(),
             QString         = QString(),
             QString         = QString(),
             const QString & = QString());

    FragRule(const FragRule &);
    ~FragRule();

    FragRule *clone() const;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    void clone(FragRule *) const;
    void mold(const FragRule &);
#pragma clang diagnostic pop

    FragRule &operator=(const FragRule &);

    void setPrevCode(const QString &);
    QString prevCode() const;

    void setCurrCode(const QString &);
    QString currCode() const;

    void setNextCode(const QString &);
    QString nextCode() const;

    QString formula() const;

    void setComment(const QString &);
    QString comment() const;

    static int isNameInList(const QString &,
                            const QList<FragRule *> &,
                            FragRule *other = 0);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    bool validate();
#pragma clang diagnostic pop

    bool renderXmlFgrElement(const QDomElement &);

    QString *formatXmlFgrElement(int, const QString & = QString("  "));
  };

} // namespace libmass

} // namespace msxps


#endif // FRAG_RULE_HPP
