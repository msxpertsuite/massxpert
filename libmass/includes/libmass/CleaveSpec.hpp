/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CLEAVE_SPEC_HPP
#define CLEAVE_SPEC_HPP


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes
#include "CleaveMotif.hpp"
#include "CleaveRule.hpp"


namespace msxps
{

namespace libmass
{


  //! The CleaveSpec class provides a cleavage specification.
  /*! Cleavage specifications determine the specificity of cleavage in a
    polymer seuqence using a simple syntax. For example, trypsin is able
    to cleave after lysyl and arginyl residues. Its cleavage pattern is
    thus "Lys/;Arg/;-Lys/Pro". Note that a provision can be made: if a
    lysyl residue is followed by a prolyl residue, then it is not
    cleaved by trypsin.

    A cleavage specification might not be enough information to
    determine the manner in which a polymer is cleaved. Cleavage rules
    might be required to refine the specification.  A cleavage
    specification might hold as many cleavage rules as required.
  */
  class CleaveSpec : public PolChemDefEntity
  {
    private:
    //! Pattern.
    QString m_pattern;

    //! List of cleavage motifs.
    QList<CleaveMotif *> m_motifList; // "Lys/" and -Lys/Pro and...

    //! List of cleavage rules.
    QList<CleaveRule *> m_ruleList; // Cleavage conditions...


    public:
    CleaveSpec(PolChemDefCstSPtr, QString, QString = QString());

    CleaveSpec(const CleaveSpec &);
    ~CleaveSpec();

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"

    void clone(CleaveSpec *) const;
    void mold(const CleaveSpec &);
#pragma clang diagnostic pop

    CleaveSpec &operator=(const CleaveSpec &);

    void setPattern(const QString &);
    const QString &pattern();

    QList<CleaveMotif *> *motifList();
    QList<CleaveRule *> *ruleList();

    static int isNameInList(const QString &,
                            const QList<CleaveSpec *> &,
                            CleaveSpec *other = 0);

    bool parse();

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    bool validate();
#pragma clang diagnostic pop

    bool renderXmlClsElement(const QDomElement &, int);

    QString *formatXmlClsElement(int, const QString & = QString("  "));
  };

} // namespace libmass

} // namespace msxps


#endif // CLEAVE_SPEC_HPP
