/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef SEQUENCE_HPP
#define SEQUENCE_HPP


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "Monomer.hpp"
#include "Prop.hpp"


namespace msxps
{

namespace libmass
{


  //! The Sequence class provides a simple sequence.
  /*!  A simple sequence is made of monomers arranged either in the form
    of a string of monomer codes concatenated one to the other with no
    delimitation(like "ATGC" or "AlaThrGlyCys") or in the form of a
    list(QList<Monomer *>) of full-fledged monomers.

    \attention The reference status of a sequence is in the form of a
    list of monomer instances. The conversion to the string of codes
    is only a utility. When a sequence is created(with an argument
    that is a string of monomer codes) the caller should ensure that
    the text sequence is converted into a list of monomers prior to
    starting using its methods extensively(see
    makeMonomerList()). Note that functions size() and
    removeMonomerAt()) only work on a sequence in the form of a list
    of monomers.

    Methods are provided to convert from one sequence kind
   (concatenated codes) to the other sequence kind(list of monomer
    instances).

    Equally interesting is the ability of the methods in this class to
    be able to:

    - parse the monomer sequence and to extract monomer codes one
    after the other;

    - remove monomers from the sequence at specified indexes;

    - add monomers to the sequence at specified indexes.

    However, for this rather basic class to be able to perform
    interesting tasks it has to be able to know where to find polymer
    chemistry definition data. This is possible only when a pointer to
    a polymer chemistry definition is passed to the used functions.
  */
  class Sequence
  {
    protected:
    //! Sequence in the form of a string of concatenated monomer codes.
    /*! Used to fill the \c m_monomerList member.
     */
    QString m_monomerText;

    //! List of monomers in the sequence. Empty upon creation.
    /*! Is filled with Monomer instances upon parsing of the text
      version of the sequence.
    */
    QList<const Monomer *> m_monomerList;

    public:
    Sequence(const QString & = QString());
    virtual ~Sequence();

    void setMonomerText(const QString &);
    void appendMonomerText(const QString &);
    const QString *monomerText();

    const QList<const Monomer *> &monomerList() const;
    QList<const Monomer *> *monomerListPtr();

    int nextCode(QString *, int *, QString *, int);
    const Monomer *at(int) const;
    int monomerIndex(const Monomer *);

    int size() const;
    bool isInBound(int index);

    void unspacifyMonomerText();

    virtual int makeMonomerText();
    QString *monomerText(int, int, bool) const;
    QString *monomerText(const CoordinateList &, bool, bool) const;

    int makeMonomerList(PolChemDefCstSPtr, bool = true, QList<int> * = 0);

    bool insertMonomerAt(const Monomer *, int);
    virtual bool prepareMonomerRemoval(const Monomer *);
    virtual bool removeMonomerAt(int);

    int findForwardMotif(Sequence *, PolChemDefCstSPtr, int *);

    bool validate(PolChemDefCstSPtr);

    quint16 checksum(int = -1, int = -1, bool = false) const;
  };

} // namespace libmass

} // namespace msxps


#endif // SEQUENCE_HPP
