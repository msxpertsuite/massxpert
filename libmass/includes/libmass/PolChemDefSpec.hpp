/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef POL_CHEM_DEF_SPEC_HPP
#define POL_CHEM_DEF_SPEC_HPP


/////////////////////// Qt includes
#include <QString>


namespace msxps
{

namespace libmass
{


  class PolChemDefSpec
  {
    private:
    // Name of the polymer definition(ie polymer type, 'protein').
    QString m_name;

    // PolChemDef definition file(absolute name, as found in the
    // catalogue files, like
    // '/usr/share/polyxmass/polchem-defs/protein.xml').
    QString m_filePath;

    public:
    PolChemDefSpec();
    ~PolChemDefSpec();

    void setName(const QString &);
    const QString &name() const;

    void setFilePath(const QString &);
    const QString &filePath() const;

    QString dirPath();
  };

} // namespace libmass

} // namespace msxps


#endif // POL_CHEM_DEF_SPEC_HPP
