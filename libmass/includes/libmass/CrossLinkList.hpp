/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CROSS_LINK_LIST_HPP
#define CROSS_LINK_LIST_HPP


/////////////////////// Qt includes


/////////////////////// Local includes
#include "CrossLink.hpp"


namespace msxps
{


namespace libmass
{

  class Polymer;


  class CrossLinkList : public QObject, public QList<CrossLink *>
  {
    Q_OBJECT

    friend class Polymer;

    public:
    CrossLinkList();
    CrossLinkList(const QString &name,
                  Polymer *       = 0,
                  const QString & = QString());
    CrossLinkList(const CrossLinkList *);

    virtual ~CrossLinkList();

    CrossLinkList *clone() const;
    void clone(CrossLinkList *) const;
    void mold(const CrossLinkList &);
    CrossLinkList &operator=(const CrossLinkList &);

    void setName(QString);
    QString name();

    void setComment(QString);
    const QString &comment() const;

    void setPolymer(Polymer *);
    const Polymer *polymer() const;

    int crossLinksInvolvingMonomer(const Monomer *, QList<int> *) const;

    private:
    QString m_name = "NOT_SET";
    QPointer<Polymer> mp_polymer;
    QString m_comment = "NOT_SET";
  };

} // namespace libmass

} // namespace msxps


#endif // CROSS_LINK_LIST_HPP
