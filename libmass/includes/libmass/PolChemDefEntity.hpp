/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes


namespace msxps
{

namespace libmass
{

  // Avoid inclusion of the header file.
  class PolChemDef;
  typedef std::shared_ptr<const PolChemDef> PolChemDefCstSPtr;

  //! The PolChemDefEntity class provides a chemical entity.
  /*! A polymer chemistry definition entity is something that relates to
    a polymer chemistry definition and as such has to maintain a close
    relationship with a polymer chemistry definition instance. That
    relationship is automatically generated upon construction, as a
    polymer definition chemical entity cannot be constructed without a
    pointer to a polymer chemistry definition instance. Aside from the
    pointer to a polymer chemistry definition, a polymer chemistry
    definition entity is also characterized by a name.
  */
  class PolChemDefEntity
  {
    protected:
    //! Pointer to the reference polymer chemistry definition.
    PolChemDefCstSPtr mcsp_polChemDef;

    //! Name.
    QString m_name;

    public:
    PolChemDefEntity(PolChemDefCstSPtr, const QString & = QString("NOT_SET"));

    PolChemDefEntity(const PolChemDefEntity &);

    virtual ~PolChemDefEntity();

    virtual void clone(PolChemDefEntity *) const;
    virtual PolChemDefEntity *clone() const;
    virtual void mold(const PolChemDefEntity &);
    virtual PolChemDefEntity &operator=(const PolChemDefEntity &);

    void setName(const QString &);
    QString name() const;

    PolChemDefCstSPtr polChemDefCstSPtr() const;
    void setPolChemDefCstSPtr(PolChemDefCstSPtr);

    virtual bool operator==(const PolChemDefEntity &) const;
    virtual bool operator!=(const PolChemDefEntity &) const;

    virtual bool validate() const;
  };

} // namespace libmass

} // namespace msxps
