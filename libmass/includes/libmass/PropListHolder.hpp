/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef PROP_LIST_HOLDER_HPP
#define PROP_LIST_HOLDER_HPP


/////////////////////// Qt includes


/////////////////////// Local includes
#include "Prop.hpp"


namespace msxps
{

namespace libmass
{


  //! The PropList class provides a list of allocated Prop instances.
  /*! PropList provides a list of allocated Prop instances that it
    owns. This class is used to derive classes needing to store
    dynamically allocated Prop objects without knowing with
    anticipation how many of such objects are going to be used.
  */
  class PropListHolder
  {
    protected:
    QList<Prop *> m_propList;

    public:
    PropListHolder();
    PropListHolder(const PropListHolder &);
    virtual ~PropListHolder();

    virtual PropListHolder *clone() const;
    virtual void clone(PropListHolder *) const;
    virtual void mold(const PropListHolder &);
    virtual PropListHolder &operator=(const PropListHolder &);

    const QList<Prop *> &propList() const;
    QList<Prop *> &propList();

    Prop *prop(const QString &, int * = 0);
    int propIndex(const QString &, Prop * = 0);

    int propListSize() const;

    bool appendProp(Prop *);
    bool removeProp(Prop *);
    bool removeProp(const QString &);
  };

} // namespace libmass

} // namespace msxps

#endif // PROP_LIST_HOLDER_HPP
