/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes
#include "ChemicalGroupRule.hpp"
#include "Prop.hpp"


namespace msxps
{

namespace libmass
{

  enum
  {
    CHEMGROUP_NEVER_TRAPPED = 1 << 0,
    CHEMGROUP_LEFT_TRAPPED  = 1 << 1,
    CHEMGROUP_RIGHT_TRAPPED = 1 << 2,
  };


  class ChemicalGroup
  {
    private:
    QString m_name;

    float m_pka;

    bool m_acidCharged;

    int m_polRule;

    QList<ChemicalGroupRule *> m_ruleList;

    public:
    ChemicalGroup(QString = QString(),
                  float   = 7.0,
                  bool    = true,
                  int     = CHEMGROUP_NEVER_TRAPPED);
    ChemicalGroup(const ChemicalGroup &);

    ~ChemicalGroup();

    void setName(QString);
    QString name() const;

    void setPka(float);
    float pka() const;

    void setAcidCharged(bool);
    bool isAcidCharged() const;

    void setPolRule(int);
    int polRule() const;

    QList<ChemicalGroupRule *> &ruleList();

    ChemicalGroupRule *findRuleEntity(QString, int * = 0) const;
    ChemicalGroupRule *findRuleName(QString, int * = 0) const;
    ChemicalGroupRule *findRule(QString, QString, int * = 0) const;

    bool renderXmlMnmElement(const QDomElement &);
    bool renderXmlMdfElement(const QDomElement &);
  };


  class ChemicalGroupProp : public Prop
  {
    public:
    ChemicalGroupProp(const QString & = QString(), ChemicalGroup * = 0);
    ~ChemicalGroupProp();
    virtual void deleteData();

    virtual void *clone() const;
    virtual void cloneOut(void *) const;
    virtual void cloneIn(const void *);

    bool renderXmlElement(const QDomElement &, int = 1);
    QString *formatXmlElement(int, const QString & = QString("  "));
  };

} // namespace libmass

} // namespace msxps
