/*   msXpertSuite - mass spectrometry software suite
 *   -----------------------------------------------
 *   Copyright(C) 2016 Filippo Rusconi
 *
 *   http://www.{site}.org
 *
 *   This file is part of the msXpertSuite project.
 *
 *   The msXpertSuite project is the successor of the massXpert project. This
 *   project now includes various independent modules:
 *   - massXpert, the original mass spectrometry environment software (greatly
 *     refactored code);
 *   - mobXpert, a module to process and mine mobility mass spectrometry data;
 *   - viewXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once

/////////////////////// Qt includes
#include <QObject>
#include <QString>


/////////////////////// Local includes
#include "Atom.hpp"


namespace msxps
{


namespace libmass
{


  //! The AtomCount class provides an atom and a count of that atom.
  /*!

    The AtomCount class derives from Atom, and adds a member datum representing
    the number of times that the atom occurs (an occurrence count). This class
    is used during the parsing of Formula instances.

  */
  class AtomCount : public Atom
  {
    private:
    //! Number of times that the atom occurs (its count).
    int m_count;

    public:
    AtomCount();
    AtomCount(const AtomCount &);

    AtomCount *clone() const;
    void clone(AtomCount *) const;
    void mold(const AtomCount &);
    AtomCount &operator=(const AtomCount &);

    void setCount(int);
    int count() const;

    int account(int = 1);

    bool accountMasses(const QList<Atom *> &,
                       double * = Q_NULLPTR,
                       double * = Q_NULLPTR,
                       int      = 1) const override;
  };


} // namespace libmass

} // namespace msxps
