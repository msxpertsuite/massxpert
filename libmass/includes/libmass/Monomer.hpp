/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MONOMER_HPP
#define MONOMER_HPP
//#warning "Entering MONOMER_HPP"


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "PolChemDefEntity.hpp"
#include "Formula.hpp"
#include "Ponderable.hpp"
#include "PropListHolder.hpp"
#include "CalcOptions.hpp"
#include "Modif.hpp"


namespace msxps
{

namespace libmass
{


  //! The Monomer class provides a monomer.

  /*! A monomer is the building block of a polymer sequence. It is
    mainly characterized by a name, a code and a formula.
   */
  class Monomer : public PolChemDefEntity,
                  public Formula,
                  public Ponderable,
                  public PropListHolder
  {
    private:
    //! Code.
    QString m_code;

    QList<Modif *> *mpa_modifList;

    public:
    Monomer(PolChemDefCstSPtr,
            QString,
            QString = QString(),
            QString = QString());

    ~Monomer();

    Monomer *clone() const;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    void clone(Monomer *) const;
    void mold(const Monomer &);
#pragma clang diagnostic pop

    void setCode(const QString &);
    QString code() const;

    bool operator==(const Monomer &) const;
    bool operator!=(const Monomer &) const;

    bool checkCodeSyntax() const;

    int isCodeKnown() const;
    static int
    isCodeInList(const QString &, const QList<Monomer *> &, Monomer * = 0);

    int isNameKnown() const;
    static int
    isNameInList(const QString &, const QList<Monomer *> &, Monomer * = 0);

    QString formula() const;

    QList<Modif *> *modifList() const;

    bool isModifTarget(const Modif &) const;
    bool modify(Modif *, bool, QStringList &);
    bool unmodify();
    bool unmodify(Modif *);
    bool isModified() const;

    int modifCount(const QString &);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
    bool validate();
    bool calculateMasses(int = MONOMER_CHEMENT_NONE);
    bool accountMasses(double * = 0, double * = 0, int = 1) const;
    bool accountMasses(Ponderable *, int) const;
#pragma clang diagnostic pop

    bool renderXmlMnmElement(const QDomElement &, int);

    QString *formatXmlMnmElement(int, const QString & = QString("  "));

    bool renderXmlMonomerElement(const QDomElement &, int);
    bool renderXmlMonomerElementV2(const QDomElement &, int);

    QString *formatXmlMonomerElement(int,
                                     const QString & = QString("  ")) const;

    void debugPutStdErr() const;
  };

} // namespace libmass

} // namespace msxps


#endif // MONOMER_HPP
