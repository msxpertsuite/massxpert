/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CROSS_LINK_HPP
#define CROSS_LINK_HPP
//#warning "Entering CROSS_LINK_HPP"


/////////////////////// Local includes
#include "CrossLinker.hpp"
#include "Monomer.hpp"

namespace msxps
{


namespace libmass
{


  class Polymer;


  enum CrossLinkEncompassed
  {
    CROSS_LINK_ENCOMPASSED_NO      = 0,
    CROSS_LINK_ENCOMPASSED_PARTIAL = 1,
    CROSS_LINK_ENCOMPASSED_FULL    = 2,
  };

  class CrossLink : public CrossLinker
  {
    protected:
    // We do not own these two monomers and polymer.
    QPointer<Polymer> mp_polymer;
    QList<const Monomer *> m_monomerList;

    QString m_comment;

    public:
    CrossLink(PolChemDefCstSPtr,
              Polymer *,
              const QString &,
              const QString &,
              const QString & = QString());

    CrossLink(const CrossLink &);

    CrossLink(const CrossLinker &, Polymer *, const QString & = QString());

    ~CrossLink();

    bool setMonomerAt(Monomer *, int);
    bool appendMonomer(const Monomer *);
    const Monomer *monomerAt(int);
    bool removeMonomerAt(int);

    void setPolymer(Polymer *);
    Polymer *polymer();

    void setComment(const QString &);
    const QString &comment() const;

    virtual bool operator==(const CrossLink &) const;

    int populateMonomerList(QString);
    QList<const Monomer *> *monomerList();
    int monomerIndexList(QList<int> *);
    QString monomerIndexText();
    QString monomerPosText();

    int involvesMonomer(const Monomer *) const;
    const Monomer *firstMonomer() const;
    int encompassedBy(int, int, int *in = 0, int * = 0);
    int encompassedBy(const CoordinateList &, int *in = 0, int * = 0);

    int involvedMonomers(QList<Monomer *> *);


    bool validate();

    virtual bool calculateMasses();

    virtual bool accountMasses(double * = 0, double * = 0, int = 1);
    virtual bool accountMasses(Ponderable *, int = 1);

    QString *prepareResultsTxtString();
  };

} // namespace libmass

} // namespace msxps


#endif // CROSS_LINK_HPP
