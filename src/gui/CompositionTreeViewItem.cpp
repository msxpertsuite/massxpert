/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "CompositionTreeViewItem.hpp"


namespace msxps
{

namespace massxpert
{


  CompositionTreeViewItem::CompositionTreeViewItem(
    const QList<QVariant> &data, CompositionTreeViewItem *parent)
  {
    mp_parentItem = parent;
    m_itemData    = data;
    mp_monomer    = 0;
  }


  CompositionTreeViewItem::~CompositionTreeViewItem()
  {
    qDeleteAll(m_childItemsList);
  }


  CompositionTreeViewItem *
  CompositionTreeViewItem::parent()
  {
    return mp_parentItem;
  }


  void
  CompositionTreeViewItem::appendChild(CompositionTreeViewItem *item)
  {
    m_childItemsList.append(item);
  }


  void
  CompositionTreeViewItem::insertChild(int index, CompositionTreeViewItem *item)
  {
    m_childItemsList.insert(index, item);
  }


  CompositionTreeViewItem *
  CompositionTreeViewItem::child(int row)
  {
    return m_childItemsList.value(row);
  }


  CompositionTreeViewItem *
  CompositionTreeViewItem::takeChild(int row)
  {
    CompositionTreeViewItem *item = m_childItemsList.takeAt(row);

    return item;
  }


  const QList<CompositionTreeViewItem *> &
  CompositionTreeViewItem::childItems()
  {
    return m_childItemsList;
  }


  int
  CompositionTreeViewItem::childCount() const
  {
    return m_childItemsList.count();
  }


  int
  CompositionTreeViewItem::columnCount() const
  {
    return m_itemData.count();
  }


  QVariant
  CompositionTreeViewItem::data(int column) const
  {
    return m_itemData.value(column);
  }


  bool
  CompositionTreeViewItem::setData(int column, const QVariant &value)
  {
    m_itemData[column].setValue(value.toString());

    return true;
  }


  int
  CompositionTreeViewItem::row() const
  {
    if(mp_parentItem)
      return mp_parentItem->m_childItemsList.indexOf(
        const_cast<CompositionTreeViewItem *>(this));

    return 0;
  }


  void
  CompositionTreeViewItem::setMonomer(libmass::Monomer *monomer)
  {
    Q_ASSERT(monomer);
    mp_monomer = monomer;
  }


  libmass::Monomer *
  CompositionTreeViewItem::monomer()
  {
    return mp_monomer;
  }

} // namespace massxpert

} // namespace msxps
