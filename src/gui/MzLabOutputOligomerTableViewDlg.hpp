/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
#define MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "ui_MzLabOutputOligomerTableViewDlg.h"
#include "MzLabOutputOligomerTableViewModel.hpp"
#include "MzLabOutputOligomerTableViewSortProxyModel.hpp"
#include "MzLabInputOligomerTableViewModel.hpp"


namespace msxps
{

	namespace massxpert
	{



class MzLabOutputOligomerTableView;
class MzLabOutputOligomerTableViewModel;
class MzLabOutputOligomerTableViewSortProxyModel;

class MzLabOutputOligomerTableViewDlg : public QDialog
{
  Q_OBJECT

  private:
  Ui::MzLabOutputOligomerTableViewDlg m_ui;

  // The list of oligomerPair instances that will be handed in the
  // table view. Note these oligomerPair instances are transferred
  // to us by MzLabWnd, and thus, although we did not alloate them,
  // we are given ownership on them, and are thus responsible for
  // freeing them. Hence, the mpa_prefix that indicated "allocated
  // here" ownership -> free that stuff when non more needed.
  QList<OligomerPair *> *mpa_oligomerPairList;

  libmass::MassType m_massType;

  // The dialog windows that hold the data that were used to perform
  // the matches that we are reporting in *this dialog window.
  MzLabInputOligomerTableViewDlg *mp_inputOligomerTableViewDlg1;
  MzLabInputOligomerTableViewDlg *mp_inputOligomerTableViewDlg2;

  QString m_name;

  MzLabWnd *mp_mzLabWnd;

  MzLabOutputOligomerTableViewModel *mpa_oligomerTableViewModel;
  MzLabOutputOligomerTableViewSortProxyModel *mpa_proxyModel;

  void setupTableView();

  void closeEvent(QCloseEvent *event);

  public:
  MzLabOutputOligomerTableViewDlg(QWidget *,
                                  QList<OligomerPair *> *,
                                  libmass::MassType,
                                  MzLabInputOligomerTableViewDlg *,
                                  MzLabInputOligomerTableViewDlg *,
                                  const QString & = QString(),
                                  const QString & = QString());

  ~MzLabOutputOligomerTableViewDlg();

  void setName(const QString &);
  QString name();

  public slots:
  void exportToClipboard();
  void exportSelectedAsNewList();
};

} // namespace massxpert

} // namespace msxps


#endif // MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
