/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef SEQUENCE_SELECTION_HPP
#define SEQUENCE_SELECTION_HPP


/////////////////////// Qt includes
#include <QList>
#include <QRect>
#include <QGraphicsRectItem>
#include <QPointF>


/////////////////////// Local includes
#include "RegionSelection.hpp"


namespace msxps
{

	namespace massxpert
	{



class SequenceEditorGraphicsView;
class CoordinateList;

class SequenceSelection
{
  private:
  SequenceEditorGraphicsView *mp_view;
  QList<RegionSelection *> m_regionSelectionList;

  public:
  SequenceSelection(SequenceEditorGraphicsView * = 0);

  ~SequenceSelection(void);

  void setView(SequenceEditorGraphicsView *);
  SequenceEditorGraphicsView *view();

  const QList<RegionSelection *> &regionSelectionList() const;

  int selectRegion(const QPointF &, const QPointF &, bool = true, bool = false);
  int selectRegion(int, int, bool = true, bool = false);

  int reselectRegions();
  int deselectRegions();
  int deselectRegion(RegionSelection *regionSelection);
  int deselectLastRegion();
  int deselectRegionsButLast();
  int deselectMultiSelectionRegionsButFirstSelection();

  RegionSelection *
  regionSelectionEncompassing(const QPointF &, const QPointF &, int * = 0);

  RegionSelection *regionSelectionEncompassing(int, int, int * = 0);

  double manhattanLength();
  bool isManhattanLength(const QPointF &, const QPointF &);

  int regionSelectionCount();

  bool selectionIndices(libmass::CoordinateList * = 0);

  void debugSelectionPutStdErr();
};

} // namespace massxpert

} // namespace msxps

#endif // SEQUENCE_SELECTION_HPP
