/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MASS_SEARCH_DLG_HPP
#define MASS_SEARCH_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>
#include <QSortFilterProxyModel>


/////////////////////// Local includes
#include "ui_MassSearchDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include "SequenceEditorWnd.hpp"
#include "MassSearchOligomerTableViewModel.hpp"
#include "MassSearchOligomerTableViewSortProxyModel.hpp"


namespace msxps
{

namespace massxpert
{


  class MassSearchOligomerTableViewModel;
  class MassSearchOligomerTableViewSortProxyModel;

  class MassSearchDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    private:
    Ui::MassSearchDlg m_ui;

    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    libmass::CalcOptions m_calcOptions;
    libmass::IonizeRule *mp_ionizeRule;

    double m_monoTolerance;
    double m_avgTolerance;

    double m_currentMass;
    int m_foundOligosCount;
    int m_testedOligosCount;
    int m_progressValue;

    int m_ionizeStart;
    int m_ionizeEnd;

    bool m_abort;
    bool m_sequenceEmbedded;

    // For the filtering of the data in the treeview.
    QAction *monoFilterAct;
    QAction *avgFilterAct;
    double m_filterTolerance;
    QWidget *mp_monoFocusWidget;
    QWidget *mp_avgFocusWidget;

    QList<double> m_monoMassesList;
    QList<double> m_avgMassesList;

    OligomerList m_monoOligomerList;
    OligomerList m_avgOligomerList;

    MassSearchOligomerTableViewModel *mpa_monoOligomerTableViewModel;
    MassSearchOligomerTableViewModel *mpa_avgOligomerTableViewModel;

    MassSearchOligomerTableViewSortProxyModel *mpa_monoProxyModel;
    MassSearchOligomerTableViewSortProxyModel *mpa_avgProxyModel;

    void writeSettings();
    void readSettings();

    public:
    MassSearchDlg(SequenceEditorWnd *editorWnd,
                  libmass::Polymer *polymer,
                  /* no libmass::PolChemDef **/
                  const QString &configSettingsFilePath,
                  const QString &applicationName,
                  const QString &description,
                  const libmass::CalcOptions &calcOptions,
                  libmass::IonizeRule *ionizeRule);

    ~MassSearchDlg();

    bool initialize();

    bool populateSelectedOligomerData();
    bool populateToleranceTypeComboBoxes();
    void setupTableViews();

    void updateIonizationData();
    void updateProgressDetails(int, bool = false, libmass::Oligomer * = 0);

    void updateMassSearchDetails(const libmass::CalcOptions &);
    void updateOligomerSequence(QString *);

    bool checkTolerance(int type);
    bool calculateTolerance(double, int);
    bool calculateFilterTolerance(double, int);

    bool parseMassText(int);
    bool searchMasses(int);
    bool searchMass(double, const libmass::Coordinates &, int);

    void freeAllOligomerLists();
    void emptyAllMassLists();

    bool updateSelectionData(bool = true);

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString();
    bool exportResultsClipboard();
    bool exportResultsFile();
    bool selectResultsFile();
    //////////////////////////////////// The results-exporting functions.

    public slots:

    void updateWholeSelectedSequenceData();

    void search();
    void abort();

    void exportResults(int);

    // MONO set of filtering widgets
    void monoFilterOptions(bool);
    void monoFilterOptionsToggled();

    void monoFilterSearched();
    void monoFilterError();
    void monoFilterMonoMass();
    void monoFilterAvgMass();

    // AVG set of filtering widgets
    void avgFilterOptions(bool);
    void avgFilterOptionsToggled();

    void avgFilterSearched();
    void avgFilterError();
    void avgFilterMonoMass();
    void avgFilterAvgMass();
  };

} // namespace massxpert

} // namespace msxps


#endif // MASS_SEARCH_DLG_HPP
