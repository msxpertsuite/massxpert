/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef SEQUENCE_EDITOR_FIND_DLG_HPP
#define SEQUENCE_EDITOR_FIND_DLG_HPP


/////////////////////// Local includes
#include "ui_SequenceEditorFindDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"


namespace msxps
{

namespace massxpert
{


  class SequenceEditorWnd;

  class SequenceEditorFindDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    private:
    Ui::SequenceEditorFindDlg m_ui;

    int m_startSearchIndex;
    int m_endSearchIndex;

    bool m_reprocessMotif;

    libmass::Sequence *mpa_motif;

    QStringList m_history;

    void writeSettings();
    void readSettings();

    public:
    SequenceEditorFindDlg(SequenceEditorWnd *editorWnd,
                          libmass::Polymer *polymer,
                          /* no libmass::PolChemDef **/
                          const QString &configSettingsFilePath,
                          const QString &applicationName,
                          const QString &description);

    ~SequenceEditorFindDlg();

    bool initialize();

    public slots:
    void find();
    void next();
    void motifComboBoxEditTextChanged(const QString &);
    void clearHistory();
  };

} // namespace massxpert

} // namespace msxps


#endif // SEQUENCE_EDITOR_FIND_DLG_HPP
