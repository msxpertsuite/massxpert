/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef POLYMER_MODIFICATION_DLG_HPP
#define POLYMER_MODIFICATION_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "libmass/globals.hpp"
#include "ui_PolymerModificationDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include "SequenceEditorWnd.hpp"


namespace msxps
{

namespace massxpert
{


  class PolymerModificationDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    private:
    Ui::PolymerModificationDlg m_ui;

    libmass::PolymerEnd m_polymerEnd;

    void updateModificationLineEdits();

    bool populateModificationList();

    bool initialize();

    public:
    PolymerModificationDlg(
      SequenceEditorWnd *editorWnd,
      /* no polymer **/
      /* no libmass::PolChemDef **/
      const QString &settingsFilePath,
      const QString &applicationName,
      const QString &description,
      libmass::PolymerEnd end = libmass::PolymerEnd::END_NONE);

    ~PolymerModificationDlg();

    bool parseModifDefinition(libmass::Modif *);

    public slots:
    void modify();
    void unmodify();
  };

} // namespace massxpert

} // namespace msxps


#endif // POLYMER_MODIFICATION_DLG_HPP
