/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef ABSTRACT_MAIN_TASK_WINDOW_HPP
#define ABSTRACT_MAIN_TASK_WINDOW_HPP

/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "../nongui/UserSpec.hpp"
#include "../nongui/ConfigSettings.hpp"


namespace msxps
{

	namespace massxpert
	{



class ProgramWindow;


class AbstractMainTaskWindow : public QMainWindow
{
  protected:
  ProgramWindow *mp_parentWnd;

  QString m_wndTypeName;
  QString m_applicationName;
  QString m_windowDescription;

  virtual void readSettings();
  virtual void writeSettings();

  virtual bool initialize() = 0;

  protected slots:
  virtual void closeEvent(QCloseEvent *event);

  public:
  AbstractMainTaskWindow(ProgramWindow *parent,
                         const QString &wndTypeName,
                         const QString &applicationName,
                         const QString &description);

  ~AbstractMainTaskWindow();

  QString configSettingsFilePath();
  const UserSpec &userSpec();
  const ConfigSettings *configSettings() const;

  const ProgramWindow *mainWindow() const;
};

} // namespace massxpert

} // namespace msxps

#endif // ABSTRACT_MAIN_TASK_WINDOW_HPP
