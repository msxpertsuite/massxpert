/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes


/////////////////////// Local includes
#include "AbstractPolChemDefDependentDlg.hpp"
#include "libmass/PolChemDef.hpp"
#include "PolChemDefWnd.hpp"
#include <libmass/PolChemDefEntity.hpp>


namespace msxps
{

namespace massxpert
{


  AbstractPolChemDefDependentDlg::AbstractPolChemDefDependentDlg(
    libmass::PolChemDefSPtr polChemDefSPtr,
    PolChemDefWnd *polChemDefWnd,
    const QString &settingsFilePath,
    const QString &wndTypeName,
    const QString &applicationName,
    const QString &description)
    : msp_polChemDef{polChemDefSPtr},
      mp_polChemDefWnd{polChemDefWnd},
      m_settingsFilePath{settingsFilePath},
      m_wndTypeName{wndTypeName},
      m_applicationName(applicationName),
      m_windowDescription(description)
  {
    if(!msp_polChemDef)
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

    if(!mp_polChemDefWnd)
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

    QString windowTitle =
      QString("%1 - %2").arg(m_applicationName).arg(m_windowDescription);

    setWindowTitle(windowTitle);
    // qDebug() << "Window title: " << windowTitle;
  }

  AbstractPolChemDefDependentDlg::~AbstractPolChemDefDependentDlg()
  {
  }


  void
  AbstractPolChemDefDependentDlg::displayWindowTitle()
  {
    setWindowTitle(
      QString("%1 - %2").arg(m_applicationName).arg(m_windowDescription));
  }


  void
  AbstractPolChemDefDependentDlg::closeEvent(
    [[maybe_unused]] QCloseEvent *event)
  {
    // No real close, because we did not ask that
    // close==destruction. Thus we only hide the dialog remembering its
    // position and size.

    writeSettings();
  }


  void
  AbstractPolChemDefDependentDlg::writeSettings()
  {
    QSettings settings(m_settingsFilePath, QSettings::IniFormat);

    settings.beginGroup(m_wndTypeName);
    settings.setValue("geometry", saveGeometry());
    settings.endGroup();
  }


  void
  AbstractPolChemDefDependentDlg::readSettings()
  {
    QSettings settings(m_settingsFilePath, QSettings::IniFormat);

    settings.beginGroup(m_wndTypeName);
    restoreGeometry(settings.value("geometry").toByteArray());
    settings.endGroup();
  }


  void
  AbstractPolChemDefDependentDlg::setModified()
  {
    mp_polChemDefWnd->setWindowModified(true);
  }

} // namespace massxpert

} // namespace msxps
