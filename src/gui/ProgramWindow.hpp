/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QMainWindow>
#include <QDir>
#include <QStringList>


/////////////////////// local includes
#include "AboutDlg.hpp"
#include "../nongui/UserSpec.hpp"
#include "../nongui/ConfigSettings.hpp"
#include "libmass/PolChemDef.hpp"
#include "libmass/PolChemDefSpec.hpp"
#include "libmassgui/includes/libmassgui/MassPeakShaperDlg.hpp"
#include "libmassgui/includes/libmassgui/IsoSpecDlg.hpp"


class QAction;
class QMenu;
class QTextEdit;


namespace msxps
{

namespace massxpert
{


  class SequenceEditorWnd;


  class ProgramWindow : public QMainWindow
  {
    Q_OBJECT

    public:
    ProgramWindow(const QString &applicationName, const QString &description);
    ~ProgramWindow();

    QString configSettingsFilePath() const;
    const UserSpec &userSpec() const;
    const ConfigSettings *configSettings() const;

    bool setConfigSettingsManually();

    void initializeDecimalPlacesOptions();

    void setLastFocusedSeqEdWnd(SequenceEditorWnd *);

    QList<libmass::PolChemDefSpec *> *polChemDefCatList();
    libmass::PolChemDefSpec *polChemDefSpecName(const QString &);
    libmass::PolChemDefSpec *polChemDefSpecFilePath(const QString &);

    void polChemDefCatStringList(QStringList &stringList);

    std::vector<libmass::PolChemDefCstSPtr> polChemDefList();
    libmass::PolChemDefCstSPtr polChemDefByName(const QString &name);

    bool isSequenceEditorWnd(SequenceEditorWnd *) const;

    public slots:
    void openPolChemDef();
    void newPolChemDef();

    void newCalculator();

    void openSequence(const QString &fileName = QString());
    void openSampleSequence();
    void newSequence();
    void delistSequenceEditorWnd(SequenceEditorWnd *wnd);

    void mzLab();

    void about();
    void showAboutDlg();

    void massListSorter();
    void seqManip();
    void showIsoSpecDlg();
    void showMassPeakShaperDlg();

    signals:

    void aboutToCloseSignal();

    protected:
    void closeEvent(QCloseEvent *);

    private:
    QString m_applicationName;
    QString m_windowDescription;

    UserSpec m_userSpec;
    QString m_configSettingsFilePath;
    ConfigSettings *mpa_configSettings = nullptr;

    void readSettings();
    void writeSettings();

    // Last sequence editor window that got the focus.
    SequenceEditorWnd *mp_lastFocusedSeqEdWnd;

    // The QList of all the sequence editor windows.
    QList<SequenceEditorWnd *> m_sequenceEditorWndList;

    // List of all the polymer chemistry definition specifications that
    // were created during parsing of all the different catalogue files
    // on the system.
    QList<libmass::PolChemDefSpec *> m_polChemDefCatList;

    // List of all the polymer chemistry definitions that are loaded in
    // memory and usable to load polymer sequences.
    std::vector<libmass::PolChemDefCstSPtr> m_polChemDefList;

    QMenu *mp_fileMenu;

    QMenu *mp_xpertDefMenu;
    QMenu *mp_xpertCalcMenu;
    QMenu *mp_xpertEditMenu;
    QMenu *mp_xpertMinerMenu;

    QMenu *mp_utilitiesMenu;

    QMenu *mp_helpMenu;

    QAction *mp_openPolChemDefAct;
    QAction *mp_newPolChemDefAct;

    QAction *mp_newCalculatorAct;

    QAction *mp_openSequenceAct;
    QAction *mp_openSampleSequenceAct;
    QAction *mp_newSequenceAct;

    QAction *mp_mzLabAct;

    QAction *mp_massListSorterAct;
    QAction *mp_seqManipAct;
    QAction *mp_isoSpecDlgAct;
    QAction *mp_massPeakShaperDlgAct;

    QAction *mp_exitAct;
    QAction *mp_configSettingsAct;

    QAction *mp_aboutAct;
    QAction *mp_aboutQtAct;

    libmassgui::IsoSpecDlg *mp_isoSpecDlg = nullptr;
    libmassgui::MassPeakShaperDlg *mp_massPeakShaperDlg = nullptr;

    void addToMenu(QObject *, const QStringList &, QMenu *, const char *);

    void createActions();
    void createMenus();
    void createStatusBar();

    void setupWindow();
    bool setupConfigSettings();
    bool initializeSystemConfig();
    bool initializeUserConfig();
  };

} // namespace massxpert

} // namespace msxps

