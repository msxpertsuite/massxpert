/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Local includes
#include "libmass/Atom.hpp"
#include "AbstractPolChemDefDependentDlg.hpp"
#include "ui_AtomDefDlg.h"
#include "libmass/PolChemDef.hpp"
#include "libmass/Isotope.hpp"
#include "PolChemDefWnd.hpp"


namespace msxps
{

namespace massxpert
{



  class AtomDefDlg : public AbstractPolChemDefDependentDlg
  {
    Q_OBJECT

    public:
    AtomDefDlg(libmass::PolChemDefSPtr polChemDefSPtr,
               PolChemDefWnd *polChemDefWnd,
               const QString &settingsFilePath,
               const QString &applicationName,
               const QString &description);

    ~AtomDefDlg();

    bool initialize();

    void updateAtomIdentityDetails(libmass::Atom *);
    void updateAtomMassDetails(libmass::Atom *);
    void updateIsotopeDetails(libmass::Isotope *);

    void clearAllDetails();

    public slots:

    // ACTIONS
    void addAtomPushButtonClicked();
    void removeAtomPushButtonClicked();
    void moveUpAtomPushButtonClicked();
    void moveDownAtomPushButtonClicked();

    void addIsotopePushButtonClicked();
    void removeIsotopePushButtonClicked();
    void moveUpIsotopePushButtonClicked();
    void moveDownIsotopePushButtonClicked();

    void applyAtomPushButtonClicked();
    void applyIsotopePushButtonClicked();

    bool validatePushButtonClicked();

    // VALIDATION
    bool validate();

    // WIDGETRY
    void atomListWidgetItemSelectionChanged();
    void isotopeListWidgetItemSelectionChanged();

    private:
    Ui::AtomDefDlg m_ui;

    QList<libmass::Atom *> *mp_list;

    void closeEvent(QCloseEvent *event);

    void writeSettings();
    void readSettings();

  };

} // namespace massxpert

} // namespace msxps


