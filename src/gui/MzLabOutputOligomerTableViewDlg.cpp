/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QMessageBox>
#include <QCloseEvent>
#include <QDebug>
#include <QClipboard>

/////////////////////// Local includes
#include "MzLabOutputOligomerTableViewDlg.hpp"
#include "MzLabWnd.hpp"
#include "MzLabOutputOligomerTableViewModel.hpp"

namespace msxps
{

namespace massxpert
{


  MzLabOutputOligomerTableViewDlg::MzLabOutputOligomerTableViewDlg(
    QWidget *parent,
    QList<OligomerPair *> *oligomerPairList,
    libmass::MassType massType,
    MzLabInputOligomerTableViewDlg *dlg1,
    MzLabInputOligomerTableViewDlg *dlg2,
    const QString &dlg1Name,
    const QString &dlg2Name)
    : QDialog(parent),
      mpa_oligomerPairList(oligomerPairList),
      m_massType(massType),
      mp_inputOligomerTableViewDlg1(dlg1),
      mp_inputOligomerTableViewDlg2(dlg2)
  {
    Q_ASSERT(parent);

    mp_mzLabWnd = static_cast<MzLabWnd *>(parent);

    Q_ASSERT(mpa_oligomerPairList);
    Q_ASSERT(mp_inputOligomerTableViewDlg1);
    Q_ASSERT(mp_inputOligomerTableViewDlg2);

    m_ui.setupUi(this);

    mpa_proxyModel             = 0;
    mpa_oligomerTableViewModel = 0;

    setupTableView();

    m_name = QString("%1 vs %2").arg(dlg1Name).arg(dlg2Name);

    m_ui.input1ListNameLineEdit->setText(dlg1Name);
    m_ui.input2ListNameLineEdit->setText(dlg2Name);

    setWindowTitle(tr("massXpert: mz Lab - %1").arg(m_name));

    connect(m_ui.exportToClipboardPushButton,
            SIGNAL(clicked()),
            this,
            SLOT(exportToClipboard()));

    connect(m_ui.exportSelectedAsNewListPushButton,
            SIGNAL(clicked()),
            this,
            SLOT(exportSelectedAsNewList()));
  }


  MzLabOutputOligomerTableViewDlg::~MzLabOutputOligomerTableViewDlg()
  {
    delete mpa_oligomerTableViewModel;

    while(mpa_oligomerPairList->size())
      {
        delete mpa_oligomerPairList->takeFirst();
        //	qDebug() << __FILE__ << __LINE__
        //		 << "Deleting one oligomer";
      }

    mpa_oligomerPairList = 0;
  }


  void
  MzLabOutputOligomerTableViewDlg::closeEvent(QCloseEvent *event)
  {
    QDialog::closeEvent(event);
  }


  void
  MzLabOutputOligomerTableViewDlg::setName(const QString &name)
  {
    m_name = name;

    m_ui.input1ListNameLineEdit->setText(m_name);
    setWindowTitle(tr("massXpert: mz Lab - %1").arg(m_name));
  }


  QString
  MzLabOutputOligomerTableViewDlg::name()
  {
    return m_name;
  }


  void
  MzLabOutputOligomerTableViewDlg::setupTableView()
  {
    // Model stuff all thought for sorting.
    mpa_oligomerTableViewModel =
      new MzLabOutputOligomerTableViewModel(mpa_oligomerPairList, this);

    mpa_proxyModel = new MzLabOutputOligomerTableViewSortProxyModel(this);
    mpa_proxyModel->setSourceModel(mpa_oligomerTableViewModel);
    mpa_proxyModel->setFilterKeyColumn(-1);

    m_ui.oligomerTableView->setModel(mpa_proxyModel);
    m_ui.oligomerTableView->setSourceModel(mpa_oligomerTableViewModel);
    m_ui.oligomerTableView->setOligomerPairList(mpa_oligomerPairList);

    m_ui.oligomerTableView->setMzLabWnd(mp_mzLabWnd);
    m_ui.oligomerTableView->setParentDlg(this);

    mpa_oligomerTableViewModel->setTableView(m_ui.oligomerTableView);
    mpa_oligomerTableViewModel->setMzLabWnd(mp_mzLabWnd);
    mpa_oligomerTableViewModel->setParentDlg(this);
  }


  void
  MzLabOutputOligomerTableViewDlg::exportToClipboard()
  {
    // We are asked to prepare a list of (m/z,z) pairs according to
    // what is displayed in the TableView. Ask the table view to
    // prepare a string.

    QString text = m_name;
    text.append("\n\n");

    QString *data = m_ui.oligomerTableView->selectedDataAsPlainText();
    text.append(*data);
    text.append("\n\n");
    delete data;

    QClipboard *clipboard = QApplication::clipboard();

    clipboard->setText(text, QClipboard::Clipboard);
  }

  void
  MzLabOutputOligomerTableViewDlg::exportSelectedAsNewList()
  {
    // The selected columns must encompass *one* m/z column and the
    // related z column. We want to first establish what's the column.

    // Set aside a container to stuff all the oligomers currently
    // selected.

    OligomerList tempOligomerList;

    m_ui.oligomerTableView->duplicateSelectedOligomers(&tempOligomerList);

    // We now have a set of oligomers corresponding to the selected
    // rows and columns.

    MzLabInputOligomerTableViewDlg *dlg =
      mp_mzLabWnd->newInputList(QString(), m_massType);

    if(!dlg)
      return;

    int count = dlg->duplicateOligomerData(tempOligomerList);

    if(count != tempOligomerList.count())
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

    // At this point, free the oligomers in the tempOligomerList.
    while(tempOligomerList.size())
      delete tempOligomerList.takeFirst();

    tempOligomerList.clear();
  }


} // namespace massxpert

} // namespace msxps
