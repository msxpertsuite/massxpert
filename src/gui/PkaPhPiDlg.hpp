/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef PKA_PH_PI_DLG_HPP
#define PKA_PH_PI_DLG_HPP


/////////////////////// Qt includes


/////////////////////// Local includes
#include "ui_PkaPhPiDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include "SequenceEditorWnd.hpp"
#include "../nongui/PkaPhPi.hpp"


namespace msxps
{

namespace massxpert
{


  enum
  {
    TARGET_PI         = 0,
    TARGET_NET_CHARGE = 1
  };


  class PkaPhPiDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    private:
    Ui::PkaPhPiDlg m_ui;

    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    // mpa_pkaPhPi not allocated locally, but ownnership taken by *this,
    // and will be deleted upon destruction of *this.
    PkaPhPi *mpa_pkaPhPi;

    libmass::CalcOptions m_calcOptions;

    int m_startIndex;
    int m_endIndex;
    int m_chemGroupsTested;

    bool fetchValidateInputData();

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString(int);
    bool exportResultsClipboard();
    bool exportResultsFile();
    bool selectResultsFile();
    //////////////////////////////////// The results-exporting functions.

    public:
    PkaPhPiDlg(SequenceEditorWnd *editorWnd,
               libmass::Polymer *,
               /* no libmass::PolChemDef **/
               const QString &settingsFilePath,
               const QString &applicationName,
               const QString &description,
               PkaPhPi *,
               libmass::CalcOptions &);

    ~PkaPhPiDlg();

    bool initialize();

    public slots:
    void netCharge();
    void isoelectricPoint();
    void exportResults(int);
  };

} // namespace massxpert

} // namespace msxps


#endif // PKA_PH_PI_DLG_HPP
