/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef SEQUENCE_PURIFICATION_DLG_HPP
#define SEQUENCE_PURIFICATION_DLG_HPP


/////////////////////// Local includes
#include "ui_SequencePurificationDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include "libmass/Sequence.hpp"
#include "SequenceEditorWnd.hpp"


namespace msxps
{

namespace massxpert
{


  class SequencePurificationDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    private:
    Ui::SequencePurificationDlg m_ui;

    bool m_firstRound;
    libmass::Sequence *mpa_origSequence;
    QList<int> m_errorList;
    QTextDocument m_leftTextDocument;
    QTextDocument m_rightTextDocument;

    public:
    SequencePurificationDlg(SequenceEditorWnd *editorWnd,
                            /* no libmass::Polymer **/
                            /* no libmass::PolChemDef **/
                            const QString &settingsFilePath,
                            const QString &applicationName,
                            const QString &description,
                            libmass::Sequence *sequence,
                            QList<int> *errorList);

    ~SequencePurificationDlg();

    bool initialize();

    void setupDocuments();

    void doTest(QTextDocument &);
    void setOrigText();
    bool findContinuum(QList<int> &list, int, int &);

    public slots:
    void purify();
    void test();
    void removeTagged();
    void reset();
  };

} // namespace massxpert

} // namespace msxps


#endif // SEQUENCE_PURIFICATION_DLG_HPP
