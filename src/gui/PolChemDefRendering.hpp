/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes
#include "libmass/PolChemDef.hpp"
#include "libmass/PolChemDefSpec.hpp"

#include "ChemEntVignetteRenderer.hpp"


namespace msxps
{

namespace massxpert
{

  class PolChemDef;

  //! The PolChemDefRendering class provides a container for a polymer chemistry
  /* definition that holds all the information to render the chemical enitities
   * graphically.
   */
  class PolChemDefRendering
  {
    private:
    // Constant shared point to the polymer chemistry definition for which we'll
    // handle the graphical rendering.
    libmass::PolChemDefCstSPtr mcsp_polChemDef = nullptr;

    //! Hash of graphical vignettes.
    /*! The graphical vignettes pointed to by items in this hash are
      used to render graphically chemical entities in the polymer
      chemistry definition. Their address is stored so that they can be
      reused by accessing them using a string like "Ser" or
      Ser-Phosphorylation", for example.
    */
    QHash<QString, ChemEntVignetteRenderer *> m_chemEntVignetteRendererHash;

    public:
    PolChemDefRendering();
    PolChemDefRendering(const PolChemDefRendering &other);
    PolChemDefRendering(libmass::PolChemDefCstSPtr polChemDefCstSPtr);

    virtual ~PolChemDefRendering();

    void setPolChemDefCstSPtr(libmass::PolChemDefCstSPtr polChemDefCstSPtr);

    libmass::PolChemDefCstSPtr getPolChemDef();

    QHash<QString, ChemEntVignetteRenderer *> *chemEntVignetteRendererHash();
    ChemEntVignetteRenderer *chemEntVignetteRenderer(const QString &);
    ChemEntVignetteRenderer *newChemEntVignetteRenderer(const QString &,
                                                        QObject * = 0);
    ChemEntVignetteRenderer *newMonomerVignetteRenderer(const QString &,
                                                        QObject * = 0);
    ChemEntVignetteRenderer *newModifVignetteRenderer(const QString &,
                                                      QObject * = 0);
    ChemEntVignetteRenderer *newCrossLinkerVignetteRenderer(const QString &,
                                                            QObject * = 0);
  };

} // namespace massxpert

} // namespace msxps

