/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_CALCULATION_TREEVIEW_ITEM_HPP
#define MZ_CALCULATION_TREEVIEW_ITEM_HPP


/////////////////////// Qt includes
#include <QList>
#include <QVariant>
#include <QTreeView>
#include <QMainWindow>


/////////////////////// Local includes
#include "libmass/Ionizable.hpp"


namespace msxps
{

	namespace massxpert
	{



enum
{
  MZ_CALC_LEVEL_COLUMN,
  MZ_CALC_MONO_COLUMN,
  MZ_CALC_AVG_COLUMN,
  MZ_CALC_TOTAL_COLUMNS
};


class MzCalculationTreeViewItem
{
  private:
  QList<MzCalculationTreeViewItem *> m_childItemsList;
  QList<QVariant> m_itemData;
  MzCalculationTreeViewItem *mp_parentItem;
  libmass::Ionizable *mp_ionizable;

  public:
  MzCalculationTreeViewItem(const QList<QVariant> &data,
                            MzCalculationTreeViewItem *parent = 0);

  ~MzCalculationTreeViewItem();

  MzCalculationTreeViewItem *parent();

  void appendChild(MzCalculationTreeViewItem *item);
  void insertChild(int index, MzCalculationTreeViewItem *item);

  MzCalculationTreeViewItem *child(int row);
  MzCalculationTreeViewItem *takeChild(int row);
  const QList<MzCalculationTreeViewItem *> &childItems();

  int childCount() const;
  int columnCount() const;

  QVariant data(int column) const;
  bool setData(int column, const QVariant &value);

  int row() const;

  void setIonizable(libmass::Ionizable *);
  libmass::Ionizable *ionizable();
};

} // namespace massxpert

} // namespace msxps


#endif // MZ_CALCULATION_TREEVIEW_ITEM_HPP
