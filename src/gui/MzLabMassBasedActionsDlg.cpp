/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QMessageBox>
#include <QCloseEvent>
#include <QDebug>


/////////////////////// Local includes
#include "MzLabMassBasedActionsDlg.hpp"
#include "MzLabInputOligomerTableViewDlg.hpp"
#include "MzLabWnd.hpp"


namespace msxps
{

namespace massxpert
{


  MzLabMassBasedActionsDlg::MzLabMassBasedActionsDlg(
    QWidget *parent, const QString &configSettingsFilePath)
    : QDialog{parent}, m_configSettingsFilePath{configSettingsFilePath}
  {
    if(!parent)
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);


    mp_mzLabWnd = static_cast<MzLabWnd *>(parent);

    m_ui.setupUi(this);

    setWindowTitle(tr("massXpert: mz Lab - Mass-based Actions"));

    connect(m_ui.applyMassPushButton,
            SIGNAL(clicked()),
            this,
            SLOT(applyMassPushButton()));

    connect(m_ui.applyThresholdPushButton,
            SIGNAL(clicked()),
            this,
            SLOT(applyThresholdPushButton()));

    QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

    settings.beginGroup("mz_lab_wnd_mass_based_actions");

    restoreGeometry(settings.value("geometry").toByteArray());

    settings.endGroup();
  }


  MzLabMassBasedActionsDlg::~MzLabMassBasedActionsDlg()
  {
  }


  void
  MzLabMassBasedActionsDlg::closeEvent(QCloseEvent *event)
  {
    QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

    settings.beginGroup("mz_lab_wnd_mass_based_actions");

    settings.setValue("geometry", saveGeometry());

    settings.endGroup();

    QDialog::closeEvent(event);
  }


  void
  MzLabMassBasedActionsDlg::applyMassPushButton()
  {
    // The actual work of doing the calculation is performed by the
    // MzLabInputOligomerTableViewDlg itself. So we have to get a
    // pointer to that dialog window. But first, check if the mass
    // entered in the line edit is correct.

    QString text = m_ui.massLineEdit->text();

    if(text.isEmpty())
      {
        QMessageBox::warning(this,
                             tr("massXpert: mz Lab"),
                             tr("%1@%2\n"
                                "Please, enter a valid mass.")
                               .arg(__FILE__)
                               .arg(__LINE__));
        return;
      }

    bool ok = false;

    double mass = text.toDouble(&ok);

    if(!mass && !ok)
      {
        QMessageBox::warning(this,
                             tr("massXpert: mz Lab"),
                             tr("%1@%2\n"
                                "Failed to convert %3 to double")
                               .arg(__FILE__)
                               .arg(__LINE__)
                               .arg(text));
        return;
      }

    // Get the pointer to the input list dialog window.

    MzLabInputOligomerTableViewDlg *dlg = 0;
    dlg                                 = mp_mzLabWnd->inputListDlg();
    if(!dlg)
      {
        QMessageBox::warning(this,
                             tr("massXpert: mz Lab"),
                             tr("%1@%2\n"
                                "Please, select one input list first.")
                               .arg(__FILE__)
                               .arg(__LINE__));
        return;
      }

    // Is the calculation to be performed inside the input list dialog
    // window, or are we going to create a new mz list with the
    // results of that calculation?
    bool inPlace = mp_mzLabWnd->inPlaceCalculation();

    // Finally actually have the calculation performed by the mz input
    // list dialog window.
    dlg->applyMass(mass, inPlace);
  }


  void
  MzLabMassBasedActionsDlg::applyThresholdPushButton()
  {
    // The actual work of doing the calculation is performed by the
    // MzLabInputOligomerTableViewDlg itself. So we have to get a
    // pointer to that dialog window. But first, check if the threshold
    // entered in the line edit is empty/correct or not.

    QString text = m_ui.thresholdLineEdit->text();

    if(text.isEmpty())
      {
        QMessageBox::warning(this,
                             tr("massXpert: mz Lab"),
                             tr("%1@%2\n"
                                "Please, enter a valid threshold.")
                               .arg(__FILE__)
                               .arg(__LINE__));
        return;
      }

    bool ok = false;

    double threshold = text.toDouble(&ok);

    if(!threshold && !ok)
      {
        QMessageBox::warning(this,
                             tr("massXpert: mz Lab"),
                             tr("%1@%2\n"
                                "Failed to convert %3 to double")
                               .arg(__FILE__)
                               .arg(__LINE__)
                               .arg(text));
        return;
      }

    // Get the pointer to the input list dialog window.

    MzLabInputOligomerTableViewDlg *dlg = 0;
    dlg                                 = mp_mzLabWnd->inputListDlg();
    if(!dlg)
      {
        QMessageBox::warning(this,
                             tr("massXpert: mz Lab"),
                             tr("%1@%2\n"
                                "Please, select one input list first.")
                               .arg(__FILE__)
                               .arg(__LINE__));
        return;
      }

    // Is the calculation to be performed inside the input list dialog
    // window, or are we going to create a new mz list with the
    // results of that calculation?
    bool inPlace = mp_mzLabWnd->inPlaceCalculation();

    // Are the calculations to be performed on mz values ?
    bool onMz = m_ui.mzThresholdRadioButton->isChecked();

    // Finally actually have the calculation performed by the mz input
    // list dialog window.
    dlg->applyThreshold(threshold, inPlace, onMz);
  }


} // namespace massxpert

} // namespace msxps
