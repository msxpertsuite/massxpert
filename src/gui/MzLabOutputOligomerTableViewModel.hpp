/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_MODEL_HPP
#define MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_MODEL_HPP


/////////////////////// Qt includes
#include <QTableView>


/////////////////////// Local includes
#include "libmass/Oligomer.hpp"
#include "../nongui/OligomerList.hpp"
#include "MzLabOutputOligomerTableView.hpp"
#include "MzLabWnd.hpp"


namespace msxps
{

	namespace massxpert
	{




enum
{
  MZ_LAB_OUTPUT_OLIGO_1_MASS_COLUMN,
  MZ_LAB_OUTPUT_OLIGO_1_CHARGE_COLUMN,
  MZ_LAB_OUTPUT_OLIGO_2_MASS_COLUMN,
  MZ_LAB_OUTPUT_OLIGO_2_CHARGE_COLUMN,
  MZ_LAB_OUTPUT_OLIGO_ERROR_COLUMN,
  MZ_LAB_OUTPUT_OLIGO_MATCH_COLUMN,
  MZ_LAB_OUTPUT_OLIGO_TOTAL_COLUMNS,
};


class MzLabWnd;


class MzLabOutputOligomerTableViewModel : public QAbstractTableModel
{
  Q_OBJECT

  private:
  QList<OligomerPair *> *mp_oligomerPairList;

  MzLabOutputOligomerTableView *mp_tableView;

  MzLabWnd *mp_mzLabWnd;
  MzLabOutputOligomerTableViewDlg *mp_parentDlg;

  // Fixme: does not this belong to the dialog? Maybe a pointer to
  // the other one should be better.
  libmass::MassType m_massType;

  public:
  MzLabOutputOligomerTableViewModel(QList<OligomerPair *> *, QObject *);
  ~MzLabOutputOligomerTableViewModel();

  void setParentDlg(MzLabOutputOligomerTableViewDlg *);
  MzLabOutputOligomerTableViewDlg *parentDlg();

  void setMzLabWnd(MzLabWnd *);
  MzLabWnd *mzLabWnd();

  void setTableView(MzLabOutputOligomerTableView *);
  MzLabOutputOligomerTableView *tableView();

  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;

  QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

  bool setData(const QModelIndex &, const QVariant &, int role = Qt::EditRole);

  QVariant data(const QModelIndex &parent = QModelIndex(),
                int role                  = Qt::DisplayRole) const;

  void addOligomerPair(OligomerPair *);
};

} // namespace massxpert

} // namespace msxps


#endif // MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_MODEL_HPP
