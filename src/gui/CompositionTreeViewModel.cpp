/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "CompositionTreeViewModel.hpp"
#include "CompositionTreeViewItem.hpp"


namespace msxps
{

namespace massxpert
{


  CompositionTreeViewModel::CompositionTreeViewModel(
    QList<libmass::Monomer *> *dataList, QObject *parent)
    : QAbstractItemModel(parent)
  {
    QList<QVariant> rootData;

    Q_ASSERT(dataList);
    Q_ASSERT(parent);

    mp_parentDlg = static_cast<CompositionsDlg *>(parent);

    rootData << tr("Name") << tr("Code") << tr("Count") << tr("Modif");

    mpa_rootItem = new CompositionTreeViewItem(rootData);

    mp_list = dataList;
    setupModelData(mpa_rootItem);
  }


  CompositionTreeViewModel::~CompositionTreeViewModel()
  {
    delete mpa_rootItem;
  }


  CompositionsDlg *
  CompositionTreeViewModel::getParentDlg()
  {
    return mp_parentDlg;
  }


  void
  CompositionTreeViewModel::setTreeView(QTreeView *treeView)
  {
    Q_ASSERT(treeView);

    mp_treeView = treeView;
  }


  QTreeView *
  CompositionTreeViewModel::treeView()
  {
    return mp_treeView;
  }


  QVariant
  CompositionTreeViewModel::data(const QModelIndex &index, int role) const
  {
    if(!index.isValid())
      return QVariant();

    if(role != Qt::DisplayRole)
      return QVariant();

    CompositionTreeViewItem *item =
      static_cast<CompositionTreeViewItem *>(index.internalPointer());

    return item->data(index.column());
  }


  QVariant
  CompositionTreeViewModel::headerData(int section,
                                       Qt::Orientation orientation,
                                       int role) const
  {
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole)
      return mpa_rootItem->data(section);

    return QVariant();
  }


  Qt::ItemFlags
  CompositionTreeViewModel::flags(const QModelIndex &index) const
  {
    if(!index.isValid())
      return Qt::ItemIsEnabled;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
  }


  QModelIndex
  CompositionTreeViewModel::index(int row,
                                  int column,
                                  const QModelIndex &parent) const
  {
    CompositionTreeViewItem *parentItem = 0;

    if(!parent.isValid())
      parentItem = mpa_rootItem;
    else
      parentItem =
        static_cast<CompositionTreeViewItem *>(parent.internalPointer());

    CompositionTreeViewItem *childItem = parentItem->child(row);
    if(childItem != 0)
      return createIndex(row, column, childItem);
    else
      return QModelIndex();
  }


  QModelIndex
  CompositionTreeViewModel::parent(const QModelIndex &index) const
  {
    if(!index.isValid())
      return QModelIndex();

    CompositionTreeViewItem *childItem =
      static_cast<CompositionTreeViewItem *>(index.internalPointer());

    CompositionTreeViewItem *parentItem = childItem->parent();

    if(parentItem == mpa_rootItem)
      return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
  }


  int
  CompositionTreeViewModel::rowCount(const QModelIndex &parent) const
  {
    CompositionTreeViewItem *parentItem;

    if(!parent.isValid())
      parentItem = mpa_rootItem;
    else
      parentItem =
        static_cast<CompositionTreeViewItem *>(parent.internalPointer());

    return parentItem->childCount();
  }


  int
  CompositionTreeViewModel::columnCount(const QModelIndex &parent) const
  {
    if(parent.isValid())
      return static_cast<CompositionTreeViewItem *>(parent.internalPointer())
        ->columnCount();
    else
      return mpa_rootItem->columnCount();
  }


  void
  CompositionTreeViewModel::removeAll()
  {
    int count = rowCount();

    //   qDebug() << __FILE__ << __LINE__
    // 	    << "count items:" << count;

    if(!count)
      return;

    // On WinXP , the following beginRemoveRows crashes when count is 0,
    // because last>=first fails, due to having count-1 < than 0. This
    // is why we return immediately when count is 0.
    beginRemoveRows(QModelIndex(), 0, count - 1);

    while(!mp_list->isEmpty())
      {
        delete(mp_list->takeFirst());

        delete(mpa_rootItem->takeChild(0));
      }

    endRemoveRows();

    emit layoutChanged();
  }


  bool
  CompositionTreeViewModel::setupModelData(CompositionTreeViewItem *parent)
  {
    QList<CompositionTreeViewItem *> parents;
    CompositionTreeViewItem *currentItem = 0;

    Q_ASSERT(parent);

    // Start with populating the very first item of the treeviewmodel.
    parents << parent;

    // We have the mp_list pointer that points to a QList<Monomer*>
    // list.

    for(int iter = 0; iter < mp_list->size(); ++iter)
      {
        libmass::Monomer *monomer = mp_list->at(iter);

        libmass::Prop *prop = monomer->prop("MONOMER_COUNT");
        Q_ASSERT(prop);

        int monomerCount = *static_cast<const int *>(prop->data());

        prop           = monomer->prop("MODIF_COUNT");
        int modifCount = 0;
        if(prop)
          modifCount = *static_cast<const int *>(prop->data());
        else
          modifCount = 0;


        QList<QVariant> columnData;

        columnData << monomer->name() << monomer->code() << monomerCount
                   << modifCount;

        // Create an item with those columnar strings. The parent of the
        // current item is parents.
        currentItem = new CompositionTreeViewItem(columnData, parents[0]);

        currentItem->setMonomer(monomer);

        // We should append that item right now.
        parents[0]->appendChild(currentItem);

        // At this point we have finished setting up the Model data.
      }

    return true;
  }

} // namespace massxpert

} // namespace msxps
