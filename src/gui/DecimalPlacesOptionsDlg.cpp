/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QMessageBox>


/////////////////////// Local includes
#include "../nongui/globals.hpp"
#include "DecimalPlacesOptionsDlg.hpp"


namespace msxps
{

namespace massxpert
{


  DecimalPlacesOptionsDlg::DecimalPlacesOptionsDlg(
    QWidget *parent, [[maybe_unused]] const QString &configSettingsFilePath)
    : QDialog(parent)
  {
    m_ui.setupUi(this);

    mp_editorWnd = static_cast<SequenceEditorWnd *>(parent);

    QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

    settings.beginGroup("decimal_places_options_dlg");

    restoreGeometry(settings.value("geometry").toByteArray());

    settings.endGroup();

    m_ui.atomDecimalPlacesSpinBox->setValue(libmass::ATOM_DEC_PLACES);
    m_ui.oligomerDecimalPlacesSpinBox->setValue(libmass::OLIGOMER_DEC_PLACES);
    m_ui.polymerDecimalPlacesSpinBox->setValue(libmass::POLYMER_DEC_PLACES);
    m_ui.pKaPhPiDecimalPlacesSpinBox->setValue(libmass::PH_PKA_DEC_PLACES);

    connect(m_ui.validatePushButton, SIGNAL(clicked()), this, SLOT(validate()));
  }


  DecimalPlacesOptionsDlg::~DecimalPlacesOptionsDlg()
  {
  }


  void
  DecimalPlacesOptionsDlg::closeEvent(QCloseEvent *event)
  {
    Q_UNUSED(event);

    QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

    settings.beginGroup("decimal_places_options_dlg");

    settings.setValue("geometry", saveGeometry());

    settings.endGroup();
  }


  SequenceEditorWnd *
  DecimalPlacesOptionsDlg::editorWnd()
  {
    return mp_editorWnd;
  }


  void
  DecimalPlacesOptionsDlg::validate()
  {

    libmass::ATOM_DEC_PLACES     = m_ui.atomDecimalPlacesSpinBox->value();
    libmass::OLIGOMER_DEC_PLACES = m_ui.oligomerDecimalPlacesSpinBox->value();
    libmass::POLYMER_DEC_PLACES  = m_ui.polymerDecimalPlacesSpinBox->value();
    libmass::PH_PKA_DEC_PLACES   = m_ui.pKaPhPiDecimalPlacesSpinBox->value();

    QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

    settings.beginGroup("decimal_places_options");

    settings.setValue("ATOM_DEC_PLACES", libmass::ATOM_DEC_PLACES);
    settings.setValue("OLIGOMER_DEC_PLACES", libmass::OLIGOMER_DEC_PLACES);
    settings.setValue("POLYMER_DEC_PLACES", libmass::POLYMER_DEC_PLACES);
    settings.setValue("PH_PKA_DEC_PLACES", libmass::PH_PKA_DEC_PLACES);

    settings.endGroup();

    // At this point ask that masses be redisplayed, for the sequence
    // editor window.

    emit(updateWholeSequenceMasses());
    emit(updateSelectedSequenceMasses());
  }

} // namespace massxpert

} // namespace msxps
