/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MONOMER_MODIFICATION_DLG_HPP
#define MONOMER_MODIFICATION_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "ui_MonomerModificationDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include "SequenceEditorWnd.hpp"


namespace msxps
{

namespace massxpert
{

  class MonomerModificationDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    private:
    Ui::MonomerModificationDlg m_ui;

    libmass::CoordinateList m_coordinateList;
    QList<int> m_indicesList;

    int prepareIndicesList();

    bool populateAvailableModifList();
    bool populateModifList(bool = true);

    bool populateMonomerList();
    bool populateModifiedMonomerList();
    bool populateModifAndModifiedMonomerLists();

    bool parseModifDefinition(libmass::Modif *);

    bool initialize();

    void readSettings();
    void writeSettings();

    public:
    MonomerModificationDlg(SequenceEditorWnd *editorWnd,
                           /* no polymer **/
                           /* no libmass::PolChemDef **/
                           const QString &settingsFilePath,
                          const QString &applicationName,
                          const QString &description);

    ~MonomerModificationDlg();

    public slots:
    void modify();
    void unmodify();
    bool updateSelectionData();
    void modifiedMonomerListWidgetItemSelectionChanged();
    void modifListWidgetItemSelectionChanged();
    void displayAllModifsChanged(int);
  };

} // namespace massxpert

} // namespace msxps


#endif // MONOMER_MODIFICATION_DLG_HPP
