/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CALCULATOR_CHEM_PAD_DLG_HPP
#define CALCULATOR_CHEM_PAD_DLG_HPP


/////////////////////// Qt includes
#include <QGroupBox>
#include <QColor>
#include <QHash>
#include <QDialog>
#include <QScrollArea>
#include <QGridLayout>
#include <QVBoxLayout>

/////////////////////// Local includes
// #include "ui_CalculatorChemPadDlg.h"
#include "ChemPadButton.hpp"
#include "CalculatorChemPadGroupBox.hpp"


namespace msxps
{

	namespace massxpert
	{



class CalculatorChemPadDlg : public QDialog
{
  Q_OBJECT

  private:
  //    Ui::CalculatorChemPadDlg m_ui;

  CalculatorWnd *mp_parentWnd = 0;

  QGroupBox *mpa_mainGroupBox;
  QVBoxLayout *mpa_mainGroupBoxVBLayout;

  QVBoxLayout *mpa_mainVBoxLayout;
  QScrollArea *mpa_scrollArea;
  QVBoxLayout *mpa_scrollAreaVBoxLayout;

  CalculatorChemPadGroupBox *mp_lastGroupbox;
  QGridLayout *mp_lastGridLayout;

  QHash<QString, QColor> m_colorHash;

  int m_columnCount;
  int m_row;
  int m_column;

  void keyPressEvent(QKeyEvent *);
  void keyReleaseEvent(QKeyEvent *);

  public:
  CalculatorChemPadDlg(CalculatorWnd *parent);
  ~CalculatorChemPadDlg();

  bool parseFile(QString &);
  bool setup(QString &, const QString & = QString());

  bool parseColumnCount(QString &);
  QColor parseColor(QString &);

  QStringList parseSeparator(QString &);
  ChemPadButton *parseKey(QString &, QStringList &);

  public slots:
  void close();
};

} // namespace massxpert

} // namespace msxps


#endif // CALCULATOR_CHEM_PAD_DLG_HPP
