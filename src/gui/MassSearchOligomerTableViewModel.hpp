/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MASS_SEARCH_OLIGOMER_TABLE_VIEW_MODEL_HPP
#define MASS_SEARCH_OLIGOMER_TABLE_VIEW_MODEL_HPP


/////////////////////// Qt includes
#include <QTableView>


/////////////////////// Local includes
#include "libmass/Oligomer.hpp"
#include "MassSearchDlg.hpp"


namespace msxps
{

	namespace massxpert
	{



enum
{
  MASS_SEARCH_OLIGO_SEARCHED_COLUMN,
  MASS_SEARCH_OLIGO_NAME_COLUMN,
  MASS_SEARCH_OLIGO_COORDINATES_COLUMN,
  MASS_SEARCH_OLIGO_ERROR_COLUMN,
  MASS_SEARCH_OLIGO_MONO_COLUMN,
  MASS_SEARCH_OLIGO_AVG_COLUMN,
  MASS_SEARCH_OLIGO_MODIF_COLUMN,
  MASS_SEARCH_OLIGO_TOTAL_COLUMNS
};


class MassSearchOligomerTableViewModel : public QAbstractTableModel
{
  Q_OBJECT

  private:
  OligomerList *mp_oligomerList;
  MassSearchDlg *mp_parentDlg;
  MassSearchOligomerTableView *mp_tableView;

  public:
  MassSearchOligomerTableViewModel(OligomerList *, QObject *);
  ~MassSearchOligomerTableViewModel();

  const OligomerList *oligomerList();
  MassSearchDlg *parentDlg();

  void setTableView(MassSearchOligomerTableView *);
  MassSearchOligomerTableView *tableView();

  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;

  QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

  QVariant data(const QModelIndex &parent = QModelIndex(),
                int role                  = Qt::DisplayRole) const;

  int addOligomer(libmass::Oligomer *);
  int addOligomers(const OligomerList &);
  int removeOligomers(int, int);
};

} // namespace massxpert

} // namespace msxps


#endif // MASS_SEARCH_OLIGOMER_TABLE_VIEW_MODEL_HPP
