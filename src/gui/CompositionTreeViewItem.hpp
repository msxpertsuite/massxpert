/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef COMPOSITION_TREEVIEW_ITEM_HPP
#define COMPOSITION_TREEVIEW_ITEM_HPP


/////////////////////// Qt includes
#include <QList>
#include <QVariant>
#include <QTreeView>
#include <QMainWindow>


/////////////////////// Local includes
#include "libmass/Monomer.hpp"


namespace msxps
{

	namespace massxpert
	{



enum
{
  COMPOSITION_NAME_COLUMN,
  COMPOSITION_CODE_COLUMN,
  COMPOSITION_COUNT_COLUMN,
  COMPOSITION_MODIF_COLUMN,
  COMPOSITION_TOTAL_COLUMNS
};


class CompositionTreeViewItem
{
  private:
  QList<CompositionTreeViewItem *> m_childItemsList;
  QList<QVariant> m_itemData;
  CompositionTreeViewItem *mp_parentItem;
  libmass::Monomer *mp_monomer;

  public:
  CompositionTreeViewItem(const QList<QVariant> &data,
                          CompositionTreeViewItem *parent = 0);

  ~CompositionTreeViewItem();

  CompositionTreeViewItem *parent();

  void appendChild(CompositionTreeViewItem *item);
  void insertChild(int index, CompositionTreeViewItem *item);

  CompositionTreeViewItem *child(int row);
  CompositionTreeViewItem *takeChild(int row);
  const QList<CompositionTreeViewItem *> &childItems();

  int childCount() const;
  int columnCount() const;

  QVariant data(int column) const;
  bool setData(int column, const QVariant &value);

  int row() const;

  void setMonomer(libmass::Monomer *);
  libmass::Monomer *monomer();
};

} // namespace massxpert

} // namespace msxps


#endif // COMPOSITION_TREEVIEW_ITEM_HPP
