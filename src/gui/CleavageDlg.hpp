/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CLEAVAGE_DLG_HPP
#define CLEAVAGE_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>
#include <QSortFilterProxyModel>
#include <libmass/PolChemDefEntity.hpp>


/////////////////////// Local includes
#include "ui_CleavageDlg.h"
#include "AbstractSeqEdWndDependentDlg.hpp"
#include "libmass/IonizeRule.hpp"
#include "../nongui/Cleaver.hpp"
#include "SequenceEditorWnd.hpp"
#include "CleaveOligomerTableViewModel.hpp"
#include "CleaveOligomerTableViewSortProxyModel.hpp"


namespace msxps
{

namespace massxpert
{


  class CleaveOligomerTableViewModel;
  class CleaveOligomerTableViewSortProxyModel;

  enum ExportResultsActions
  {
    EXPORT_TO_CLIPBOARD_OVERWRITE = 0,
    EXPORT_TO_CLIPBOARD_APPEND,
    EXPORT_TO_FILE,
    SELECT_FILE,
    CALCULATE_SPECTRUM
  };

  class CleavageDlg : public AbstractSeqEdWndDependentDlg
  {
    Q_OBJECT

    public:
    CleavageDlg(SequenceEditorWnd *editorWnd,
                libmass::Polymer *polymer,
                libmass::PolChemDefCstSPtr polChemDefCstSPtr,
                const QString &configSettingsFilePath,
                const QString &applicationName,
                const QString &description,
                QByteArray hash,
                const libmass::CalcOptions &calcOptions,
                const libmass::IonizeRule *ionizeRule);

    ~CleavageDlg();

    bool initialize();

    bool populateSelectedOligomerData();
    void populateCleaveSpecListWidget();

    SequenceEditorWnd *editorWnd();
    QByteArray polymerHash() const;

    void updateCleavageDetails(const libmass::CalcOptions &);
    void updateOligomerSequence(QString *);

    bool calculateTolerance(double);

    QString lastUsedCleavageAgentName() const;

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString(bool append = false);
    bool exportResultsToClipboard(bool append);
    bool exportResultsFile();
    bool selectResultsFile();
    bool calculateSpectrum();
    //////////////////////////////////// The results-exporting functions.

    public slots:
    void cleave();

    void keyPressEvent(QKeyEvent *event);
    void showOutputPatternDefinitionHelp();
    void exportResults(int);
    void filterOptions(bool);
    void filterOptionsToggled();
    void filterPartial();
    void filterMonoMass();
    void filterAvgMass();
    void filterCharge();

    private:
    Ui::CleavageDlg m_ui;

    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    QByteArray m_polymerHash;

    OligomerList m_oligomerList;

    libmass::CalcOptions m_calcOptions;
    const libmass::IonizeRule *mp_ionizeRule;

    Cleaver *mpa_cleaver;

    void writeSettings();
    void readSettings();

    CleaveOligomerTableViewModel *mpa_oligomerTableViewModel;
    CleaveOligomerTableViewSortProxyModel *mpa_proxyModel;

    // For the filtering of the data in the treeview.
    QAction *filterAct;
    double m_tolerance;
    QWidget *mp_focusWidget;

    void setupTableView();
  };

} // namespace massxpert

} // namespace msxps


#endif // CLEAVAGE_DLG_HPP
