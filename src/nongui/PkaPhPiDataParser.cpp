/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "PkaPhPiDataParser.hpp"
#include "libmass/ChemicalGroup.hpp"
#include <libmass/PolChemDefEntity.hpp>


namespace msxps
{

namespace massxpert
{


  PkaPhPiDataParser::PkaPhPiDataParser(
    const libmass::PolChemDefCstSPtr polChemDefCstSPtr, QString filePath)
    : mcsp_polChemDef(polChemDefCstSPtr), m_filePath(filePath)
  {
    Q_ASSERT(mcsp_polChemDef);
  }


  PkaPhPiDataParser::~PkaPhPiDataParser()
  {
  }


  void
  PkaPhPiDataParser::setFilePath(const QString &filePath)
  {
    m_filePath = filePath;
  }


  const QString &
  PkaPhPiDataParser::filePath()
  {
    return m_filePath;
  }


  bool
  PkaPhPiDataParser::renderXmlFile(QList<libmass::Monomer *> *monomerList,
                                   QList<libmass::Modif *> *modifList)
  {
    Q_ASSERT(monomerList);
    Q_ASSERT(modifList);

    // General structure of the file:
    //
    // <pkaphpidata>
    //   <monomers>
    //     <monomer>
    //       <code>A</code>
    //       <mnmchemgroup>
    //         <name>N-term NH2</name>
    // 	<pka>9.6</pka>
    // 	<acidcharged>TRUE</acidcharged>
    // 	<polrule>left_trapped</polrule>
    // 	<chemgrouprule>
    // 	  <entity>LE_PLM_MODIF</entity>
    // 	  <name>Acetylation</name>
    // 	  <outcome>LOST</outcome>
    // 	</chemgrouprule>
    //       </mnmchemgroup>
    //       <mnmchemgroup>
    //         <name>C-term COOH</name>
    // 	<pka>2.35</pka>
    // 	<acidcharged>FALSE</acidcharged>
    // 	<polrule>right_trapped</polrule>
    //       </mnmchemgroup>
    //    </monomer>
    // ...
    //
    //   <modifs>
    //     <modif>
    //       <name>Phosphorylation</name>
    //       <mdfchemgroup>
    //         <name>none_set</name>
    // 	<pka>12</pka>
    // 	<acidcharged>FALSE</acidcharged>
    //       </mdfchemgroup>
    //       <mdfchemgroup>
    //         <name>none_set</name>
    // 	<pka>7</pka>
    // 	<acidcharged>FALSE</acidcharged>
    //       </mdfchemgroup>
    //     </modif>
    //   </modifs>
    // </pkaphpidata>
    //
    // The DTD stipulates that:
    //
    // <!ELEMENT pkaphpidata(monomers,modifs*)>
    // <!ELEMENT monomers(monomer*)>
    // <!ELEMENT modifs(modif*)>
    // <!ELEMENT monomer(code,mnmchemgroup*)>
    // <!ELEMENT modif(name,mdfchemgroup*)>


    QDomDocument doc("pkaPhPiData");
    QDomElement element;
    QDomElement child;
    QDomElement indentedChild;

    QFile file(m_filePath);

    if(!file.open(QIODevice::ReadOnly))
      return false;

    if(!doc.setContent(&file))
      {
        file.close();
        return false;
      }

    file.close();

    element = doc.documentElement();

    if(element.tagName() != "pkaphpidata")
      {
        qDebug() << __FILE__ << __LINE__
                 << "pKa-pH-pI data file is erroneous\n";
        return false;
      }

    // The first child element must be <monomers>.

    child = element.firstChildElement();
    if(child.tagName() != "monomers")
      {
        qDebug() << __FILE__ << __LINE__
                 << "pKa-pH-pI data file is erroneous\n";
        return false;
      }

    // Parse the <monomer> elements.

    indentedChild = child.firstChildElement();
    while(!indentedChild.isNull())
      {
        if(indentedChild.tagName() != "monomer")
          return false;

        QDomElement superIndentedElement = indentedChild.firstChildElement();

        if(superIndentedElement.tagName() != "code")
          return false;

        libmass::Monomer *monomer = new libmass::Monomer(
          mcsp_polChemDef, "NOT_SET", superIndentedElement.text());

        // All the <mnmchemgroup> elements, if any.

        superIndentedElement = superIndentedElement.nextSiblingElement();
        while(!superIndentedElement.isNull())
          {
            if(superIndentedElement.tagName() != "mnmchemgroup")
              {
                delete monomer;
                return false;
              }

            libmass::ChemicalGroup *chemGroup =
              new libmass::ChemicalGroup("NOT_SET");

            if(!chemGroup->renderXmlMnmElement(superIndentedElement))
              {
                delete monomer;
                delete chemGroup;
                return false;
              }

            libmass::ChemicalGroupProp *prop =
              new libmass::ChemicalGroupProp("CHEMICAL_GROUP", chemGroup);

            monomer->appendProp(prop);

            superIndentedElement = superIndentedElement.nextSiblingElement();
          }

        monomerList->append(monomer);

        indentedChild = indentedChild.nextSiblingElement();
      }

#if 0

    qDebug() << __FILE__ << __LINE__
	      << "Debug output of all the monomers parsed:";

    for (int iter = 0; iter < monomerList->size(); ++iter)
      {
	Monomer *monomer = monomerList->at(iter);
	qDebug() << __FILE__ << __LINE__
		  << "Monomer:" << monomer->name();

	for(int jter = 0; jter < monomer->propList()->size(); ++jter)
	  {
	    libmass::Prop *prop = monomer->propList()->at(jter);

	    if (prop->name() == "CHEMICAL_GROUP")
	      {
		const libmass::ChemicalGroup *chemGroup =
		  static_cast<const libmass::ChemicalGroup *>(prop->data());

		qDebug() << __FILE__ << __LINE__
			  << "Chemical group:"
			  << chemGroup->name() << chemGroup->pka();
	      }
	  }
      }

#endif

    // And now parse the <modifs> elements, if any, this time, as
    // this element is not compulsory.

    child = child.nextSiblingElement();
    if(child.isNull())
      return true;

    if(child.tagName() != "modifs")
      {
        qDebug() << __FILE__ << __LINE__
                 << "pKa-pH-pI data file is erroneous\n";
        return false;
      }

    // Parse the <modif> elements.

    indentedChild = child.firstChildElement();
    while(!indentedChild.isNull())
      {
        if(indentedChild.tagName() != "modif")
          return false;

        QDomElement superIndentedElement = indentedChild.firstChildElement();

        if(superIndentedElement.tagName() != "name")
          return false;

        libmass::Modif *modif = new libmass::Modif(
          mcsp_polChemDef, superIndentedElement.text(), "H0");

        // All the <mdfchemgroup> elements, if any.

        superIndentedElement = superIndentedElement.nextSiblingElement();
        while(!superIndentedElement.isNull())
          {
            if(superIndentedElement.tagName() != "mdfchemgroup")
              {
                delete modif;
                return false;
              }

            libmass::ChemicalGroup *chemGroup =
              new libmass::ChemicalGroup("NOT_SET");

            if(!chemGroup->renderXmlMdfElement(superIndentedElement))
              {
                delete modif;
                delete chemGroup;
                return false;
              }

            libmass::ChemicalGroupProp *prop =
              new libmass::ChemicalGroupProp("CHEMICAL_GROUP", chemGroup);

            modif->appendProp(prop);

            superIndentedElement = superIndentedElement.nextSiblingElement();
          }

        modifList->append(modif);

        indentedChild = indentedChild.nextSiblingElement();
      }

#if 0

    qDebug() << __FILE__ << __LINE__
	      << "Debug output of all the modifs parsed:";

    for (int iter = 0; iter < modifList->size(); ++iter)
      {
	Modif *modif = modifList->at(iter);

	//       qDebug() << __FILE__ << __LINE__
	// 		<< "Modif:" << modif->name();

	for(int jter = 0; jter < modif->propList()->size(); ++jter)
	  {
	    libmass::Prop *prop = modif->propList()->at(jter);

	    if (prop->name() == "CHEMICAL_GROUP")
	      {
		const libmass::ChemicalGroup *chemGroup =
		  static_cast<const libmass::ChemicalGroup *>(prop->data());

		qDebug() << __FILE__ << __LINE__
			  << "Chemical group:"
			  << chemGroup->name() << chemGroup->pka();
	      }
	  }
      }

#endif

    return true;
  }

} // namespace massxpert

} // namespace msxps
