/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef POL_CHEM_DEF_CAT_PARSER_HPP
#define POL_CHEM_DEF_CAT_PARSER_HPP


/////////////////////// Local includes
#include "ConfigSettings.hpp"
#include "libmass/PolChemDefSpec.hpp"

namespace msxps
{

	namespace massxpert
	{



enum
{
  POLCHEMDEF_CAT_PARSE_APPEND_CONFIG  = 1 << 0,
  POLCHEMDEF_CAT_PARSE_PREPEND_CONFIG = 1 << 1,
  POLCHEMDEF_CAT_PARSE_SYSTEM_CONFIG  = 1 << 2,
  POLCHEMDEF_CAT_PARSE_USER_CONFIG    = 1 << 3,
  POLCHEMDEF_CAT_PARSE_BOTH_CONFIG =
    POLCHEMDEF_CAT_PARSE_SYSTEM_CONFIG | POLCHEMDEF_CAT_PARSE_USER_CONFIG,
};


class PolChemDefCatParser
{
  private:
  QString m_filePath;

  int m_pendingMode   = 0;
  int m_configSysUser = 0;

  const ConfigSettings *mp_configSettings = nullptr;


  public:
  PolChemDefCatParser();
  ~PolChemDefCatParser();

  void setFilePath(const QString &);
  const QString &getFilePath();

  void setPendingMode(int);
  int getPendingMode();

  void setConfigSysUser(int);
  int getConfigSysUser();

  int parseFiles(const ConfigSettings *, QList<libmass::PolChemDefSpec *> *);

  int parse(QList<libmass::PolChemDefSpec *> *, int);
};

} // namespace massxpert

} // namespace msxps


#endif // POL_CHEM_DEF_CAT_PARSER_HPP
