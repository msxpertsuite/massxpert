/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef FRAG_OPTIONS_HPP
#define FRAG_OPTIONS_HPP


/////////////////////// Local includes
#include "libmass/FragSpec.hpp"
#include "libmass/globals.hpp"
#include <libmass/PolChemDefEntity.hpp>


namespace msxps {


	namespace massxpert
	{


class FragOptions : public libmass::FragSpec
{
  private:
  int m_startIndex;
  int m_endIndex;

  // These two values have to be both positive and in increasing order
  // or equal. That is m_endIonizeLevel >= m_startIonizeLevel. Only
  // use access functions to set their values.
  int m_startIonizeLevel;
  int m_endIonizeLevel;

  bool m_sequenceEmbedded;

  QList<libmass::Formula *> m_formulaList;

  public:
  FragOptions(libmass::PolChemDefCstSPtr,
              QString,
              QString,
              libmass::FragEnd      = libmass::FragEnd::FRAG_END_NONE,
              const QString & = QString(),
              bool            = false);

  FragOptions(const libmass::FragSpec &, int = 0, int = 0, bool = false);

  FragOptions(const FragOptions &);

  ~FragOptions();

  FragOptions *clone() const;
  void clone(FragOptions *) const;
  void mold(const FragOptions &);

  void setStartIonizeLevel(int);
  int startIonizeLevel() const;

  void setEndIonizeLevel(int);
  int endIonizeLevel() const;

  void setStartIndex(int);
  int startIndex() const;

  void setEndIndex(int);
  int endIndex() const;

  bool addFormula(const libmass::Formula &);
  bool addFormula(const QString &);
  const QList<libmass::Formula *> &formulaList();

  void setSequenceEmbedded(bool);
  bool isSequenceEmbedded() const;
};

} // namespace massxpert

} // namespace msxps


#endif // FRAG_OPTIONS_HPP
