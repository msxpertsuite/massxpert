/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#include "MsMultiHash.hpp"


namespace msxps
{

namespace massxpert
{


  //! Construct an empty hash
  /*!

   */
  MsMultiHash::MsMultiHash()
  {
  }


  //! Destruct the hash
  /*!

   */
  MsMultiHash::~MsMultiHash()
  {
  }


  //! Get a list of keys (double values)
  /*!

    Iterates in the list of unique keys and for each \c key (double value)
    checks if it is contained in the [\p start--\p end] range. If so the double
    value is added to the \p keyList of double values.

    \param keyList list of double values to which keys are to be appended

    \param start double value representing the left border of the range (value
    is included in the range).

    \param end double value representing the right border or the range (value is
    included in the range).

    \return an integer containing the number of double keys that matched the
    range and that were thus added to the \p keyList list.

   */
  int
  MsMultiHash::rangedKeys(QList<double> &keyList,
                          double start,
                          double end) const
  {
    int count = 0;

    QList<double> keys = uniqueKeys();

    for(int iter = 0; iter < keys.size(); ++iter)
      {
        double key = keys.at(iter);

        if(key < start || key > end)
          continue;

        keyList.append(key);
        ++count;
      }

    return count;
  }


  // Put in \p massSpectra all the spectra that are listed in this has for \p
  // key
  int
  MsMultiHash::values(double key, QList<MassSpectrum *> &massSpectra) const
  {
    int count = 0;

    QList<MassSpectrum *> tempMassSpectra = values(key);
    count += tempMassSpectra.size();
    massSpectra.append(tempMassSpectra);

    return count;
  }


  //! Get a list of values (MassSpectrum instances)
  /*!

    Iterates in the list of unique keys and for each \c key (double value)
    appends the matching MassSpectrum instance in \p massSpectra only if \c key
    is contained in the [\p start--\p end] range.

    In this kind of situation, the \p start and \p end values might represent,
    for example, a retention time range or a drift time range.

    \param massSpectra the list of MassSpectrum instances where to store the
    matching MassSpectrum instance pointers (values of the hash).

    \param start double value representing the left border of the range (value
    is included in the range).

    \param end double value representing the right border of the range (value is
    included in the range).

    \return an integer containing the number of keys that matched the range and
    that were processed.

  */
  int
  MsMultiHash::rangedValues(QList<MassSpectrum *> &massSpectra,
                            double start,
                            double end) const
  {
    int count = 0;

    QList<double> keys = uniqueKeys();

    for(int iter = 0; iter < keys.size(); ++iter)
      {
        double key = keys.at(iter);

        if(key < start || key > end)
          continue;

        QList<MassSpectrum *> tempMassSpectra = values(key);
        count += tempMassSpectra.size();
        massSpectra.append(tempMassSpectra);
      }

    return count;
  }


} // namespace massxpert

} // namespace msxps
