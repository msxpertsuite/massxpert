/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>
#include <QStringList>
#include <QList>
#include <QString>

/////////////////////// Local includes
#include "ConfigSettings.hpp"
#include "ConfigSetting.hpp"


namespace msxps
{

namespace massxpert
{


  ConfigSettings::ConfigSettings(const QString &applicationName)
    : m_applicationName{applicationName}
  {
  }


  ConfigSettings::ConfigSettings(const ConfigSettings &other)
  {
    m_applicationName = other.m_applicationName;

    for(int iter = 0; iter < other.m_configSettingList.size(); ++iter)
      {
        ConfigSetting *iterConfigSetting = other.m_configSettingList.at(iter);
        ConfigSetting *newConfigSetting = new ConfigSetting(*iterConfigSetting);
        m_configSettingList.append(newConfigSetting);
      }
  }


  ConfigSettings &
  ConfigSettings::operator=(const ConfigSettings &other)
  {
    if(&other == this)
      return *this;

    m_applicationName = other.m_applicationName;

    for(int iter = 0; iter < other.m_configSettingList.size(); ++iter)
      {
        ConfigSetting *iterConfigSetting = other.m_configSettingList.at(iter);
        ConfigSetting *newConfigSetting = new ConfigSetting(*iterConfigSetting);
        m_configSettingList.append(newConfigSetting);
      }
    return *this;
  }


  ConfigSettings::~ConfigSettings()
  {
    while(!m_configSettingList.isEmpty())
      delete m_configSettingList.takeFirst();
  }


  void
  ConfigSettings::setModuleName(const QString &applicationName)
  {
    m_applicationName = applicationName;
  }


  QString
  ConfigSettings::applicationName()
  {
    return m_applicationName;
  }


  void
  ConfigSettings::append(ConfigSetting *configSetting)
  {
    if(configSetting == nullptr)
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

    m_configSettingList.append(configSetting);
  }


  QString
  ConfigSettings::key(ConfigSetting *configSetting) const
  {
    for(int iter = 0; iter < m_configSettingList.size(); ++iter)
      {
        ConfigSetting *iterConfigSetting = m_configSettingList.at(iter);

        if(iterConfigSetting == configSetting)
          return iterConfigSetting->m_key;
      }

    return QString();
  }


  const ConfigSetting *
  ConfigSettings::value(QString key, int userType) const
  {
    for(int iter = 0; iter < m_configSettingList.size(); ++iter)
      {
        ConfigSetting *iterConfigSetting = m_configSettingList.at(iter);

        if(iterConfigSetting->m_key == key &&
           iterConfigSetting->m_userType == userType)
          return iterConfigSetting;
      }

    return nullptr;
  }


  const QList<ConfigSetting *> *
  ConfigSettings::values() const
  {
    return &m_configSettingList;
  }


  QString *
  ConfigSettings::text() const
  {
    QString *p_text = new QString;

    for(int iter = 0; iter < m_configSettingList.size(); ++iter)
      {
        ConfigSetting *iterConfigSetting = m_configSettingList.at(iter);

        p_text->append(iterConfigSetting->text());
      }

    return p_text;
  }


} // namespace massxpert

} // namespace msxps
