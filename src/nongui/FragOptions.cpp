/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "FragOptions.hpp"
#include "libmass/PolChemDef.hpp"
#include <libmass/PolChemDefEntity.hpp>

namespace msxps
{

namespace massxpert
{


  FragOptions::FragOptions(libmass::PolChemDefCstSPtr polChemDefCstSPtr,
                           QString name,
                           QString formula,
                           libmass::FragEnd fragEnd,
                           const QString &comment,
                           bool sequenceEmbedded)
    : libmass::FragSpec(polChemDefCstSPtr, name, formula, fragEnd, comment),
      m_sequenceEmbedded(sequenceEmbedded)
  {
    // Have to be 1 and not 0, otherwise the setting functions ensuring
    // that m_startIonizeLevel>=m_endIonizeLevel will bug.
    m_startIonizeLevel = 1;
    m_endIonizeLevel   = 1;
  }


  FragOptions::FragOptions(const libmass::FragSpec &fragSpec,
                           int startIndex,
                           int endIndex,
                           bool sequenceEmbedded)
    : libmass::FragSpec(fragSpec),
      m_startIndex(startIndex),
      m_endIndex(endIndex),
      m_sequenceEmbedded(sequenceEmbedded)
  {
    // Have to be 1 and not 0, otherwise the setting functions ensuring
    // that m_startIonizeLevel>=m_endIonizeLevel will bug.
    m_startIonizeLevel = 1;
    m_endIonizeLevel   = 1;
  }


  FragOptions::FragOptions(const FragOptions &other)
    : libmass::FragSpec(other),
      m_startIndex(other.m_startIndex),
      m_endIndex(other.m_endIndex),
      m_startIonizeLevel(other.m_startIonizeLevel),
      m_endIonizeLevel(other.m_endIonizeLevel),
      m_sequenceEmbedded(other.m_sequenceEmbedded)
  {
    for(int iter = 0; iter < other.m_formulaList.size(); ++iter)
      {
        libmass::Formula *formula =
          new libmass::Formula(*other.m_formulaList.at(iter));
        m_formulaList.append(formula);
      }
  }


  FragOptions::~FragOptions()
  {
    // There might be some allocated libmass::Formula instances in the
    // m_formulaList. Free them.

    while(!m_formulaList.isEmpty())
      delete m_formulaList.takeFirst();
  }


  FragOptions *
  FragOptions::clone() const
  {
    FragOptions *other = new FragOptions(*this);

    return other;
  }


  void
  FragOptions::clone(FragOptions *other) const
  {
    Q_ASSERT(other);

    libmass::FragSpec::clone(other);

    other->m_sequenceEmbedded = m_sequenceEmbedded;
  }


  void
  FragOptions::mold(const FragOptions &other)
  {
    if(&other == this)
      return;

    libmass::FragSpec::mold(other);

    m_sequenceEmbedded = other.m_sequenceEmbedded;
  }


  void
  FragOptions::setStartIndex(int value)
  {
    m_startIndex = value;
  }


  int
  FragOptions::startIndex() const
  {
    return m_startIndex;
  }


  void
  FragOptions::setEndIndex(int value)
  {
    m_endIndex = value;
  }


  int
  FragOptions::endIndex() const
  {
    return m_endIndex;
  }


  void
  FragOptions::setStartIonizeLevel(int value)
  {
    int local = (value < 0) ? abs(value) : value;

    if(local <= m_endIonizeLevel)
      {
        m_startIonizeLevel = local;
      }
    else
      {
        m_startIonizeLevel = m_endIonizeLevel;
        m_endIonizeLevel   = local;
      }
  }


  int
  FragOptions::startIonizeLevel() const
  {
    return m_startIonizeLevel;
  }


  void
  FragOptions::setEndIonizeLevel(int value)
  {
    int local = (value < 0) ? abs(value) : value;

    if(local > m_startIonizeLevel)
      {
        m_endIonizeLevel = local;
      }
    else
      {
        m_startIonizeLevel = m_endIonizeLevel;
        m_endIonizeLevel   = local;
      }
  }


  int
  FragOptions::endIonizeLevel() const
  {
    return m_endIonizeLevel;
  }

  bool
  FragOptions::addFormula(const libmass::Formula &formula)
  {
    // First check that the formula is not already in the list.

    for(int iter = 0, size = m_formulaList.size(); iter < size; ++iter)
      {
        if(*(m_formulaList.at(iter)) == formula)
          return true;
      }

    // At this point we can say that the formula is OK and can be
    // added.

    libmass::Formula *newFormula = new libmass::Formula(formula);

    // At this point, we should check if the formula is valid.
    const QList<libmass::Atom *> &atomRefList = mcsp_polChemDef->atomList();

    if(!newFormula->validate(atomRefList))
      {
        qDebug() << "Formula" << newFormula->text() << "failed to validate.";

        delete newFormula;

        return false;
      }

    m_formulaList.append(newFormula);

    return true;
  }

  bool
  FragOptions::addFormula(const QString &text)
  {
    // With the string make a true libmass::Formula instance.

    libmass::Formula formula = libmass::Formula(text);

    return addFormula(formula);
  }


  const QList<libmass::Formula *> &
  FragOptions::formulaList()
  {
    return m_formulaList;
  }

  void
  FragOptions::setSequenceEmbedded(bool value)
  {
    m_sequenceEmbedded = value;
  }


  bool
  FragOptions::isSequenceEmbedded() const
  {
    return m_sequenceEmbedded;
  }

} // namespace massxpert

} // namespace msxps
