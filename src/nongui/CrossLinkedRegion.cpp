/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "CrossLinkedRegion.hpp"


namespace msxps
{

namespace massxpert
{


  //            [3] [4] [5] [6]        [9]    [11]
  // o---o---o---o---o--o---o---o---o---o---o---o---o---o---o
  //             |   |__|   |       |   |       |       |
  //             |          +-----------+       +-------+
  //             |                  |
  //             +------------------+
  //
  //
  // In the example above, there are two cross-linked regions: [3--9]
  // and [11--13].


  CrossLinkedRegion::CrossLinkedRegion()
  {
    m_startIndex = 0;
    m_endIndex   = 0;
  }


  CrossLinkedRegion::CrossLinkedRegion(int startIndex, int endIndex)
  {
    m_startIndex = startIndex;
    m_endIndex   = endIndex;
  }


  CrossLinkedRegion::CrossLinkedRegion(const CrossLinkedRegion &other)
  {
    m_startIndex = other.m_startIndex;
    m_endIndex   = other.m_endIndex;

    for(int iter = 0; iter < other.m_crossLinkList.size(); ++iter)
      {
        m_crossLinkList.append(other.m_crossLinkList.at(iter));
      }
  }


  CrossLinkedRegion::~CrossLinkedRegion()
  {
    // Do not destroy the libmass::CrossLink instances that we do not own. If
    // we destroyed them, then their destructor would trigger the
    // removal of the cross-link from the polymer sequence, which we
    // must not do.
  }

  void
  CrossLinkedRegion::setStartIndex(int index)
  {
    m_startIndex = index;
  }


  int
  CrossLinkedRegion::startIndex()
  {
    return m_startIndex;
  }


  void
  CrossLinkedRegion::setEndIndex(int index)
  {
    m_endIndex = index;
  }


  int
  CrossLinkedRegion::endIndex()
  {
    return m_endIndex;
  }


  const QList<libmass::CrossLink *> &
  CrossLinkedRegion::crossLinkList() const
  {
    return m_crossLinkList;
  }


  int
  CrossLinkedRegion::appendCrossLink(libmass::CrossLink *crossLink)
  {
    if(!crossLink)
      qFatal("Fatal error at %s@%d. Aborting.", __FILE__, __LINE__);

    // Only append the cross-link if it is not alreaday in the list.
    if(!m_crossLinkList.contains(crossLink))
      m_crossLinkList.append(crossLink);

    return m_crossLinkList.size();
  }


  int
  CrossLinkedRegion::appendCrossLinks(const QList<libmass::CrossLink *> &list)
  {
    for(int iter = 0; iter < list.size(); ++iter)
      {
        m_crossLinkList.append(list.at(iter));
      }

    return m_crossLinkList.size();
  }


  int
  CrossLinkedRegion::removeCrossLink(libmass::CrossLink *crossLink)
  {
    if(!crossLink)
      qFatal("Fatal error at %s@%d. Aborting.", __FILE__, __LINE__);

    m_crossLinkList.removeOne(crossLink);

    return m_crossLinkList.size();
  }


  int
  CrossLinkedRegion::removeCrossLinkAt(int index)
  {
    if(index >= m_crossLinkList.size() || index < 0)
      qFatal("Fatal error at %s@%d. Aborting.", __FILE__, __LINE__);

    m_crossLinkList.removeAt(index);

    return m_crossLinkList.size();
  }


} // namespace massxpert

} // namespace msxps
