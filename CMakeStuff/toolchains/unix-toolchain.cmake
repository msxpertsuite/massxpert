message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug -DBUILD_USER_MANUAL=1 ../development")
message("If using the locally built pappsomspp libs, add -DLOCAL_DEV")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

# This is used throughout all the build system files
set(TARGET massxpert)

# Now that we know what is the TARGET (in the toolchain files above,
# we can compute the lowercase TARGET (used for string replacements in 
# configure files and also for the resource compilation with windres.exe.
string(TOLOWER ${TARGET} TARGET_LOWERCASE)
message("TARGET_LOWERCASE: ${TARGET_LOWERCASE}")

find_package(Qt6 COMPONENTS Widgets Xml Svg SvgWidgets PrintSupport GLOBAL REQUIRED)

# Find the libIsoSpec library
find_package(IsoSpec++ GLOBAL REQUIRED)

# Find the libpappsomspp core and widget libraries
find_package(PappsoMSpp COMPONENTS Core Widget GLOBAL REQUIRED)

# Find the libmass static library
set(libmass_FOUND 1)
# Below: for the generated UIS_H files and also to access the includes like
# #include <libmass/_file_> or #include <_file_>
set(libmass_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/libmass/includes"
  "${CMAKE_SOURCE_DIR}/libmass/includes/libmass" "${CMAKE_BINARY_DIR}/libmass")
set(libmass_LIBRARIES "${CMAKE_SOURCE_DIR}/libmass/libmass.a") 
if(NOT TARGET libmass::nongui)

  add_library(libmass::nongui UNKNOWN IMPORTED)

  set_target_properties(libmass::nongui PROPERTIES
    IMPORTED_LOCATION             "${libmass_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${libmass_INCLUDE_DIRS}"
  )
endif()

message(STATUS "Found libmass at: ${libmass_LIBRARIES} with include dir: ${libmass_INCLUDE_DIRS}")

# Find the libmassgui static library
set(libmassgui_FOUND 1)
# Below: for the generated UIS_H files and also to access the includes like
# #include <libmassgui/_file_> or #include <_file_>
set(libmassgui_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/libmassgui/includes"
  "${CMAKE_SOURCE_DIR}/libmassgui/includes/libmassgui" "${CMAKE_BINARY_DIR}/libmassgui")
set(libmassgui_LIBRARIES "${CMAKE_SOURCE_DIR}/libmassgui/libmassgui.a") 
if(NOT TARGET libmass::gui)

  add_library(libmass::gui UNKNOWN IMPORTED)

  set_target_properties(libmass::gui PROPERTIES
    IMPORTED_LOCATION             "${libmassgui_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${libmassgui_INCLUDE_DIRS}"
  )
endif()

message(STATUS "Found libmassgui at: ${libmassgui_LIBRARIES} with include dir: ${libmassgui_INCLUDE_DIRS}")


## INSTALL directories
if(NOT CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX /usr)
endif()
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DATA_DIR ${CMAKE_INSTALL_PREFIX}/share/${TARGET}/data)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})


# The appstream, desktop and icon files
install(FILES org.msxpertsuite.massxpert.desktop
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/applications)

install(FILES org.msxpertsuite.massxpert.appdata.xml
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/metainfo)

install(FILES images/icons/16x16/massxpert.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/16x16/apps)

install(FILES images/icons/32x32/massxpert.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/32x32/apps)

install(FILES images/icons/48x48/massxpert.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/48x48/apps)

install(FILES images/icons/64x64/massxpert.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/64x64/apps)


## Platform-dependent compiler flags:
include(CheckCXXCompilerFlag)

if (WITH_FPIC)
  add_definitions(-fPIC)
endif()

