# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

MESSAGE("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(HOME_DEVEL_DIR $ENV{HOME}/devel)

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /home/rusconi/devel/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /home/rusconi/devel/mxe/usr/x86_64-w64-mingw32.shared/include)

# This is used throughout all the build system files
set(TARGET massXpert)
set(TARGET_LOWERCASE massxpert)


# Now that we have the TARGET variable contents, let's configure InnoSetup
CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/${TARGET_LOWERCASE}-mxe.iss.in
  ${CMAKE_SOURCE_DIR}/winInstaller/${TARGET_LOWERCASE}-mxe.iss @ONLY)


## INSTALL directories
# This is the default on windows, but set it nonetheless.
set(CMAKE_INSTALL_PREFIX "C:/Program Files")

set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET})
# On Win, the doc dir is uppercase.
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/doc)
# Same for the data dir.
set(DATA_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/data)

# On Win10 all the code is relocatable.
remove_definitions(-fPIC)

