# Packager
set(CPACK_PACKAGE_NAME "massxpert")
set(CPACK_PACKAGE_VENDOR "msXpertSuite")
set(CPACK_PACKAGE_VERSION "${VERSION}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Software for polymer modelling and mass spectrometry simulations")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE")
set(CPACK_RESOURCE_FILE_AUTHORS "${CMAKE_SOURCE_DIR}/AUTHORS")
set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_SOURCE_IGNORE_FILES
  ".git"
  ".gitattributes"
  ".gitignore"
  ".gitmodules"
  ".cache"
  "compile_commands.json"
  "maintainer-scripts"
  "Makefile"
  "tests"
  "TODO"
  "doc/user-manual/build"
  "libmass/.git"
  "libmass/.cache"
  "libmass/compile_commands.json"
  "libmass/build"
  "libmass/libmass.a"
  ${CPACK_SOURCE_IGNORE_FILES}
)

set(CPACK_SOURCE_PACKAGE_FILE_NAME 
  "${LOWCASE_PROJECT_NAME}-${CPACK_PACKAGE_VERSION}")

include(CPack)


