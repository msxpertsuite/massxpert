/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <cmath>
#include <algorithm>
#include <limits> // for std::numeric_limits


/////////////////////// Qt includes
#include <QtGui>
#include <QMessageBox>
#include <QFileDialog>

#include <libmass/globals.hpp>

/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>
#include <pappsomspp/trace/trace.h>

/////////////////////// Local includes
#include "MassPeakShaperDlg.hpp"


namespace msxps
{

namespace libmassgui
{


  MassPeakShaperDlg::MassPeakShaperDlg(QWidget *program_window_p,
                                       const QString &applicationName,
                                       const QString &description)
    : QDialog(static_cast<QWidget *>(program_window_p)),
      mp_programWindow(program_window_p),
      m_applicationName{applicationName}
  {
    if(!program_window_p)
      qFatal("Programming error. Program aborted.");

    m_ui.setupUi(this);

    // Update the window title because the window title element in m_ui might be
    // either erroneous or empty.
    setWindowTitle(QString("%1 - %2").arg(applicationName).arg(description));

    // We want to destroy the dialog when it is closed.
    setAttribute(Qt::WA_DeleteOnClose);

    setupDialog();
  }


  MassPeakShaperDlg::MassPeakShaperDlg(
    QWidget *program_window_p,
    const QString &applicationName,
    const QString &description,
    const std::vector<libmass::PeakCentroidSPtr> &peak_centroids,
    double new_max_intensity)
    : QDialog(static_cast<QWidget *>(program_window_p)),
      mp_programWindow(program_window_p),
      m_applicationName{applicationName},
      m_newMaxIntensity(new_max_intensity)
  {
    if(!program_window_p)
      qFatal("Programming error. Program aborted.");

    // qDebug() << "New max intensity:" << m_newMaxIntensity;

    m_ui.setupUi(this);

    // Update the window title because the window title element in m_ui might be
    // either erroneous or empty.
    setWindowTitle(QString("%1 - %2").arg(applicationName).arg(description));

    // We want to destroy the dialog when it is closed.
    setAttribute(Qt::WA_DeleteOnClose);

    setupDialog();

    setCentroidData(peak_centroids);
  }


  void
  MassPeakShaperDlg::setNewMaxIntensity(double new_max_intensity)
  {
    m_newMaxIntensity = new_max_intensity;
    if(m_newMaxIntensity != std::numeric_limits<double>::min())
      m_ui.normalizeValueDoubleSpinBox->setValue(m_newMaxIntensity);
  }


  void
  MassPeakShaperDlg::closeEvent([[maybe_unused]] QCloseEvent *event)
  {
    // qDebug();
    writeSettings(libmass::configSettingsFilePath(m_applicationName));
  }


  MassPeakShaperDlg::~MassPeakShaperDlg()
  {
    // qDebug();

    writeSettings(libmass::configSettingsFilePath(m_applicationName));
  }


  //! Save the settings to later restore the window in its same position.
  void
  MassPeakShaperDlg::writeSettings(const QString &configSettingsFilePath)
  {
    // qDebug();

    QSettings settings(configSettingsFilePath, QSettings::IniFormat);
    settings.beginGroup("MassPeakShaperDlg");

    settings.setValue("geometry", saveGeometry());
    settings.setValue("pointCount", m_ui.pointCountSpinBox->value());
    settings.setValue("resolution", m_ui.resolutionSpinBox->value());
    settings.setValue("charge", m_ui.ionChargeSpinBox->value());
    settings.setValue("fwhm", m_ui.fwhmDoubleSpinBox->value());
    settings.setValue("binSize", m_ui.binSizeDoubleSpinBox->value());
    settings.setValue("splitter", m_ui.splitter->saveState());
    settings.setValue("binSizeGroupBoxState",
                      m_ui.binSizeGroupBox->isChecked());
    settings.setValue("fmwhToBinSizeRatio",
                      m_ui.fmwhToBinSizeRatioSpinBox->value());

    settings.endGroup();
  }


  //! Read the settings to restore the window in its last position.
  void
  MassPeakShaperDlg::readSettings(const QString &configSettingsFilePath)
  {
    // qDebug();

    QSettings settings(configSettingsFilePath, QSettings::IniFormat);
    settings.beginGroup("MassPeakShaperDlg");

    restoreGeometry(settings.value("geometry").toByteArray());
    m_ui.pointCountSpinBox->setValue(settings.value("pointCount", 150).toInt());
    m_ui.resolutionSpinBox->setValue(
      settings.value("resolution", 45000).toInt());
    m_ui.ionChargeSpinBox->setValue(settings.value("charge", 1).toInt());
    m_ui.fwhmDoubleSpinBox->setValue(settings.value("fwhm", 0).toDouble());

    m_ui.binSizeGroupBox->setChecked(
      settings.value("binSizeGroupBoxState", 0).toInt());

    m_ui.binSizeDoubleSpinBox->setValue(
      settings.value("binSize", 0).toDouble());
    m_ui.splitter->restoreState(settings.value("splitter").toByteArray());


    m_ui.fmwhToBinSizeRatioSpinBox->setValue(
      settings.value("fwhmToBinSizeRatio", 6).toInt());
    settings.endGroup();
  }


  void
  MassPeakShaperDlg::setupDialog()
  {
    // Ranges for various numerical values
    m_ui.resolutionSpinBox->setRange(0, 2000000);
    m_ui.fwhmDoubleSpinBox->setRange(0, 10);
    m_ui.pointCountSpinBox->setRange(5, 1000);
    m_ui.ionChargeSpinBox->setRange(1, 10000);

    // The values above are actually set in readSettings (with default values if
    // missing in the config settings.)
    readSettings(libmass::configSettingsFilePath(m_applicationName));

    // Always start the dialog with the first page of the tab widget,
    // the input data page.
    m_ui.tabWidget->setCurrentIndex(
      static_cast<int>(TabWidgetPage::INPUT_DATA));

    // By default we want a gaussian-type shape.
    m_config.setMassPeakShapeType(libmass::MassPeakShapeType::GAUSSIAN);
    m_ui.gaussianRadioButton->setChecked(true);

    // Throughout of *this* dialog window, the normalization factor
    // for the peak shapes (Gaussian, specifically) is going to be 1.

    // Get the number of points used to craft the peak curve.
    m_config.setPointCount(m_ui.pointCountSpinBox->value());

    // One of the constructors can transmit the intensity normalization value.
    // Set that value to the spinbox.

    if(m_newMaxIntensity != std::numeric_limits<double>::min())
      m_ui.normalizeValueDoubleSpinBox->setValue(m_newMaxIntensity);

    connect(m_ui.importPushButton,
            &QPushButton::clicked,
            this,
            &MassPeakShaperDlg::importFromText);

    connect(m_ui.normalizationGroupBox,
            &QGroupBox::toggled,
            this,
            &MassPeakShaperDlg::normalizingGrouBoxToggled);

    connect(m_ui.normalizeValueDoubleSpinBox,
            &QDoubleSpinBox::valueChanged,
            this,
            &MassPeakShaperDlg::normalizingIntensityValueChanged);

    connect(m_ui.fmwhToBinSizeRatioSpinBox,
            &QSpinBox::valueChanged,
            this,
            &MassPeakShaperDlg::fwhmBinSizeRatioValueChanged);

    connect(m_ui.resolutionSpinBox,
            &QSpinBox::editingFinished,
            this,
            &MassPeakShaperDlg::resolutionEditingFinished);

    connect(m_ui.fwhmDoubleSpinBox,
            &QSpinBox::editingFinished,
            this,
            &MassPeakShaperDlg::fwhmEditingFinished);

    connect(m_ui.pointCountSpinBox,
            &QSpinBox::editingFinished,
            this,
            &MassPeakShaperDlg::pointCountEditingFinished);

    connect(m_ui.gaussianRadioButton,
            &QRadioButton::toggled,
            this,
            &MassPeakShaperDlg::gaussianRadioButtonToggled);

    connect(m_ui.lorentzianRadioButton,
            &QRadioButton::toggled,
            this,
            &MassPeakShaperDlg::lorentzianRadioButtonToggled);

    connect(m_ui.checkParametersPushButton,
            &QPushButton::clicked,
            this,
            &MassPeakShaperDlg::checkParameters);

    connect(
      m_ui.runPushButton, &QPushButton::clicked, this, &MassPeakShaperDlg::run);

    connect(m_ui.outputFilePushButton,
            &QPushButton::clicked,
            this,
            &MassPeakShaperDlg::outputFileName);

    connect(m_ui.displayMassSpectrumPushButton,
            &QPushButton::clicked,
            this,
            &MassPeakShaperDlg::displayMassSpectrum);

    connect(m_ui.copyMassSpectrumToClipboardPushButton,
            &QPushButton::clicked,
            this,
            &MassPeakShaperDlg::copyMassSpectrumToClipboard);
  }


  QString
  MassPeakShaperDlg::craftMassSpectrumName()
  {
    // The user might have forgotten to insert a preferred name in the mass
    // spectrum file name edit widget. We thus craft one on the basis of the
    // centroid peaks and the current time.

    QString name = m_ui.massSpectrumNameResultsLineEdit->text();

    if(name.isEmpty())
      {
        name = QString("%1-%2@%3")
                 .arg(m_peakShapers.at(0)->getPeakCentroid().mz())
                 .arg(m_peakShapers.at(m_peakShapers.size() - 1)
                        ->getPeakCentroid()
                        .mz())
                 .arg(QTime::currentTime().toString("hh:mm:ss.zzz"));
      }
    else
      {
        name.append("@");
        name.append(QTime::currentTime().toString("hh:mm:ss.zzz"));
      }

    // qDebug() << "Returning mass spectrum name:" << name;
    return name;
  }


  std::size_t
  MassPeakShaperDlg::fillInThePeakShapers()
  {
    // qDebug();

    // Clear the peak shapers that might have been created in a previous run.
    m_peakShapers.clear();

    for(auto peak_centroid_sp : m_peakCentroids)
      {
        // We need to account for the charge in the config. But we do this on a
        // local copy.

        libmass::PeakCentroidSPtr local_peak_centroid_sp =
          std::make_shared<libmass::PeakCentroid>(*peak_centroid_sp);

        local_peak_centroid_sp->setMz(peak_centroid_sp->mz() /
                                      m_config.getCharge());

        m_peakShapers.append(std::make_shared<libmass::MassPeakShaper>(
          *local_peak_centroid_sp, m_config));
      }

    // qDebug() << "m_config:" << m_config.asText(800);

    message(
      QString("The number of peak centroids is: %1").arg(m_peakShapers.size()));

    return m_peakShapers.size();
  }


  bool
  MassPeakShaperDlg::processBinSizeConfig()
  {
    // We want to understand what the user is expecting: create bins or not,
    // and do they want to have the bin size configured manually or do they
    // expect that the bin size be determined automatically.

    double bin_size            = 0;
    bool set_bin_size_manually = m_ui.binSizeGroupBox->isChecked();
    bool no_bins               = m_ui.noBinsCheckBox->isChecked();

    bool ok = false;

    // First off, report the usage of bins. If no bins are to be calculated,
    // then it is not necessary to bother with the fwhm / bin_size factor (see
    // below).
    if(no_bins)
      {
        //qDebug() << "The user requests no bins.";

        m_config.setWithBins(false);
        m_config.setBinSize(0);

        return true;
      }
    else
      {
        m_config.setWithBins(true);
        //qDebug() << "The user requests bins, either automatic of manual.";
      }

    if(set_bin_size_manually)
      {
        // We are checking the manual settings configuration.

        //qDebug() << "Configuring the bin size by direct setting.";

        m_config.setBinSizeFixed(true);

        // Since the bin size is set manually, let the config object know that
        // it does not need to perform any computation.
        m_config.setBinSizeMultFactor(1);

        // Since the user checked the group box and they did not check the "no
        // bins" checkbox, then that means that they want to
        // actually set the bin size.

        bin_size = m_ui.binSizeDoubleSpinBox->value();

        if(!bin_size)
          {
            qDebug() << "The bin size cannot be 0.";
            message("The bin size cannot be 0.");
            return false;
          }

        m_config.setBinSize(bin_size);

        //qDebug() << "Bin size set manually:" << bin_size;
      }
    else
      {
        //qDebug() << "Configuring the bin size by calculation.";

        // The user did not elect to set the bin size manually, we can
        // compute it and update the value in its spin box.

        // The mass peak shaper will compute the bin size simply by dividing
        // FWHM by that factor. The idea is that it takes roughly that value
        // data points to craft a peak that has that FWHM.
        int ratio = m_ui.fmwhToBinSizeRatioSpinBox->value();

        if(!ratio)
          {
            qDebug() << "The FWHM / bin size ratio cannot be 0.";
            message("The FWHM / bin size ratio cannot be 0. Please, fix it.");

            return false;
          }

        // That multiplication factor will be accounted for in the config when
        // asked to compute the bin size = fwhm / ratio. An empirically good
        // value is 6.
        m_config.setBinSizeMultFactor(ratio);
        // qDebug() << "Set bin a size multiplication factor of" << ratio;

        m_config.setBinSizeFixed(false);

        double bin_size = m_config.binSize(&ok);

        if(!ok)
          {
            message("Could not compute the bin size.");
            qDebug() << "Could not compute the bin size.";
            return false;
          }

        m_ui.binSizeDoubleSpinBox->setValue(bin_size);

        qDebug() << "The configuration has computed bin size:" << bin_size;
      }

    return true;
  }


  bool
  MassPeakShaperDlg::checkParameters()
  {
    // qDebug();

    // At this point, reset the m_config so as to start fresh.
    m_config.reset();

    // The general idea is that a number of parameters are inter-dependent. We
    // need to check these parameters in a precise order because of these
    // inter-dependencies.

    // Remove all the text in the log text edit widget.
    m_ui.logPlainTextEdit->clear();

    // Remove all text in the results text edit widget.
    m_ui.resultPlainTextEdit->clear();

    // Make sure we have some (mz,i) values to crunch.
    if(!m_peakCentroids.size())
      {
        message("There are no centroid peaks to process");
        qDebug("There are no centroid peaks to process");

        return false;
      }

    if(m_mostIntensePeakCentroidSPtr == nullptr)
      {
        message("There is no most intense peak centroid.");
        qDebug() << "There is no most intense peak centroid.";

        return false;
      }

    // At the very end of the process, that is, when the whole mass spectrum
    // will have been computed with all Gaussian curves corresponding to all the
    // peak centroids, we may have to perform a normalization such that the max
    // intensity matches the set normalizing value. Check that value now.

    if(m_ui.normalizationGroupBox->isChecked())
      {
        // Evidently the user asks for normalization.

        // This value might have been set by the setup of the dialog based on
        // the value in the constructor. Also, it is updated when the user edits
        // the spinbox. So it is up-to-date.
        double new_max_intensity = m_ui.normalizeValueDoubleSpinBox->value();

        qDebug() << "Normalization requested with intensity:"
                 << new_max_intensity;

        if(!new_max_intensity)
          {
            message("The intensity normalization value is erroneous.");
            qDebug() << "The intensity normalization value is erroneous.";

            return false;
          }
      }
    else
      {
        // No normalization required, set the member datum to its default value
        // that is also a hint.

        // Do not change the value in the spin box, because the user
        // might want to keep that value handy even if the run is without
        // normalization.
        m_newMaxIntensity = std::numeric_limits<double>::min();
      }

    // We need to set back the reference peak centroid because the m_config was
    // reset above.
    m_config.setReferencePeakCentroid(*m_mostIntensePeakCentroidSPtr);

    // We will need this value for computations below.
    int point_count = m_ui.pointCountSpinBox->value();

    if(point_count < 3)
      {
        // qDebug() << "point_count:" << point_count;

        message(
          "The number of points allowed to craft the shape is too small. "
          "Please, fix it.");

        return false;
      }

    m_config.setPointCount(point_count);
    // qDebug() << "The set point count:" << m_config.getPointCount();

    // Get to know if we want gaussian or lorentzian shapes.
    if(m_ui.gaussianRadioButton->isChecked())
      {
        m_config.setMassPeakShapeType(libmass::MassPeakShapeType::GAUSSIAN);
        m_ui.logPlainTextEdit->appendPlainText("Peak shape is Gaussian\n");
      }
    else
      {
        m_config.setMassPeakShapeType(libmass::MassPeakShapeType::LORENTZIAN);
        m_ui.logPlainTextEdit->appendPlainText("Peak shape is Lorentzian\n");
      }

    // Now check the resolution/fwhm data that are competing one another.

    double fwhm    = m_ui.fwhmDoubleSpinBox->value();
    int resolution = m_ui.resolutionSpinBox->value();

    // Both values cannot be naught.
    if(!resolution && !fwhm)
      {
        QMessageBox::warning(
          0,
          tr("mineXpert: Peak shaper (Gaussian or Lorentzian)"),
          tr("Please, fix the Resolution / FWHM value (one or the other)"),
          QMessageBox::Ok);

        m_ui.logPlainTextEdit->clear();

        return false;
      }

    if(resolution)
      {
        // We will use the resolution
        m_config.setResolution(resolution);

        // We have to compute the FWHM value because that is a fundamental
        // parameter for the peak shaping. We use the most intense peak
        // centroid's mz value for that.

        bool ok = false;

        // With all the data successfully tested above, we must be able to
        // compute the FWHM. The config will store the computed value.
        double fwhm = m_config.fwhm(&ok);

        if(!ok)
          {
            message("Could not compute FWHM starting from resolution.");
            // qDebug() << "Could not compute FWHM starting from resolution.";
            return false;
          }

        m_ui.fwhmDoubleSpinBox->setValue(fwhm);
      }
    // End of
    // if(resolution)
    else
      {
        // We will use the FWHM
        m_config.setFwhm(fwhm);

        // We all the data above, we should be able to compute the resolution
        // and set it to the config.

        bool ok    = false;
        resolution = m_config.resolution(&ok);

        if(!ok)
          {
            message("Failed to compute the resolution.");
            // qDebug() << "Failed to compute the resolution.";
            return false;
          }

        m_ui.resolutionSpinBox->setValue(resolution);
      }
    // End of
    // ! if(resolution), that is use FWHM

    //////////////////// THE BIN SIZE /////////////////////

    // qDebug() << "Now processing the bin size configuration.";

    if(!processBinSizeConfig())
      {
        // qDebug() << "Failed processing the bin size configuration.";

        return false;
      }

    //////////////////// THE ION CHARGE /////////////////////

    m_config.setCharge(m_ui.ionChargeSpinBox->value());

    // We have finished configuring the process and storing all the useful
    // parameters in m_config. That configuration should resolve fine.

    // Craft a string describing the whole set of params as we have been
    // working on them in the configuration instance.

    QString info = QString(
                     "Resolution: %1.\n"
                     "FWHM: %2.\n"
                     "Reference m/z: %3\n"
                     "The number of points to shape the peak: %4.\n"
                     "Bins are requested: %5\n"
                     "The bin size is currently set at %6\n.")
                     .arg(m_config.getResolution())
                     .arg(m_config.getFwhm())
                     .arg(m_config.getReferencePeakCentroid().mz(), 0, 'f', 10)
                     .arg(m_config.getPointCount())
                     .arg(m_config.withBins())
                     .arg(m_config.getBinSize(), 0, 'f', 10);

    if(!m_config.resolve())
      {
        message("Failed to finally resolve the configuration.");
        qDebug().noquote() << info;

        return false;
      }

    m_ui.logPlainTextEdit->appendPlainText(info);

    m_ui.logPlainTextEdit->appendPlainText(
      QString("The peak shapes are computed for ions of charge: %1\n")
        .arg(m_config.getCharge()));

    // At this point, because we have set all the relevant values to the
    // m_config PeakShapeConfig, we can create the PeakShaper instances and set
    // to them the m_config.

    if(!fillInThePeakShapers())
      {
        QMessageBox::warning(
          0,
          tr("mineXpert: Peak shaper (Gaussian or Lorentzian)"),
          tr(
            "Please, fix the (mz,i) pairs. Make sure they are in the following "
            "format:\n"
            "<mz value><separator><i value>, with <separator> being any non "
            "numerical character set."),
          QMessageBox::Ok);

        return false;
      }

    m_ui.logPlainTextEdit->appendPlainText(
      QString("Number of input peak centroids to process: %1.\n")
        .arg(m_peakShapers.size()));

    m_ui.logPlainTextEdit->appendPlainText(m_config.toString());


    message("Check the LOG tab for details.");
    // At this point we'll have some things to report to the LOG tab,
    // switch to it now.
    // m_ui.tabWidget->setCurrentIndex(static_cast<int>(TabWidgetPage::LOG));

    // qDebug() << "Now returning true after having checked the parameters.";
    return true;
  }


  void
  MassPeakShaperDlg::resolutionEditingFinished()
  {
    double resolution = m_ui.resolutionSpinBox->value();

    if(!resolution)
      {
        // Tell the user to set a valid FWHM value, then.
        message("Will use the FWHM. Please, set a valid FWHM value");

        return;
      }

    // At this point we know that resolution contains a proper value.

    m_config.setResolution(resolution);

    QString msg;

    msg += "Will try to use the resolution... ";

    message(msg);

    bool ok = false;

    double fwhm = m_config.fwhm(&ok);

    if(!ok)
      {
        msg += "Failed. Check the parameters.";
        message(msg);
        return;
      }

    m_ui.fwhmDoubleSpinBox->setValue(fwhm);

    // Check if we have enough of data to actually compute the bin size (or not
    // depending on the user's requirements).

    ok = processBinSizeConfig();

    if(ok && m_config.withBins())
      {
        QString msg = QString("Could compute the bin size: %1")
                        .arg(m_config.getBinSize(), 0, 'f', 10);

        message(msg);
      }

    return;
  }


  void
  MassPeakShaperDlg::fwhmEditingFinished()
  {
    double fwhm = m_ui.fwhmDoubleSpinBox->value();

    if(!fwhm)
      {
        // Tell the user to set a valid resolution value, then.
        message(
          "Will use the resolution. Please, set a valid resolution value");
        return;
      }

    // At this point we know that fwhm contains a proper value.

    m_config.setFwhm(fwhm);


    QString msg;

    msg += "Will use the FWHM... ";

    message(msg);

    bool ok = false;

    double resolution = m_config.resolution(&ok);

    // This is a peculiar situation, because from here we MUST be able to
    // compute the resolution, because all that is required is the FWHM, that we
    // have, and the reference peak centroid that SHOULD be there also. So, make
    // the error fatal.

    if(!ok)
      qFatal(
        "Programming error. At this point we should be able to compute the "
        "resolution.");

    m_ui.resolutionSpinBox->setValue(resolution);

    // Check if we have enough of data to actually compute the bin size (or not
    // depending on the user's requirements).

    ok = processBinSizeConfig();

    if(ok && m_config.withBins())
      {
        QString msg = QString("Could compute the bin size: %1")
                        .arg(m_config.getBinSize(), 0, 'f', 10);

        message(msg);
      }

    return;
  }


  void
  MassPeakShaperDlg::pointCountEditingFinished()
  {
    // The points have changed, we'll use that value to compute the m/z step
    // between two consecutive points in the shaped peak.

    // All we need is either FWHM or resolution to advance the configuration.
    // Just check what we have.

    int point_count = m_ui.pointCountSpinBox->value();

    if(point_count < 5)
      {
        message("The number of points to craft the shape is too small.");
        return;
      }

    m_config.setPointCount(point_count);

    // At this point try to perform some calculations.

    double fwhm    = m_ui.fwhmDoubleSpinBox->value();
    int resolution = m_ui.resolutionSpinBox->value();

    if(fwhm)
      fwhmEditingFinished();
    else if(resolution)
      resolutionEditingFinished();

    // The bin size configuration is handled by the functions above.

    // That's all we can do.
  }

  void
  MassPeakShaperDlg::normalizingIntensityValueChanged()
  {
    m_newMaxIntensity = m_ui.normalizeValueDoubleSpinBox->value();
  }


  void
  MassPeakShaperDlg::normalizingGrouBoxToggled(bool checked)
  {
    if(!checked)
      m_newMaxIntensity = std::numeric_limits<double>::min();
    else
      m_newMaxIntensity = m_ui.normalizeValueDoubleSpinBox->value();
  }


  void
  MassPeakShaperDlg::fwhmBinSizeRatioValueChanged([[maybe_unused]] int value)
  {
    // Recalculate all the bin size stuff.
    processBinSizeConfig();
  }


  void
  MassPeakShaperDlg::gaussianRadioButtonToggled(bool checked)
  {
    if(checked)
      m_config.setMassPeakShapeType(libmass::MassPeakShapeType::GAUSSIAN);
    else
      m_config.setMassPeakShapeType(libmass::MassPeakShapeType::LORENTZIAN);
  }


  void
  MassPeakShaperDlg::lorentzianRadioButtonToggled(bool checked)
  {
    if(checked)
      m_config.setMassPeakShapeType(libmass::MassPeakShapeType::LORENTZIAN);
    else
      m_config.setMassPeakShapeType(libmass::MassPeakShapeType::GAUSSIAN);
  }


  void
  MassPeakShaperDlg::message(const QString &message)
  {
    m_ui.messageLineEdit->setText(message);
  }


  void
  MassPeakShaperDlg::run()
  {
    if(!checkParameters())
      {
        QMessageBox::warning(
          0,
          tr("mineXpert: Peak shaper (Gaussian or Lorentzian)"),
          tr("Please, insert at least one (m/z i) pair in the following "
             "format:\n\n"
             "<number><separator><number>\n\n"
             "With <separator> being any non numerical character \n"
             "(space or non-'.' punctuation, for example).\n\n"
             "Also, make sure that you fill-in the resolution or the FWHM "
             "value."),
          QMessageBox::Ok);

        return;
      }

    // At this point all the data are set, we can start the computation on all
    // the various peak shapers.

    double minMz = std::numeric_limits<double>::max();
    double maxMz = std::numeric_limits<double>::min();

    // Clear the map trace that will receive the results of the combinations.
    m_mapTrace.clear();

    // Now actually shape a peak around each peak centroid (contained in each
    // peak shaper).

    int processed = 0;

    for(auto peak_shaper_sp : m_peakShapers)
      {
        if(!peak_shaper_sp->computePeakShape())
          {
            QMessageBox::warning(
              0,
              tr("mineXpert: Peak shaper (Gaussian or Lorentzian)"),
              QString("Failed to compute a Trace for peak shape at m/z %1.")
                .arg(peak_shaper_sp->getPeakCentroid().mz(), QMessageBox::Ok));

            return;
          }

        // Now that we have computed the full shape around the centroid, we'll
        // be able to extract the smallest and greatest mz values of the whole
        // shape. We will need these two bounds to craft the bins in the
        // MzIntegrationParams object.

        if(peak_shaper_sp->getTrace().size())
          {
            double smallestMz = peak_shaper_sp->getTrace().front().x;
            minMz             = std::min(smallestMz, minMz);

            double greatestMz = peak_shaper_sp->getTrace().back().x;
            maxMz             = std::max(greatestMz, maxMz);
          }

        ++processed;
      }

    if(m_config.withBins())
      {
        // Bins were requested.

        // Get the bin size out of the configuration.

        double bin_size = m_config.getBinSize();

        // We will need to perform combinations, positive combinations.
        pappso::MassSpectrumPlusCombiner mass_spectrum_plus_combiner;

        m_mzIntegrationParams = pappso::MzIntegrationParams(
          minMz,
          maxMz,
          pappso::BinningType::ARBITRARY,
          -1,
          pappso::PrecisionFactory::getDaltonInstance(bin_size),
          false,
          0,
          true);

        // Now compute the bins.

        std::vector<double> bins = m_mzIntegrationParams.createBins();

        mass_spectrum_plus_combiner.setBins(bins);

        for(auto mass_peak_shaper_sp : m_peakShapers)
          {
            mass_spectrum_plus_combiner.combine(
              m_mapTrace, mass_peak_shaper_sp->getTrace());
          }
      }
    else
      {
        // No bins were required. We can combine simply:

        // qDebug() << "Performing the combination without bins.";

        pappso::TracePlusCombiner trace_plus_combiner(-1);

        for(auto mass_peak_shaper_sp : m_peakShapers)
          {
            // The combiner does not handle any data point for which y = 0.
            // So the result trace does not have a single such point;
            trace_plus_combiner.combine(m_mapTrace,
                                        mass_peak_shaper_sp->getTrace());
          }
      }

    message(QString("Successfully processed %1 peak centroids").arg(processed));

    // This is actually where the normalization needs to be performed. The
    // user might have asked that the apex of the shape be at a given
    // intensity. This is actually true when the IsoSpec++-based calculations
    // have been performed, that is the peak centroids have the expected
    // intensities normalized to the expected value. But then we have binned
    // all the numerous peak centroids into bins that are way larger than the
    // mz_step above. This binning is crucial to have a final spectrum that
    // actually looks like a real one. But it comes with a drawback: the
    // intensities of the m/z bins are no more the one of the initial
    // centroids, they are way larger. So we need to normalize the obtained
    // trace.
    if(m_newMaxIntensity != std::numeric_limits<double>::min())
      {
        // qDebug() << "Now normalizing to max intensity = " <<
        // m_newMaxIntensity;

        pappso::Trace trace = m_mapTrace.toTrace();
        pappso::Trace normalized_trace =
          trace.filter(pappso::FilterNormalizeIntensities(m_newMaxIntensity));

        // double max_int = normalized_trace.maxYDataPoint().y;
        // qDebug() << "After normalization max int:" << max_int;

        m_ui.resultPlainTextEdit->appendPlainText(normalized_trace.toString());
      }
    else
      m_ui.resultPlainTextEdit->appendPlainText(m_mapTrace.toString());

    // Now switch to the page that contains these results:

    m_ui.tabWidget->setCurrentIndex(static_cast<int>(TabWidgetPage::RESULTS));
  }


  void
  MassPeakShaperDlg::outputFileName()
  {
    m_fileName = QFileDialog::getSaveFileName(this,
                                              tr("Export to text file"),
                                              QDir::homePath(),
                                              tr("Any file type(*)"));
  }


  void
  MassPeakShaperDlg::displayMassSpectrum()
  {
    // We must display the result in the mass spectrum-displaying window and
    // in all the pinned_down widgets.

    // We want that the process going on from now on has an unambiguous
    // file/sample name, so we craft the name here.

    QString mass_spectrum_name = craftMassSpectrumName();

    // qDebug() << "Using mass spectrum name:" << mass_spectrum_name;

    // Here we need to somehow transfer the mass spectral data to a program
    // that can display that spectrum, typically minexpert2.
  }


  void
  MassPeakShaperDlg::copyMassSpectrumToClipboard()
  {

    // Simply copy the results shown in the text edit widget to the clipboard.
    // We cannot use m_mapTrace for that because if normalization was asked for,
    // then we would not see that in m_mapTrace (normalization is local).

    QString trace_text = m_ui.resultPlainTextEdit->toPlainText();

    if(trace_text.isEmpty())
      {
        message("The mass spectrum is empty.");
        return;
      }

    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(trace_text);
  }


  void
  MassPeakShaperDlg::setCentroidData(
    const std::vector<libmass::PeakCentroidSPtr> &peak_centroids)
  {
    m_mostIntensePeakCentroidSPtr = nullptr;

    // Start by copying the centroids locally.
    m_peakCentroids.assign(peak_centroids.begin(), peak_centroids.end());

    // Now create the text to display that in the text edit widget.
    QString result_as_text;

    // Convert the data into a string that can be displayed in the text edit
    // widget.

    // Reset the value to min so that we can test for new intensities below.
    double greatestIntensity = std::numeric_limits<double>::min();

    for(auto peak_centroid_sp : m_peakCentroids)
      {
        result_as_text += QString("%1 %2\n")
                            .arg(peak_centroid_sp->mz(), 0, 'f', 30)
                            .arg(peak_centroid_sp->intensity(), 0, 'f', 30);

        // Store the peak centroid that has the greatest intensity.
        // Used to compute the FWHM value starting from resolution. And
        // vice-versa.
        if(peak_centroid_sp->intensity() > greatestIntensity)
          {
            m_mostIntensePeakCentroidSPtr = peak_centroid_sp;
            greatestIntensity             = peak_centroid_sp->intensity();
          }
      }

    // qDebug() << "Reference peak centroid:"
    //<< m_mostIntensePeakCentroidSPtr.get()
    //<< QString("(%1,%2)")
    //.arg(m_mostIntensePeakCentroidSPtr->mz())
    //.arg(m_mostIntensePeakCentroidSPtr->intensity());

    m_config.setReferencePeakCentroid(*m_mostIntensePeakCentroidSPtr);

    m_ui.inputDataPointsPlainTextEdit->setPlainText(result_as_text);
  }

  void
  MassPeakShaperDlg::importFromText()
  {
    // We have peak centroids as in the text edit widget
    //
    // 59.032643588680002721957862377167 0.021811419157258506856811308694
    //
    // and we want to import them.

    QString peak_centroids_text =
      m_ui.inputDataPointsPlainTextEdit->toPlainText();

    // Easily transfom that into a Trace
    pappso::Trace trace(peak_centroids_text);

    // Temp vector of PeakCentroid instances.
    std::vector<libmass::PeakCentroidSPtr> peak_centroids;

    for(auto datapoint : trace)
      peak_centroids.push_back(
        std::make_shared<libmass::PeakCentroid>(datapoint.x, datapoint.y));

    // Finally do the real import.
    setCentroidData(peak_centroids);

    message("The data were imported. Please check the import results.");
  }

} // namespace libmassgui

} // namespace msxps
