/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QApplication>
#include <QDrag>
#include <QDebug>
#include <QMouseEvent>
#include <QMessageBox>

/////////////////////// Local includes 
#include <libmass/globals.hpp>
#include "IsoSpecTableView.hpp"
#include <libmass/IsoSpecEntity.hpp>
#include "IsoSpecDlg.hpp"


namespace msxps
{
namespace libmassgui
{


IsoSpecTableView::IsoSpecTableView(QWidget *parent) : QTableView(parent)
{

  setAlternatingRowColors(true);

  setSortingEnabled(false);

  QHeaderView *headerView = horizontalHeader();
  headerView->setSectionsClickable(true);
  headerView->setSectionsMovable(true);
}


IsoSpecTableView::~IsoSpecTableView()
{
}


void
IsoSpecTableView::setIsoSpecEntityList(
  QList<libmass::IsoSpecEntity *> *isoSpecEntityList)
{
  m_isoSpecEntityList = isoSpecEntityList;
}


QList<libmass::IsoSpecEntity *> *
IsoSpecTableView::getIsoSpecEntityList()
{
  return m_isoSpecEntityList;
}


IsoSpecDlg *
IsoSpecTableView::parentDlg()
{
  return mp_parentDlg;
}

void
IsoSpecTableView::setParentDlg(IsoSpecDlg *dlg)
{
  Q_ASSERT(dlg);
  mp_parentDlg = dlg;
}


} // namespace libmassgui

} // namespace msxps
