/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDialog>
#include <QPlainTextEdit>


/////////////////////// pappsomspp includes
#include <limits>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/trace/maptrace.h>
#include <pappsomspp/processing/combiners/mzintegrationparams.h>


/////////////////////// Local includes
#include "ui_MassPeakShaperDlg.h"
#include <libmass/PeakCentroid.hpp>
#include <libmass/MassPeakShaperConfig.hpp>
#include <libmass/MassPeakShaper.hpp>


namespace msxps
{

namespace libmassgui
{


  enum class TabWidgetPage
  {
    INPUT_DATA = 0x000,
    LOG,
    RESULTS,
  };


  class ProgramWindow;

  class MassPeakShaperDlg : public QDialog
  {
    Q_OBJECT

    public:
    MassPeakShaperDlg(QWidget *program_window_p,
                      const QString &applicationName,
                      const QString &description);

    MassPeakShaperDlg(
      QWidget *program_window_p,
      const QString &applicationName,
      const QString &description,
      const std::vector<libmass::PeakCentroidSPtr> &peak_centroids,
      double new_max_intensity = std::numeric_limits<double>::min());

    virtual ~MassPeakShaperDlg();

    void setCentroidData(
      const std::vector<libmass::PeakCentroidSPtr> &peak_centroids);

    void setNewMaxIntensity(double new_max_intensity);

    private:
    Ui::MassPeakShaperDlg m_ui;

    QWidget *mp_programWindow = nullptr;

    QString m_applicationName;
    QString m_fileName;

    pappso::MapTrace m_mapTrace;

    pappso::MzIntegrationParams m_mzIntegrationParams;

    // Of all the peak centroids' intensities, what is the greatest?
    libmass::PeakCentroidSPtr m_mostIntensePeakCentroidSPtr = nullptr;

    // This is the starting material for the shaping. Each peak centroid in this
    // vector is the apex of the shaped peak.
    std::vector<libmass::PeakCentroidSPtr> m_peakCentroids;

    // Each (m/z,i) pair (the i is in fact a probability that can later be
    // converted to a relative intensity) in the text edit widget will be
    // converted into a PeakShaper instance. Each PeakShaper instance will craft
    // the m_pointCount DataPoints to create the trace corresponding to the
    // starting peak centroid.
    QList<libmass::MassPeakShaperSPtr> m_peakShapers;

    double m_newMaxIntensity = std::numeric_limits<double>::min();

    libmass::MassPeakShaperConfig m_config;

    void writeSettings(const QString &configSettingsFilePath);
    void readSettings(const QString &configSettingsFilePath);

    void closeEvent(QCloseEvent *event);

    void setupDialog();

    QString craftMassSpectrumName();
    std::size_t fillInThePeakShapers();

    bool processBinSizeConfig();

    void message(const QString &message);

    private slots:
    void importFromText();
    void resolutionEditingFinished();
    void fwhmEditingFinished();
    void pointCountEditingFinished();

    void gaussianRadioButtonToggled(bool checked);
    void lorentzianRadioButtonToggled(bool checked);

    void normalizingIntensityValueChanged();
    void normalizingGrouBoxToggled(bool checked);

    void fwhmBinSizeRatioValueChanged(int value);

    bool checkParameters();

    // void inputDataSelectionChanged();

    void run();
    void outputFileName();
    void displayMassSpectrum();
    void copyMassSpectrumToClipboard();
  };

} // namespace libmassgui

} // namespace msxps

