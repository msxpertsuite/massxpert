/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <limits>
#include <vector>
#include <memory>

/////////////////////// Qt includes
#include <QSortFilterProxyModel>
#include <QFormLayout>


/////////////////////// Local includes
#include "IsoSpecTableView.hpp"
#include "IsoSpecTableViewModel.hpp"
#include "ui_IsoSpecDlg.h"
#include <libmass/globals.hpp>
#include <libmass/Atom.hpp>
#include <libmass/Formula.hpp>
#include <libmass/PeakCentroid.hpp>
#include <libmass/IsoSpecEntity.hpp>


namespace msxps
{
namespace libmassgui
{


  class IsoSpecTableViewModel;
  class IsoSpecTableViewSortProxyModel;

  class IsoSpecDlg : public QDialog
  {
    Q_OBJECT

    public:
    IsoSpecDlg(QWidget *program_window_p,
               const QString &applicationName,
               const QString &description);

    virtual ~IsoSpecDlg();

    bool initializeIsoSpecStandardStaticEntityList();
    bool initializeIsoSpecManualConfigurationWidgets();

    public slots:

    // To ease configuration of the IsoSpec standard data, let them save the
    // standard static data to a file that they can than configure to open in
    // the standard user config tab.
    bool saveIsoSpecStandardStaticTables();

    // These functions allow to store and retrieve the standard IsoSpecEntity
    // data after the user has configured them in a text editor or spreadsheet
    // program.
    bool loadIsoSpecStandardUserTables();
    bool saveIsoSpecStandardUserTables();

    // These functions allow to store and retrieve the user manual configuation
    // data that have been set using the element/isotope widgets.
    bool loadUserManualConfiguration();
    bool saveUserManualConfiguration();

    // std::pair<QLineEdit *, QSpinBox *> addElementSkeletonGroupBox();
    QGroupBox *addElementSkeletonGroupBox();
    QGroupBox *addElementGroupBox();
    void removeElementGroupBox();

    QFrame *createIsotopeFrame(QGroupBox *elementGroupBox = nullptr);
    std::pair<QDoubleSpinBox *, QDoubleSpinBox *> addIsotopeFrame();
    void removeIsotopeFrame();

    // The format of the IsoSpecStandarxxxConfig is the full tables from the
    // library format that is displayed in the table view widgets.
    bool runIsoSpecStandardStaticConfig();
    bool runIsoSpecStandardUserConfig();

    // The format of the manual config is the one used to create the widgtets
    // that pack the element/isotope widgets.
    bool runIsoSpecManualConfig();

    void toPeakShaper();

    private:
    Ui::IsoSpecDlg m_ui;

    QString m_applicationName;
    QString m_fileName;

    libmass::Formula m_formula;

    QWidget *mp_programWindow = nullptr;

    double m_intensityNormalizingValue = std::numeric_limits<double>::min();

    std::vector<libmass::PeakCentroidSPtr> m_peakCentroids;

    // The static list of entities as read from the IsoSpec header
    QList<libmass::IsoSpecEntity *> m_isoSpecStandardStaticEntityList;
    // Helper lists for the static list of entities
    QList<libmass::Atom *> m_isoSpecStandardStaticAtomList;
    std::vector<libmass::Atom *> m_isoSpecStandardStaticAtomVector;

    // The lists that hold *User* IsoSpec standard entities
    QList<libmass::IsoSpecEntity *> m_isoSpecStandardUserEntityList;
    // Helper lists for the static list of entities
    QList<libmass::Atom *> m_isoSpecStandardUserAtomList;
    std::vector<libmass::Atom *> m_isoSpecStandardUserAtomVector;

    // The lists that hold the *User* IsoSpec manual entities
    QList<libmass::Atom *> m_userManualAtomList;
    std::vector<libmass::Atom> m_userManualAtomVector;

    // Helper vector to hold atom / count pairs
    std::vector<std::pair<QString, int>> m_atomCountPairVector;

    // The table view model that manages the static IsoSpec standard data
    IsoSpecTableViewModel *mpa_isoSpecStandardStaticTableViewModel;

    // The table view model that manages the user IsoSpec standard data
    IsoSpecTableViewModel *mpa_isoSpecStandardUserTableViewModel;

    void writeSettings(const QString &configSettingsFilePath);
    void readSettings(const QString &configSettingsFilePath);

    std::size_t checkConsistencyIsoSpecTables();

    QString checkFormula(libmass::Formula &formula,
                         const QList<libmass::Atom *> &atomRefList);
    std::size_t validateManualConfig();

    void normalizeIntensities(double requested_intensity);

    void freeIsoSpecEntityList();
    void freeIsoSpecAtomList();
    void freeUserAtomList();

    void closeEvent(QCloseEvent *event);

    void setupIsoSpecStandardStaticTableView();
    void setupIsoSpecStandardUserTableView();
    bool setupDialog();

    void sortPeakCentroidsByMz(libmass::PeakCentroidSortOrder sort_order);
    void sortPeakCentroidsByRelativeIntensity(libmass::PeakCentroidSortOrder sort_order);

    void message(const QString &message, int timeout = 3000);
  };

} // namespace libmassgui

} // namespace msxps

